## FRA3826 Théories de l'édition numérique

# 12 - Chaînes éditoriales et systèmes de publication

===note===

===↓===

## Sommaire de la séance 12 - Chaînes éditoriales et systèmes de publication

1. Définition du concept de "chaîne éditoriale" : vers des systèmes de publication ?
2. Présentation de 5 systèmes d'édition numérique ou fabriques de publication emblématiques (Quire, Zettlr et Stylo)
3. Enjeux de fabrication, de production et de diffusion

===↓===

## 1. Définition du concept de "chaîne éditoriale" : vers des systèmes de publication ?

===↓===

### 1.1. Chaîne éditoriale ?

> L’ensemble des méthodes et des outils permettant de concevoir, fabriquer, produire et diffuser un livre ou une publication. Une chaîne de publication réunit des logiciels propres à la gestion du texte et des images, mais aussi les différents rôles d’un processus éditorial que sont l’auteur, l’éditeur, le correcteur, le graphiste, etc.

===note===
Le concept de _chaîne_ est quelque peu galvaudé, mais il définit bien certaines démarches d'édition qui lient les étapes.

===↓===

### 1.1. Chaîne éditoriale ?

> L’ensemble des **méthodes** et des **outils** permettant de **concevoir**, **fabriquer**, **produire** et **diffuser** un livre ou une publication. Une chaîne de publication réunit des logiciels propres à la gestion du texte et des images, mais aussi les différents rôles d’un processus éditorial que sont l’auteur, l’éditeur, le correcteur, le graphiste, etc.

===↓===

### 1.2. La quête du single source publishing
Une source pour de multiples formats produits.

===note===
Sur la base de la séparation du contenu et de la mise en forme, mais d'autres modèles existent (voir la thèse de Robin de Mourat).

===↓===

<!-- .slide: data-background="medias/single-source-publishing-2.jpg" data-background-size="contain" -->

===↓===

<!-- .slide: data-background="medias/single-source-publishing.png" data-background-size="contain" -->

===↓===

## 2. 5 systèmes de publication (en édition savante)

1. Lodel
2. Métopes
3. Scalar
4. Manifold
5. Quire

===↓===

### 2.1. Lodel
Lodel est un système de gestion de contenu (ou CMS en anglais) pour l'édition scientifique :

- **objectifs** : proposer un outil (relativement) simple d'utilisation pour éditer, publier et diffuser des publications scientifiques (articles et monographies) ;

<!-- .element: class="fragment" -->

- **protocole éditorial** : édition dans Lodel, avec ou sans une préparation préliminaire avec un traitement de texte (modèles et feuilles de styles spécifiques) ;

<!-- .element: class="fragment" -->

- **format(s) d'édition** : le travail peut être fait en 2 étapes (traitement de texte puis Lodel), dans Lodel les données sont conservées dans une base de données ;

<!-- .element: class="fragment" -->

- **format(s) produit(s)** : XML et XML-TEI, métadonnées ;

<!-- .element: class="fragment" -->

- **diffusion** : XML valides pour une diffusion sur les plateformes d'OpenEdition.

<!-- .element: class="fragment" -->

===note===
Lodel est clairement orienté pour gérer des contenus scientifiques avec des riches appareils critiques, et dans la perspective d'une diffusion sur les plateformes francophones.
Exemple : https://journals.openedition.org/corela/11734.

===↓===

### 2.1. Lodel

- **principes** : une solution clé en main, développée par l'une des principales plateformes de diffusion scientifique en sciences humaines en France (ou dans l'espace francophone) ;

<!-- .element: class="fragment" -->

- **contraintes** : ce CMS est loin d'être simple à installer. Son utilisation nécessite une formation adéquate (normal pour un outil aussi puissant) ;

<!-- .element: class="fragment" -->

- **actualité** : le code de Lodel est en train d'être revu en profondeur, il repose sur des briques logicielles vieillissantes.

<!-- .element: class="fragment" -->

===note===
J'ai moi-même essayé d'installer Lodel, j'ai abandonné...

===↓===

### 2.2. Métopes
L'infrastructure Métopes, pour Méthodes et outils pour l’édition structurée, permet la production de formats XML pour la diffusion scientifique, en se basant sur le principe du _single source publishing_ :

- **objectifs** : produire des articles et des monographies pour les plateformes de diffusion scientifique ;

<!-- .element: class="fragment" -->

- **protocole éditorial** : édition avec un traitement de texte via l'utilisation de feuilles de styles, puis génération des formats XML via différents outils ;

<!-- .element: class="fragment" -->

- **format(s) d'édition** : essentiellement du .docx et du .odt pour l'écriture et l'édition, puis XML ;

<!-- .element: class="fragment" -->

- **format(s) produit(s)** : un format XML-TEI qui peut être transformé en de nombreux formats (HTML, .indd, XML pour Cairn.info ou OpenEdition, etc.) ;

<!-- .element: class="fragment" -->

- **diffusion** : ce n'est pas le but de la chaîne, même si de nombreux formats de diffusion pour des plateformes francophones sont pris en compte (OpenEdition, Cairn.info et Érudit).

<!-- .element: class="fragment" -->

===↓===

<!-- .slide: data-background="medias/metopes.png" data-background-size="contain" -->

===note===
Exemple :

Qu'est-ce que XSLT ?
Un moyen de transformer un fichier balisé en un autre fichier balisé, avec une construction logique et en arbre sémantique.

===↓===

### 2.2. Métopes

- **principes** : proposer une chaîne puissante qui se connecte à des outils existants (traitements de texte et InDesign) pour l'édition scientifique et la diffusion scientifique ;

<!-- .element: class="fragment" -->

- **contraintes** : installer et utiliser les outils de Métopes qui viennent _s'ajouter_ ;

<!-- .element: class="fragment" -->

- **actualité** : Métopes travaille par exemple à l'interconnexion avec d'autres outils (comme Stylo).

<!-- .element: class="fragment" -->

===note===
Métopes reste agnostique.

===↓===

### 2.3. Scalar
Scalar est une plateforme de publication pour les humanités numériques :

- **objectifs** : permettre la création, l'édition et la publication de contenus académiques numériques et enrichis via une interface simple d'utilisation (monographies ou apparentées) ;

<!-- .element: class="fragment" -->

- **protocole éditorial** : gestion de contenus façon CMS, avec un modularité de ces contenus et de leurs enrichissements ;

<!-- .element: class="fragment" -->

- **format(s) d'édition** : interface web, bases de données ;

<!-- .element: class="fragment" -->

- **format(s) produit(s)** : web ;

<!-- .element: class="fragment" -->

- **diffusion** : directement sur la plateforme Scalar, avec des données exposées pour le web sémantique.

<!-- .element: class="fragment" -->

===note===


===↓===

<!-- .slide: data-background="medias/scalar.png" data-background-size="contain" -->

===note===
Quelques exemples : http://scalar.cdla.oxycreates.org/ ou https://scalar.usc.edu/nehvectors/thirdworldmajority/.

===↓===

### 2.3. Scalar

- **principes** : permettre une grande souplesse dans la gestion de contenus très divers, y compris des contenus multimédias, avec une maîtrise de la chaîne complète (création, édition, publication, diffusion) ;

<!-- .element: class="fragment" -->

- **contraintes** : plateforme monolithique et centralisée, édition façon CMS et WYSIWYG assez fastidieuse (et datée) ;

<!-- .element: class="fragment" -->

- **actualité** : des outils de versionnement et d'édition sont désormais intégrés.

<!-- .element: class="fragment" -->

===note===
L'outil est un peu vieillissant même s'il reste très puissant.
C'est moins sexy que Manifold, mais aussi plus riche en possibilités.
[La documentation](https://scalar.usc.edu/works/guide2/index) est complète.

===↓===

### 2.4. Manifold
Manifold est une plateforme collaborative pour l'édition scientifique :

- **objectifs** : permettre d'éditer facilement des publications scientifiques aux formats numériques (monographies) ;

<!-- .element: class="fragment" -->

- **protocole éditorial** : import de formats de traitement de texte (type .docx), ajout de données et de contenus enrichis, annotations et commentaires ;

<!-- .element: class="fragment" -->

- **format(s) d'édition** : base de données, interface web ;

<!-- .element: class="fragment" -->

- **format(s) produit(s)** : web, EPUB ;

<!-- .element: class="fragment" -->

- **diffusion** : Manifold est une plateforme d'édition **et** de diffusion.

<!-- .element: class="fragment" -->

===note===
Le positionnement de Manifold est très similaire à celui de Scalar, mais plus orienté texte et avec une dimension beaucoup plus contributive (annotations, commentaires, brouillon suivable).
Un travail d'UX important a été apporté, c'est fluide et rapide.

Exemple : les livres de University of Minnesota Press.
[Cut/Copy/Paste, Fragments of History, Whitney Trettien](https://manifold.umn.edu/projects/cut-copy-paste).

===↓===

### 2.4. Manifold

- **principes** : proposer une chaîne de publication (relativement) facile à installer, facile à utiliser et libre/open source. Il est possible d'éditer avec l'interface web _ou_ via les fichiers statiques (Markdown, YAML) ;

<!-- .element: class="fragment" -->

- **contraintes** : même si Manifold est basé sur des briques libres, c'est un système monolithique ;

<!-- .element: class="fragment" -->

- **actualité** : l'outil a renforcé l'intégration de formats type .docx, avec l'abandon de l'éditeur Markdown.

<!-- .element: class="fragment" -->

===note===
Manifold est aussi intéressant sur son adaptation à la réalité : la plupart des auteurs ne souhaitant pas travailler avec Markdown, l'éditeur de texte a été supprimé du système (mais il est possible d'intégrer des )
Manifold est, malheureusement, plus une plateforme de diffusion qu'une plateforme d'_édition_.
[La documentation](https://manifoldapp.org/docs/) est très complète.

===↓===

### 2.5. Quire
Une chaîne modulaire basée sur des outils du développement web :

- **objectifs** : produire plusieurs formats à partir d'une même source (_single source publishing_) ;

<!-- .element: class="fragment" -->

- **protocole éditorial** : éditeur de texte et versionnement, possibilité de brancher un CMS ou une interface en WYSIWYG ;

<!-- .element: class="fragment" -->

- **format(s) d'édition** : contenus et données balisés (Markdown, YAML, JSON)

<!-- .element: class="fragment" -->

- **format(s) produit(s)** : HTMLs, PDFs, EPUB, etc.

<!-- .element: class="fragment" -->

- **diffusion** : site web indépendant, vente de livre imprimé, dépôt de données

<!-- .element: class="fragment" -->

===note===
Un point d'histoire concernant Quire : éviter l'écueil des systèmes monolithiques.

===↓===

<!-- .slide: data-background="medias/quire.gif" data-background-size="contain" data-background-color="white" -->

===↓===

<!-- .slide: data-background="medias/chaine-modulaire.svg" data-background-size="contain" data-background-color="white" -->

===note===
Exemple : https://www.getty.edu/publications/ambers/.

===↓===

### 2.5. Quire

- **principes** : ne pas dépendre d'un logiciel monolithique, maîtriser les outils de la chaîne éditorial, pouvoir faire évoluer la chaîne, constituer une communauté d'éditeurs et d'éditrices ;

<!-- .element: class="fragment" -->

- **contraintes** : utilisation d'un éditeur de texte et d'un logiciel de versionnement. Toutes les briques sont libres _sauf_ PrinceXML (le générateur de PDF) ;

<!-- .element: class="fragment" -->

- **actualité** : communauté qui se constitue autour de l'outil, évolutions régulières, abandon de PrinceXML.

<!-- .element: class="fragment" -->

===note===
Utilisation du versionnement comme principe de gestion des fichiers (surtout pour les corrections).
Ce qu'il faut prendre en compte c'est que les personnes qui vont utiliser ces outils sont des éditeurs ou des éditrices, et plus spécifiquement l'équipe du département numérique de Getty Publications.
Il y a donc tout une dimension de formation, même si [la documentation](https://gettypubs.github.io/quire/) est très fournie.

À lire en complément :

- https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/
- http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/
- https://publications-prairial.fr/balisages/index.php?id=321

À noter que Quire pourrait aussi produire des formats XML sans trop de difficultés.

===↓===

## 3. Enjeux de fabrication, de production et de diffusion

- éradiquer le traitement de texte ?<!-- .element: class="fragment" -->
- deux positionnements : bases de données VS fichiers en plein texte ;<!-- .element: class="fragment" -->
- le tiercé gagnant :<!-- .element: class="fragment" -->
  1. single source publishing<!-- .element: class="fragment" -->
  2. chaîne modulaire<!-- .element: class="fragment" -->
  3. indépendance et pérennité<!-- .element: class="fragment" -->
- l'enjeu de la diffusion et du référencement : être trouvable et visible.<!-- .element: class="fragment" -->

===note===
Trop souvent cette question du référencement et de la diffusion est écartée au profit du contrôle de l'image des publications.

===↓===

## Ce qu'il faut retenir de Chaînes éditoriales et systèmes de publication

- le concept de chaîne éditorial tend vers des _systèmes_ sous l'influence du numérique ;

<!-- .element: class="fragment" -->

- les chaînes d'édition numérique _varient_ en fonction des objectifs ;

<!-- .element: class="fragment" -->

- 3 tendances observées :

<!-- .element: class="fragment" -->

  1. l'enrichissement des contenus (multimédias, annotations, commentaires)

<!-- .element: class="fragment" -->

  2. une recherche d'utilisabilité (écriture et lecture)

<!-- .element: class="fragment" -->

  3. bases de données complexes vs données à plat

<!-- .element: class="fragment" -->

===note===
Un monde de compromis ?
