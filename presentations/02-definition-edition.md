## FRA3826 Théories de l'édition numérique

# 02 - Pour une définition de l'édition

===↓===

## Sommaire de la séance 02 - Pour une définition de l'édition

1. Qu'est-ce que l'édition ?
2. Petite histoire de l'édition
3. Des éditions : littéraires, savantes, etc.
4. Pratiques éditoriales et métiers de l'édition
5. Supports éditoriaux et circulation du savoir

===↓===

## 1. Qu'est-ce que l'édition ?
Quelle est votre définition de l'édition ?

===note===
Est-ce que vous pourriez donner votre définition de l'édition ?
Il n'y a pas de mauvaise réponse.

===↓===

## 1. Qu'est-ce que l'édition ?
Reprenons les notions présentées dans le texte de Benoît Epron et Marcello Vitali-Rosati :

- l'édition est un "processus" qui opère sur des "contenus"<!-- .element: class="fragment" -->
- ce processus se divise en 3 "étapes" (3 fonctions) :<!-- .element: class="fragment" -->
  1. fonction de choix et de production<!-- .element: class="fragment" -->
  2. fonction de légitimation<!-- .element: class="fragment" -->
  3. fonction de diffusion<!-- .element: class="fragment" -->
- la maison d'édition : une instance éditoriale parmi d'autres<!-- .element: class="fragment" -->

===note===
Reprenons ensemble les notions abordées par Benoît Epron et Marcello Vitali-Rosati.
Qu'avez-vous retenu ?

La notion de processus implique une double dimension logique et temporelle : les étapes se suivent dans le temps et sont interdépendantes (si une étape n'a pas été réalisée, la suivante ne peut pas débuter).
Par la suite nous interrogerons à nouveau cette double dimension.

> Éditer signifie d’abord choisir et produire.

> La fonction de légitimation établit donc une différence entre les contenus et donne des indices sur leur valeur et finalement sur leur sens.

La fonction de diffusion est essentielle, car pour qu'un livre existe encore faut-il qu'il soit accessible ou disponible, et ce n'est pas une mince affaire.
Dans le cas d'un livre imprimé il s'agit de la chaîne du livre, et dans le cas d'un contenu numérique d'autres éléments sont à prendre en compte (comme le référencement web : la mise en ligne d'une page web ne suffit pas totalement à la faire exister).

===↓===

## 1. Qu'est-ce que l'édition ?

> L’édition peut être comprise comme un processus de médiation qui permet à un contenu d’exister et d’être accessible. On peut distinguer trois étapes de ce processus qui correspondent à trois fonctions différentes de l’édition : une fonction de choix et de production, une fonction de légitimation et une fonction de diffusion.  
> (Epron et Vitali-Rosati 2018, p. 6)

===note===
Voici une définition qui nous servira de base commune, et que nous compléterons en définissant par exemple ce qu'est un éditeur ou une maison d'édition.

Cette définition est très intéressante :

- c'est une définition complète qui ne concerne pas l'édition telle qu'on la considère en occident au 20e siècle ;
- c'est une définition qui recoupe à la fois le monde de l'imprimé et celui du numérique ;
- c'est une définition qui déstructure ses éléments, et donc qui montre la complexité d'une activité qui est pourtant dans le langage courant et au centre de nos cultures occidentales.

===↓===

## 1. Qu'est-ce que l'édition ?
Quelques distinctions importantes :

- distinction entre les instances éditoriales et les maisons d'édition<!-- .element: class="fragment" -->
- pour qu'il y ait "édition" il faut qu'il y ait les 3 fonctions<!-- .element: class="fragment" -->
- la diffusion ne se limite pas à une mise à disposition : "Un contenu est édité quand il est pour quelqu’un."<!-- .element: class="fragment" -->
- l'édition évolue selon les époques et les environnements technologiques<!-- .element: class="fragment" -->

===note===
Attention : produire un livre ne suffit pas pour qu'il y ait édition, c'est d'ailleurs ce qui distingue 

Nous pouvons expliciter la notion de légitimation, qui s'inscrit dans un espace économique (économie de la connaissance, économie financière) et qui représente aussi le risque pris par la structure ou la personne _qui édite_.

L'édition est clairement différente à travers les époques, comme par exemple avec ces 3 événements :

- imprimerie à caractères métalliques mobiles
- informatique
- Internet et le web

Ce qui nous amène à la partie suivante : quelques dates clés de l'édition.

===↓===

## 2. Petite histoire de l'édition

1. jusqu'au XVe siècle<!-- .element: class="fragment" -->

===note===
Vous m'excuserez cet européocentrisme dû à ma culture française.

Je vous propose d'observer comment l'édition a évolué, et plus spécifiquement comment la figure de l'éditeur a évolué.
Cette évolution est particulièrement liée aux techniques de production, c'est donc aussi une histoire de la technique.
Il s'agit d'un panorama trop rapide.

===↓===

<!-- .slide: data-background="medias/copiste.jpg" data-background-size="contain" -->

===note===
S'il fallait retenir une image ce serait probablement celle-ci : un copiste qui recopie à la main un codex.
Cette image ne peut pas réellement résumer l'édition d'avant le codex, mais elle permet de se figurer les techniques de reproductibilité qui avait de fortes contraintes sur la circulation du savoir.

===↓===

## 2. Petite histoire de l'édition

1. jusqu'au XVe siècle
2. à partir du XVe siècle : imprimerie à caractères mobiles<!-- .element: class="fragment" -->

===↓===

<!-- .slide: data-background="medias/imprimerie.jpg" data-background-size="contain" -->

===note===
Il faut noter le passage à un rythme artisanal pré-industriel.

L'invention de Gutenberg, l'imprimerie à caractères métalliques mobiles, est une invention à 3 dimensions :

- technique
- culturelle ou humaniste
- entrepreneurial

Il ne faut jamais oublier que la recherche de Gutenberg était autant une recherche technique et humaniste qu'une recherche de profit.

Les éditeurs disposaient alors de plusieurs fonctions : la fonction éditoriale, la production et la vente (tout cela dans un même espace).

===↓===

## 2. Petite histoire de l'édition

1. jusqu'au XVe siècle
2. à partir du XVe siècle : imprimerie à caractères mobiles
3. à partir du XIXe siècle : distinction imprimeur, libraire et éditeur<!-- .element: class="fragment" -->
4. à partir des années 1950 : nouvelles formes, nouveaux publics<!-- .element: class="fragment" -->

===note===
En 1810 un décret permet aux libraires d'être désormais distincts des éditeurs, et les imprimeurs se détachent eux aussi de l'éditeur.
Il s'agit d'une division des tâches jusqu'ici intégrées dans la figure de l'éditeur.

Ce fonctionnement va perdurer pendant un siècle et demi, période durant laquelle d'autres événements vont intervenir, comme les techniques de fabrication et d'impression, les circuits de diffusion, l'apparition des roman feuilletons, etc.

Le livre de poche est un moment important, car à partir de nouveaux procédés de fabrication et de diffusion (liés à un système d'achat de droits), le roman puis l'essai vont pouvoir être lus par d'autres publics.
La littérature et le savoir se diffusent plus largement, et plus vite.

===↓===

<!-- .slide: data-background="medias/livre-de-poche.jpg" data-background-size="contain" -->

===↓===

## 2. Petite histoire de l'édition

1. jusqu'au XVe siècle
2. à partir du XVe siècle : imprimerie à caractères mobiles
3. à partir du XIXe siècle : distinction imprimeur, libraire et éditeur
4. à partir des années 1950 : nouvelles formes, nouveaux publics
5. à partir des années 1980 : arrivée de l'informatique<!-- .element: class="fragment" -->

===↓===

<!-- .slide: data-background="medias/mac.png" data-background-size="contain" -->

===note===
Le premier ordinateur grand public et dans le monde professionnel est ce Macintosh 128K, rapidement utilisé dans le domaine du livre et spécifiquement dans celui de l'édition (avec les premiers traitements de texte).

C'est donc aussi et surtout l'apparition de la PAO pour publication assistée par ordinateur, une pratique qui consiste à utiliser l'informatique pour composer des livres.

===↓===

## 2. Petite histoire de l'édition

1. jusqu'au XVe siècle
2. à partir du XVe siècle : imprimerie à caractères mobiles
3. à partir du XIXe siècle : distinction imprimeur, libraire et éditeur
4. à partir des années 1950 : nouvelles formes, nouveaux publics
5. à partir des années 1980 : arrivée de l'informatique
6. à partir des années 1990 : espace numérique<!-- .element: class="fragment" -->

===↓===

<!-- .slide: data-background="medias/requin-cable.png" data-background-size="contain" -->

===note===
Cette image d'un requin qui tente de mordre un câble qui relie l'Europe et l'Amérique du Nord est une bonne illustration de l'espace dans lequel nous vivons depuis les années 1990, avec la démocratisation d'Internet puis l'arrivée du web.

Source : le documentaire _World Brain_ réalisé par Stéphane Degoutin et Gwenola Wagon (https://www.arte.tv/fr/videos/050970-001-A/world-brain/).

===↓===

<!-- .slide: data-background="medias/pod.jpg" data-background-size="contain" -->

===note===
L'impression à la demande est un processus de fabrication/production/diffusion apparu grâce au numérique : l'impression numérique et des machines de façonnage spécifiques permettent de fabriquer des exemplaires à la demande plutôt que de prévoir un tirage important et onéreux pour les éditeurs.
Les coûts de stockage sont très fortement diminués, et les coûts de diffusion/distribution grandement améliorés.

===↓===

## 3. Des éditions : littéraires, savantes, etc.
L'édition est plurielle selon les domaines, l'organisation ou les dispositions&nbsp;:

- littérature, essai, édition savante, livres pratiques, littératures de genre, beaux livres, littérature de jeunesse, bande dessinée, etc.<!-- .element: class="fragment" -->
- groupes d'édition, édition indépendante, co-édition, etc.<!-- .element: class="fragment" -->
- édition à compte d'éditeur, édition à compte d'auteur, auto-édition, etc.<!-- .element: class="fragment" -->


===note===
L'édition est plurielle et diverse : il faut se détacher de l'image d'Épinal de l'édition uniquement littéraire.
Exemples des domaines comme la littérature jeunesse (et plus spécifiquement l'album), les livres pratiques, etc.

Nous allons nous intéresser, dans ce cours, principalement à l'édition littéraire et à l'édition savante.

===↓===

## 4. Pratiques éditoriales et métiers de l'édition
### Des pratiques très différentes selon les domaines éditoriaux


===note===
Tour d'horizon des pratiques et des métiers de l'édition (liste non exhaustive).

===↓===

## 4. Pratiques éditoriales et métiers de l'édition
### Quelques métiers de l'édition

- éditeur·trice
- relecteur·trice
- correcteur·trice
- graphiste/infographiste
- responsable de fabrication
- gestionnaire de droits
- commercial·e
- iconographe

===note===
Ce n'est pas une liste exhaustive et cela correspond globalement à l'organisation du métier d'éditeur à la fin du 20e siècle.
Il faut noter que ces sous-fonctions tendent à s'externaliser (comme l'imprimeur ou le libraire au 19e siècle), l'édition (au sens des fonctions de choix et de légitimation) est la seule fonction intégrée au sein de la maison d'édition (même si certaines maisons tendent à ne conserver que la gestion globale et administrative, l'éditeur étant lui-même un sous-traitant).

Certains métiers ou profils peuvent être plus spécialisés sur le numérique, il y a eu pendant plusieurs années des référent·e·s sur les questions numériques au sein de grands groupes d'édition (pratique bientôt révolue).

===↓===

## 5. Supports éditoriaux et circulation du savoir
Premiers éléments d'une réflexion :

- les supports sont (très) divers (artéfacts imprimés ou numériques)<!-- .element: class="fragment" -->
- les supports conditionnent la circulation (et inversement)<!-- .element: class="fragment" -->
- les multipliticité des supports (pour un même contenu) se superposent (sans se substituer)<!-- .element: class="fragment" -->

===note===
Comme nous l'avons vu les pratiques éditoriales divergent en fonction des domaines.
Nous reviendrons plus longuement sur la question des supports, je veux simplement insister sur le rapport entre les supports et la circulation du savoir (ou plus globalement des contenus).
Dans ce cours sur l'édition numérique cela nous intéresse particulièrement : comment penser l'articulation entre plusieurs supports différents, qui sont souvent complémentaires.

Pour prendre un exemple, nous pouvons évoquer deux livres :

- 

Pour pouvoir prolonger ces questions de supports et de circulation nous devrons faire quelques détours, ce sera l'objet des prochaines séances (Qu'est-ce que le numérique ? ; Introduction aux humanités numériques ; Édition, politique et société ; et Livre et lecture numériques).

===↓===

## Conclusion
Pour une définition de l'édition, points essentiels :

- distinction entre l'instance éditoriale et la structure d'édition

<!-- .element: class="fragment" -->
- l'histoire de l'édition est aussi une histoire des techniques

<!-- .element: class="fragment" -->

- les supports conditionnent la circulation des contenus

<!-- .element: class="fragment" -->

- pour en savoir plus sur les métiers de l'édition et la chaîne du livre, écoutez l'excellent podcast La mécanique du livre ([https://www.editionsducommun.org/blogs/podcasts/](https://www.editionsducommun.org/blogs/podcasts/))

<!-- .element: class="fragment" -->
