# FRA3826 – Théories de l'édition numérique

Antoine Fauchié
[antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)

**Pour naviguer d'une séance à l'autre, utiliser votre clavier ou les flèches directionnelles :**

- ← et → pour naviguer d'un cours à l'autre
- ↓ et ↑ pour naviguer d'un diapositive à une autre à l'intérieur d'un cours
- touche ÉCHAP pour avoir une vision d'ensemble
