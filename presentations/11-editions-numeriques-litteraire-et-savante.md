## FRA3826 Théories de l'édition numérique

# 11 - L'édition numérique littéraire et l'édition numérique savante

===note===
Préambule :

- évaluation de mi-session
- les deux dernières séances
- les nouveaux visages de l'édition savante
- l'impression à la demande

===↓===

## Sommaire de la séance 11 - L'édition numérique littéraire et l'édition numérique savante

1. Qu'est-ce qu'éditer un texte littéraire en numérique ?<!-- .element: class="fragment" -->
2. L'édition numérique savante : production (enjeux des formats de structuration)<!-- .element: class="fragment" -->
3. L'édition numérique savante : diffusion (enjeux des données et des interfaces)<!-- .element: class="fragment" -->

===↓===

## 1. Qu'est-ce qu'éditer un texte littéraire en numérique ?

===note===
Cette question aussi vaste que complexe, je ne peux pas la traiter en quelques minutes, nous allons plutôt observer les étapes d'édition d'un texte littéraire, puis constater la complexité de la littérature numérique.

===↓===

### 1.1. Les étapes d'édition
Quelques étapes (génériques) d'édition :

1. écriture du manuscrit par l'auteur ;<!-- .element: class="fragment" -->
2. soumission du texte finalisé ou non finalisé à l'éditeur ;<!-- .element: class="fragment" -->
3. premier avis sur le texte par l'éditeur ;<!-- .element: class="fragment" -->
4. reprise du texte par l'auteur ;<!-- .element: class="fragment" -->
5. phases de révision/correction orthotypographique ;<!-- .element: class="fragment" -->
6. mise en forme du texte, création d'une couverture ;<!-- .element: class="fragment" -->
7. soumission du BAT à l'auteur par l'éditeur ;<!-- .element: class="fragment" -->
8. validation du BAT par l'auteur ;<!-- .element: class="fragment" -->
9. envoi en fabrication.<!-- .element: class="fragment" -->

===note===
Il est nécessaire de rappeler quel est le contexte de l'édition littéraire _avant_ de parler de l'édition numérique.
Ces étapes sont celles habituellement mises en œuvre pour publier un texte littéraire, qu'il s'agisse de fiction ou d'essai.

===↓===

### 1.1. Les étapes d'édition (et leurs outils)
Quelques étapes (génériques) d'édition :

1. écriture du _manuscrit_ par l'auteur : **traitement de texte 1** ;
2. soumission du texte finalisé ou non finalisé à l'éditeur : **format 1** ;
3. premier avis sur le texte par l'éditeur : **traitement de texte 2** + **format 2** ;
4. reprise du texte par l'auteur : **traitement de texte 1** + **format 3** ;
5. phases de révision/correction orthotypographique : **traitement de texte 3** + **format 4** ;
6. mise en forme du texte, création d'une couverture : **logiciel de PAO** + **format 5** ;
7. soumission du BAT à l'auteur par l'éditeur : **format 6** ;
8. validation du BAT par l'auteur : **format 6** ;
9. envoi en fabrication : **format 7**.

===note===
Pourquoi analyser en détail ces phases et les outils associés ?
Parce que nous sommes ici dans le cas d'un travail d'édition sur des objets littéraires relativement simples, nous allons voir que les choses se compliquent dans le cas d'œuvres hypertextuelles, _hypermédiatiques_ ou multimodales.

===↓===

### 1.2. La réalité de la littérature numérique

- pluralité<!-- .element: class="fragment" -->
- originalité<!-- .element: class="fragment" -->
- invisibilité<!-- .element: class="fragment" -->
- non-pérennité<!-- .element: class="fragment" -->

===note===
Avant de lire le texte d'Alexandra Saemmer, il faut faire plusieurs constats :

- pluralité : il n'y a pas _une_ littérature numérique, mais beaucoup de genres différents, Alexandra Saemmer en identifie 3 dans son texte comme nous allons le voir ;
- originalité : que ce soit en terme d'écriture, de parcours narratifs ou de formes ;
- invisibilité : il faut reconnaître que le _mouvement_ de la littérature numérique est assez méconnu, les œuvres ont du mal à dépasser le public d'initiés (qui est composé en majorité d'auteurs et d'autrices de littérature numérique) ;
- non-pérennité : ces objets littéraires peuvent être considérés comme des prototypes, qui dépendent de certaines briques technologiques. Le meilleur exemple est la fin très prochaine de Flash qui va plonger un pan de la littérature numérique dans le noir.

===↓===

### 1.3. À propos de l'article d'Alexandra Saemmer

- 3 genres de la littérature numérique :<!-- .element: class="fragment" -->
    1. figurative<!-- .element: class="fragment" -->
    2. hypertextuelle<!-- .element: class="fragment" -->
    3. programmatique<!-- .element: class="fragment" -->
- constitution de communautés<!-- .element: class="fragment" -->
- problèmes soulevés : lisibilité, visibilité, pérennité<!-- .element: class="fragment" -->

===note===
Quelques éléments de contextualisation avant de commencer : Alexandra Saemmer est une chercheuse spécialisée dans la littérature numérique.
Si cet article semble pour le moins daté — 2008 c'est déjà très daté pour le temps de l'édition numérique —, ces réflexions sont précieuses et marquent un tournant important pour l'édition numérique en particulier et l'édition en général.

Pour commencer Alexandra Saemmer fait une distinction importante entre _publication_ et _édition_ : ce qui relève de l'initiative d'auteurs, sans validation ou inscription dans un corpus plus large que ce que propose l'auteur lui-même ; ce qui correspond à une démarche plus structurée, avec une instance de légitimation (voir aussi de validation).

Cet article détaille les 3 genres de la littérature numérique (ce sont mes termes) :

1. figurative : relation entre l'image et le texte (animés ou fixes)
2. hypertextuelle : "dynamisation de l'écrit" via l'hypertexte
3. programmatique : "rôle du programme"

Et au centre de cela, probablement le plus important : la constitution de communautés, donc le fait que des auteurs se réunissent pour échanger sur leur pratique et sur la maîtrise d'outils de création et de production (sous le nez des éditeurs).

Si le succès commercial de ces expérimentations littéraires est presque nul (et parfois la quête de légitimité également, hormis au sein d'un petit cercle), il y a pourtant un foisonnement extrêmement riche, varié et expérimental.
Le rôle d'Alexandra Saemmer est, selon moi et au-delà de cette recherche scientifique, de donner à voir toute cette variété en proposant une structure lisible et cohérente (ce qui n'est pas forcément le rôle de celles et ceux qui créent).

Alexandra Saemmer conclut sur les problèmes soulevés.

===↓===

### 1.4. Y a-t-il une édition numérique littéraire ?
Plusieurs événements :

- la constitution de communautés et de structures pour les accueillir (Electronic Literature Organization, Remue.net) ;

<!-- .element: class="fragment" -->

- la recherche scientifique autour de ces mouvements et initiatives ;

<!-- .element: class="fragment" -->

- la création d'éditeur _pure players_.

<!-- .element: class="fragment" -->

===note===
À partir de ce que nous venons de voir la question serait donc de savoir s'il y a une édition numérique littéraire ?
C'est-à-dire est-ce que les expérimentations 

Qu'est-ce qu'un éditeur _pure player_ ?
C'est un éditeur qui ne propose ses titres qu'au format numérique.
Plusieurs éditeurs sont apparus, dès les débuts du livre numérique à la fin des années 2000, ne voulant proposer que des livres numériques sans en imprimer.
Publie.net est particulièrement emblématique : cette maison d'édition créée par François Bon et désormais collectif, a été l'une des premières dans le paysages francophone à proposer uniquement des livres au format numérique.
Plusieurs maisons d'édition avaient déjà commencé à faire de l'édition numérique (comme 00h00 à la fin des années 1990), mais le fait de ne pas imprimer de livres était nouveau.
D'autres suivront, comme Walrus ou Numeriklivres.

Dans les années 2015-2016 il y a toutefois un virage important : le passage à l'imprimé de ces éditeurs pure player, agacés de n'avoir aucune reconnaissance dans le paysage littéraire pour la simple raison que le papier est absent de leur catalogue.
Ce passage à l'imprimé est aussi une tentative de rendre visible des livres dans les espaces physiques comme les salons ou les librairies, matérialisations tentées à plusieurs reprises avec des procédés ingénieux mais peu efficaces (cartes avec QR codes, bornes, etc.).
Plusieurs éditeurs pure player vont donc opter pour l'impression à la demande, puis pour des petits tirages.
(D'ailleurs la lecture du [carnet de bord de Publie.net](https://www.publie.net/category/carnet-de-bord/) est un très bon moyen de comprendre le métier d'éditeur.)

Tout cela pour dire que la seule littérature numérique à voir rejoint le circuit commercial classique est la littérature numérique _homothétique_, hormis quelques belles réalisations de livres numériques enrichis comme la revue [D'ici là](https://www.publie.net/categorie-produit/dici-la/) chez Publie.net.

La réponse serait donc oui, il y a une édition numérique littéraire, qui est le reflet des changements provoqués par le numérique sur l'édition, et de la complexité de ces changements et influences.

===↓===

## 2. L'édition numérique savante : production (enjeux des formats de structuration)

===↓===

### 2.1. Préambule
Deux types d'édition savante :

- l'édition scientifique<!-- .element: class="fragment" -->
- l'édition savante grand public<!-- .element: class="fragment" -->

===note===
Il est nécessaire de faire cette distinction, pour comprendre les enjeux littéraires et commerciaux, il existe donc (au moins) deux types d'édition savante :

- l'édition scientifique : des textes écrits puis validés par la communauté scientifique, publiés par des institutions rattachées à des universités. La question de la rentabilité n'est pas forcément une prérogative, les textes doivent d'abord exister ;
- l'édition savante grand public : des éditeurs _privés_ qui vont avoir autant d'exigence scientifique mais des contraintes de rentabilité, et un public plus large (les universitaires et les autres pour le dire rapidement).

Mais toujours la même exigence de qualité.

===↓===

### 2.2. Des contraintes de formats

- format de lecture : PDF et HTML (et papier si papier)

<!-- .element: class="fragment" -->

- format de _données_ : XML comme passage obligé

<!-- .element: class="fragment" -->

===note===
L'édition savante est contrainte par les formats de lecture, d'une autre façon que l'édition littéraire : les textes sont lus de multiples façons, ce sont autant des livres que des documents de travail.
Par exemple il faut permettre autant une lecture sur écran qu'une impression sur des imprimantes non professionnelles ou faciliter une recherche dans des corpus et dans les documents.

Les formats de lecture numérique plébiscités sont avant tout le PDF puis l'HTML.
Parce que ce sont les plus classiquement 

En terme d'affichage, nous pouvons prendre l'exemple de deux plateformes :

- Érudit : [article](https://www.erudit.org/fr/revues/etudfr/2007-v43-n3-etudfr1895/016907ar/) aux formats HTML, PDF puis XML ;
- Cairn.info : [article](https://www.cairn.info/revue-les-cahiers-du-numerique-2011-3-page-47.htm) aux formats HTML et PDF.

===↓===

<!-- .slide: data-background="medias/html-pdf-xml.png" data-background-size="contain" -->

===↓===

### 2.3. Écrire a-t-il encore le même sens ?
Écrire _au temps du balisage_.

===note===
Ici je vous renvoie aux travaux de Marcello Vitali-Rosati, ou à ceux d'Arthur Perret doctorant en sciences de l'information.
La question est de savoir comment les chercheurs peuvent intégrer les questions de 
Je balaie peut-être un peu vite la question de baliser ou de ne pas baliser, pour moi il est essentiel que le travail de rédaction ou d'édition soit aussi celui de la structuration

La question pourrait être résumée à : qui balise ?
C'est-à-dire qui va se charger de faire le travail intellectuel de structuration de l'information.
Cela ne me semble pas possible que ce soit quelqu'un d'autre que l'auteur ou l'autrice, alors que pourtant actuellement ce sont des _petites mains_ qui s'en chargent (et souvent des femmes...).

Question du balisage _au moment d'écrire_.

_exemple de balisage dans un traitement de texte puis dans Stylo_


===↓===

### 2.4. Les outils de production

- classique : Word to XML ;

<!-- .element: class="fragment" -->

- nouvelle tendance : XML _first_ ;

<!-- .element: class="fragment" -->

- non conventionnel : chaînes modulaires ;

<!-- .element: class="fragment" -->

===note===
Nous reparlerons en détail de ces questions d'outils et de chaînes, mais nous pouvons tout de même en dire quelques mots.

Actuellement les éditeurs se retrouvent au milieu d'exigences pour produire les articles dans des formats riches, comme le XML, et entre les auteurs qui ne savent pas utiliser autre chose que Word (et encore lorsqu'il est réellement utilisé).
Exemple : pour soumettre un chapitre j'ai dû adopter les recommandations de l'éditeur, soit une feuille de style Word.
Cette feuille de style n'était en fait que la liste des différentes mises en forme, impossible d'appliquer facilement une structuration sur un document, chaque titre devait être reprise à la main...
Exemple de mon article avec Thomas Parisot.
Le fait d'accepter autre chose que Word (donc par exemple LibreOffice) semble déjà une évolution incroyable...

===↓===

## 3. L'édition numérique savante : diffusion (enjeux des données et des interfaces)

===↓===

### 3.1. La structuration des métadonnées fines

- édition savante : impératifs de référencement et de citabilité

<!-- .element: class="fragment" -->

- la complexité est aussi du côté commerce : [ONIX](https://www.editeur.org/8/ONIX/) (ONline Information eXchange)

<!-- .element: class="fragment" -->

===note===
La question du référencement est celle de la visibilité des articles et des ouvrages dans des moteurs de recherche généralistes et spécialisés.
La citabilité c'est le fait d'être en capacité de donner toutes les informations utiles pour que le document puisse être cité facilement : ces données bibliographiques peuvent par exemple être exposées pour être moissonner par Zotero ou des agrégateurs (niveau individuel ou niveau collectif).
Voir le code source d'articles sur Érudit ou Cairn.info.

===↓===

### 3.2. Les données d'abord, les interfaces ensuite

- les données sont d'abord structurées<!-- .element: class="fragment" -->
- pour ensuite être affichées dans des environnements divers<!-- .element: class="fragment" -->
- et pour produire des formats variés<!-- .element: class="fragment" -->

===note===
Exemple de Nouvelles vues : web. PDF, HTML Érudit et XML Érudit.

La question est de savoir comment structurer des données : quel modèle ? Quel **schéma** ?
Les pratiques convergent vers [le schéma JATS](https://en.wikipedia.org/wiki/Journal_Article_Tag_Suite#Example).

Adaption des affichages : exemple du titre d'une bibliographie qui sera traduit selon la langue de l'utilisateur.

Le single source publishing comme principe de base, sans cela nous sommes réduits à tout refaire en permanence.

===↓===

### 3.3. Coup d'œil sur les technologies d'édition numérique
Technologies de l’édition numérique, Julie Blanc et Lucile Haute :

[http://recherche.julie-blanc.fr/timeline-publishing/](http://recherche.julie-blanc.fr/timeline-publishing/)

===↓===

## Ce qu'il faut retenir de L'édition numérique littéraire et l'édition numérique savante

- les deux faces de la littérature numérique : expérimentale (hors circuits classiques) et homothétique (commercialisable) ;
- les contraintes commerciales ne peuvent pas cohabiter avec les formes non conventionnelles ;
- l'édition numérique est un exercice de distinction entre écriture et lecture ;
- l'édition savante est un équilibre entre structuration, données, référencement et lisibilité ;
- évolution progressive des technologies.

===note===

- les contraintes commerciales ne peuvent pas cohabiter avec les formes non conventionnelles : encore que la question de l'hybridation pourrait permettre d'envisager une solution, nous reparlerons d'Abrüpt
- ce qui est écrit n'est pas ce qui est lu partout de la même façon
