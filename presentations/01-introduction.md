## FRA3826 Théories de l'édition numérique

# 01 - Introduction
Séance d'introduction du cours FRA3826 Théories de l'édition numérique.

Antoine Fauchié – [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)

===↓===

## Sommaire de la séance 01

1. préambule&nbsp;: pourquoi utiliser Jitsi ?
2. tour _d'écrans_&nbsp;: présentation de l'enseignant et des étudiant·e·s
3. les types de séances
4. le cours en détail
5. Markdown

===↓===

## 1. Préambule&nbsp;: pourquoi utiliser Jitsi ?


===note===
Ce cours à la vocation de donner une certaine culture numérique, et à ce titre il est nécessaire de nous interroger sur l'usage des logiciels, services, plateformes ou programmes que nous croisons tous les jours.

Pour utiliser un raccourci sur lequel nous pourrions débattre, il y a une grande différence entre utiliser Zoom et Jitsi, de la même façon qu'il y a une différence énorme entre utiliser un traitement de texte (comme Word) ou un éditeur de texte sémantique (comme Stylo).
Dans le premier cas il s'agit d'une utilisation _aveugle_, alors que dans le second il s'agit d'une utilisation ouverte.

À situation exceptionnelle mesures exceptionnelles, si vous avez besoin de vous absentez pendant la séance vous pouvez évidemment le faire.
Si vous avez une connexion de mauvaise qualité vous pouvez couper votre caméra.

===↓===

## 2. Tour d'écrans
Présentez-vous en répondant à ces 4 questions&nbsp;:

- votre prénom
- votre formation (précédente et actuelle)
- quel est le livre le plus proche de vous à cet instant ?
- quelles sont vos attentes pour ce cours ?

===note===
Je m'appelle Antoine, j'entame actuellement ma deuxième année de doctorat au Département des littératures de langue française de l'Université de Montréal (mes recherches portent sur l'influence des façons de faire des livres).
J'ai choisi ...
Mes attentes pour ce cours sont assez grandes, j'espère pouvoir transmettre des connaissances accumulées depuis de nombreuses années, et surtout j'espère être déstabilisé, surpris, et constituer une communauté autour d'enjeux qui me passionnent (une communauté hétéroclite).

===↓===

## 3. Les types de séances

- séances synchrones (6)&nbsp;: 2h de cours en vidéoconférence (9h30-11h30), présence obligatoire, participation attendue<!-- .element: class="fragment" -->
- permanences (7)&nbsp;: 2h de permanence (9h30-11h30), non-obligatoire, pour répondre à vos questions, échanger sur les cours asynchrones, discuter de votre projet d'édition numérique, etc.<!-- .element: class="fragment" -->
- cours asynchrones à écouter (5)&nbsp;: baladodiffusions de 30 minutes environ, à écouter les semaines sans séance synchrone<!-- .element: class="fragment" -->
- cours asynchrones à lire (3)&nbsp;: quelques supports de cours, à lire les semaines sans séance synchrone<!-- .element: class="fragment" -->

===note===
Les séances synchrones sont obligatoires. La Covid implique une réorganisation pour tous le monde, mais vous êtes censé·e·s être disponibles à l'horaire prévu du cours (initialement 8h30-11h30).
Je prévois 2h pour chaque séance synchrone, dans les faits je pense qu'elles dureront environ 1h30, je préfère que vous bloquiez un créneau de 2h pour nous laisser du temps si les échanges débordent.

Les évaluations, dont nous reparlerons dans quelques minutes, s'appuieront sur les cours synchrones et asynchrones.

Tout cela est susceptible d'être modifié, par exemple il est possible qu'il y ait plus de baladodiffusions (podcasts) et moins de cours à lire.

===↓===

## 4. Le cours en détail

### Objectifs
> Acquérir une culture numérique dans le domaine de l'édition tout en faisant le lien avec des études littéraires.

===note===
Identifier les éléments de rupture ou de continuité qui peuvent caractériser le passage au numérique, en comparant différentes formes éditoriales : traditionnelles issues de la culture de l’imprimé, et récentes émergeant de la culture du numérique. Porter un regard critique sur les enjeux sociaux, culturels, politique et juridiques dans les changements de pratiques de lecture et d’écriture, pour comprendre les nouvelles structurations, publications, diffusions et légitimations des contenus sur Internet.

À la fin du cours, les étudiant·e·s seront en mesure :

- de comprendre et d’expliquer les notions fondamentales de l’édition numérique ;
- d’identifier les outils et objets relevant de l’édition numérique ;
- de développer une pensée critique sur les différentes théories du numérique ;
- de prendre en main plusieurs outils et de développer leurs propres usages de ces outils.

===↓===

## 4. Le cours en détail

### Plan de cours 1/2

| Date       | Cours  | Intitulé                         | Texte à lire |
|------------|--------|----------------------------------|--------------|
| 04/09/2020 | 1      | Cours synchrone&nbsp;: Introduction   |  |
| 11/09/2020 | 2      | Cours synchrone&nbsp;: Pour une définition de l'édition | (Epron et Vitali-Rosati, 2018) |
| 18/09/2020 | 3      | Permanence + Cours asynchrone à écouter&nbsp;: Qu'est-ce que le numérique ? 1/3 | (Berners-Lee, 1989) |
| 25/09/2020 | 4      | Permanence + Cours asynchrone à écouter&nbsp;: Qu'est-ce que le numérique ? 2/3 | (Boulétreau et Habert, 2017) |
| 02/10/2020 | 5      | Permanence + Cours asynchrone à écouter&nbsp;: Qu'est-ce que le numérique ? 3/3 | (Vandendorpe, 1999) |
| 09/10/2020 | 6      | Cours synchrone&nbsp;: Introduction aux humanités numériques | (Mounier, 2018) |
| 16/10/2020 | 7      | Permanence + Cours asynchrone à lire&nbsp;: Édition, politique et société | (Morozov, 2017) |
| 19/10/2020 |        | **Mise en ligne du sujet de l'évaluation de mi-session** |  |
| 24/10/2020 |        | **Date limite de Dépôt par les étudiants de l'évaluation de mi-session** |  |

<!-- .element style="font-size:0.8em" -->

===↓===

## 4. Le cours en détail

### Plan de cours 2/2

| Date       | Cours  | Intitulé                         | Texte à lire |
|------------|--------|----------------------------------|--------------|
| 30/10/2020 | 8      | Cours synchrone&nbsp;: Livre numérique et lecture numérique, qu'est-ce qui change ? | (Mod, 2018) |
| 06/11/2020 | 9      | Permanence + Cours asynchrone à écouter&nbsp;: Qu'est-ce que l'édition numérique 1/2 ? | (Epron et Vitali-Rosati, 2018) |
| 13/11/2020 | 10     | Permanence + Cours asynchrone à écouter&nbsp;: Qu'est-ce que l'édition numérique 2/2 ? | (Bourassa et al., 2018) |
| 20/11/2020 | 11     | Cours synchrone&nbsp;: L'édition numérique littéraire et l'édition numérique savante | (Saemmer, 2007) |
| 27/11/2020 | 12 + 13 | Permanence + Cours asynchrones à lire&nbsp;: Qu'est-ce que l'éditorialisation ? Chaînes éditoriales et systèmes de publication | (Vitali Rosati, 2016) + (Fauchié et Parisot, 2018) |
| 04/12/2020 | 14     | Cours synchrone&nbsp;: Enjeux contemporains et à venir de l'édition numérique | _à déterminer_ |
| 16/12/2020 |        | **Date limite de Dépôt par les étudiants du projet d'édition numérique** |  |

<!-- .element style="font-size:0.8em" -->

===↓===

## 4. Le cours en détail

### Fonctionnement

- **participation** : pendant les séances synchrones
- **lecture** : tous les textes indiqués doivent être lus (_avant_ les séances)
- **prise de notes collaborative** : à partir de la seconde séance synchrone via Codimd (https://demo.codimd.org/)
- **participation à un site de ressources** : projet optionnel

===note===
Même si ce cours se déroule à distance il est important que vous vous impliquiez, y compris pendant les séances synchrones que j'espère les plus vivantes possibles ! Donc j'espère être coupé régulièrement !

Nous annoterons progressivement les textes, en utilisant le service en ligne Hypothesis (vous pouvez déjà vous créer un compte). Ces annotations seront évaluées.

===↓===

## 4. Le cours en détail

### Évaluations

- 20% : participation aux cours synchrones, annotations et ressources du cours<!-- .element: class="fragment" -->
- 40% : questionnaire (travail individuel)<!-- .element: class="fragment" -->
- 40% : projet d'édition numérique (travail individuel)<!-- .element: class="fragment" -->

===note===
Deux précisions importantes :

===↓===

## 4. Le cours en détail

### Bibliographie
https://www.zotero.org/groups/2547692/fra3826_-_thories_de_ldition_numrique/library

===note===
Si vous n'utilisez pas encore Zotero je vous recommande vivement de télécharger ce logiciel et de vous créer un compte !

===↓===

## 5. Markdown

- Markdown est un _langage de balisage léger_
- en quelques balises Markdown permet de structurer un document : niveaux de titre, emphases, listes, liens, notes, etc.
- le principe de Markdown : simplifier le langage HTML
- Markdown est très utilisé dans des outils d'écriture ou d'édition

===↓===

## 5. Markdown

| Markdown             | Rendu graphique         | Balisage HTML                    |
|----------------------|-------------------------|----------------------------------|
| `Un texte _en italique_` | Un texte _en italique_ | `Un texte <em>en italique</em>` |
| `### Titre de niveau 3` | **Titre de niveau 3** | `<h3>Titre de niveau 3</h3>` |
| `Un [lien](https://www.unlien.org)` | Un [lien](https://www.unlien.org) | `Un <a href="https://www.unlien.org">lien</a>` |
| `Un texte **en gras**` | Un texte **en gras** | `Un texte <strong>en gras</strong>` |

<!-- .element style="font-size:0.75em" -->

===↓===

## 5. Markdown
Quelques exemples d'application de Markdown :

- CodiMD<!-- .element: class="fragment" -->
- Stylo<!-- .element: class="fragment" -->
- Zettlr<!-- .element: class="fragment" -->
- mais aussi : cette présentation, des services de messagerie (Messenger par exemple), des CMS, etc.<!-- .element: class="fragment" -->

===↓===

## Évaluation — Travail final
**Deux sujets au choix :**<!-- .element: class="fragment" -->

- analyse d'une publication numérique<!-- .element: class="fragment" -->
- descriptif d'un projet d'édition numérique<!-- .element: class="fragment" -->

**Contraintes dans les deux cas :**<!-- .element: class="fragment" -->

- utiliser le cours et la bibliographie pour argumenter<!-- .element: class="fragment" -->
- structurer le document en parties et sous-parties<!-- .element: class="fragment" -->
- proposer un texte entre 9000 et 15000 signes<!-- .element: class="fragment" -->
- utilisation de Stylo ou d'un outil similaire pour la production du document<!-- .element: class="fragment" -->

**Dates importantes**<!-- .element: class="fragment" -->

- **6 novembre** : date limite d'envoi de la proposition de sujet

<!-- .element: class="fragment" -->

- **20 novembre** : date limite de validation du sujet

<!-- .element: class="fragment" -->

- **16 décembre** : date limite de dépôt

<!-- .element: class="fragment" -->

===↓===

## Évaluation — Travail final
### Exemple de structure pour l'analyse d'une publication numérique

- description courte
- contexte de la publication
- résumé du contenu
- descriptions technique et ergonomique
- stratégie éditoriale et commerciale
- comparaison avec d'autres publications numériques
- analyse critique (avec l'appui de références issues du cours ou de la bibliographie)
- conclusion

===↓===

## Évaluation — Travail final
### Exemple de structure pour le descriptif d'un projet d'édition numérique

- présentation courte
- objectifs de la publication : type de contenu, public visé, fonctionnalités
- appuis théoriques : justification des choix à partir de références (cours ou bibliographie)
- comparaison avec d'autres publications numériques
- choix techniques
- choix ergonomiques
- calendrier de réalisation
- conclusion
