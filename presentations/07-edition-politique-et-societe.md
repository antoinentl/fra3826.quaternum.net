## FRA3826 Théories de l'édition numérique

# 07 - Numérique, politique, société, littérature

===note===
L'objectif de cette séance est de survoler plusieurs thématiques liées au numérique, à partir de cas concrets que nous pouvons toutes et tous avoir connaissance.
Ou plutôt dont nous avons fait l'expérience.
Mon but à peine caché est de démonter le solutionnisme technologique (donc que l'usage de la technologie à tout bout de champ c'est un peu fatiguant).
Pourquoi ?
Pour tenter de vous prouver que la littérature et la technologie c'est une vieille histoire, et qu'il peut être pertinent de recourir à la technologie pour créer et diffuser la littérature.

C'est une séance qui se veut courte, pour nous laisser du temps pour réviser les premières séances.

===↓===

## Sommaire de la séance 07 - Édition, politique et société

1. Numérique et ouverture
2. Numérique et enjeux sociaux
3. Numérique et littérature
4. Révisions
5. Stylo

===note===
Ce plan peut paraître particulièrement ambitieux, nous allons passer rapidement sur ces diptyques.

===↓===

## 1. Numérique et ouverture

===note===
Le duo _numérique_ et _ouverture_ peut être compris de plusieurs façons.
Ce qui nous intéresse ici c'est de comprendre comment un domaine comme l'édition s'est constitué dans un environnement numérique qui est loin d'être homogène.
Cet environnement numérique est aussi économique, c'est probablement un des principaux angles morts de la série sur Qu'est-ce que le numérique.
Dès sa création le Web est une invention économique.
Économie de la connaissance comme vous l'avez compris, mais aussi économie financière.
Tim Berners-Lee a par exemple essayer de vendre le concept du Web à Microsoft, qui a refusé d'investir dans ce qui semblait alors un projet peu intéressant pour la grande firme américaine.
Grâce à cette immense erreur de jugement, le Web a pu devenir ce qu'il est — ou plutôt ce qu'il a été, car le Web n'est plus si ouvert...

En ce qui concerne _numérique et ouverture_, nous pouvons parler de deux phénomènes : le logiciel libre et le libre accès.

===↓===

### 1.1. Le mouvement du logiciel libre

- liberté d'usage d'un programme :<!-- .element: class="fragment" -->
  0. la liberté d'exécuter le programme, pour tous les usages ;<!-- .element: class="fragment" -->
  1. la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins ;<!-- .element: class="fragment" -->
  2. la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies) ;<!-- .element: class="fragment" -->
  3. la liberté d'améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté ;<!-- .element: class="fragment" -->
- une autre façon d'envisager l'économie : plus value et service, plutôt que brevet ;<!-- .element: class="fragment" -->
- publier pour partager, faire connaître, contribuer, dupliquer ;<!-- .element: class="fragment" -->
- le coût du logiciel libre (ou open source) : Sur quoi reposent nos infrastructures numériques ? de Nadia Eghbal (books.openedition.org/oep/1797).<!-- .element: class="fragment" -->

===note===
Nous l'avons déjà vu, le logiciel libre est à la fois un usage et une approche philosophique.
En terme d'usage cela implique la gratuité même si ce n'est pas le premier objet du logiciel libre : il s'agit avant tout de la liberté de l'utilisateur ou de l'utilisatrice (même si cette liberté implique dans la grande majorité des cas une gratuité d'accès et d'usage).
Je préfère parler de logiciel libre que de logiciel open source, le logiciel libre propose un projet moins centré sur le business et est en partie moins capitaliste.

Pour vous donner un exemple concret, le professeur Marcello Vitali-Rosati que vous avez déjà pu lire et qui enseigne au Département des littératures de langue française, ne souhaite pas utiliser du matériel qui repose sur du logiciel propriétaire.
Prenons l'exemple de la carte wifi de nos ordinateurs portables.
Pour fonctionner avec le système d'exploitation (probablement Mac ou Windows pour vous, peut-être un peu Linux pour certains et certaines d'entre vous, en tout cas c'est mon cas), la carte wifi utilise un petit programme appelé pilote.
Nombreux sont les pilotes propriétaires, ce qui signifie que vous ne pouvez pas savoir ce que fait le programme puisque sa licence ne vous autorise pas à savoir comment il fonctionne.
Et donc vous ne savez pas ce que fait ce programme, ce pilote.
Pendant plusieurs mois Marcello a préféré brancher une antenne wifi dont il savait le pilote libre sur son ordinateur portable, plutôt qu'utiliser le wifi de son ordinateur.

Le logiciel libre est aussi fondamentalement une autre façon d'envisager l'économie : les structures qui proposent du logiciel libre, donc un _produit_ potentiellement gratuit, vont proposer une plus value et des services associés.
Je m'explique : plutôt que de proposer une solution numérique fermée et payante, qui sera probablement la même pour tous le monde, une structure qui produit un logiciel libre pourra travailler avec un client pour développer de nouvelles fonctionnalités, à sa demande.
Le client ne vient donc plus pour acheter un logiciel mais pour avoir un nouveau service.
Les sociétés qui produisent du logiciel libre peuvent également baser leurs revenus sur de la maintenance de produit, ou sur des services associés.
Par exemple l'outil le plus utilisé pour créer des sites web, WordPress, est un logiciel libre.
La société qui le développe, Automattic, propose des services comme de l'hébergement de sites.

Le logiciel libre implique une pratique de publication du code.
Il n'y a rien d'obligatoire mais si vous créez un programme et que vous souhaitez que celui-ci soit libre, vous allez le mettre à disposition pour que d'autres personnes l'utilisent, voir y contribuent pour l'améliorer.
Il y a aussi beaucoup de programmes libres dupliqués, parce que les utilisateurs ou utilisatrices souhaitaient changer _légèrement_ les fonctionnalités.
Grâce au logiciel libre ils et elles ont simplement à copier le programme et à le modifier.
On parle aussi de forker un programme, nous reviendrons sur ce terme propre au développement informatique et au système de version Git.

En théorie cette idée que n'importe qui peut créer des programmes est très enthousiasmant, imaginez toutes ces petites mains qui créent des programmes informatiques et les partagent librement (dans tous les sens du terme).
En pratique si vous développez un programme informatique et que vous attirez de nombreux contributeurs et contributrices, cela peut vite devenir complexe à gérer.
Cela devient un vrai problème.
C'est pourquoi 

===↓===

### 1.2. Le libre accès

> L’idée de base de l’accès ouvert est simple : faire en sorte que la littérature scientifique soit disponible en ligne sans barrières liées au prix et sans la plupart des contraintes dues à des autorisations.  
> Peter Suber, _Qu’est-ce que l’accès ouvert ?_ ([https://books.openedition.org/oep/1600](https://books.openedition.org/oep/1600))

===note===
Le libre accès, ou accès ouvert, ou encore _open access_, est en quelque sorte un mouvement héritier du logiciel libre.
Pour l'open access ou libre accès, il s'agit de la question de la diffusion des contenus.
Il ne s'agit pas d'omettre les conditions de productions (ici de textes scientifiques, et non plus de programmes, mais vous noterez que nous restons dans le texte), mais d'ouvrir par défaut la diffusion et en partie la réutilisation.

Pour comprendre ce qu'est le libre accès il faut faire un rapide point sur le fonctionnement de la publication scientifique, _actuellement_.
Un ou une chercheuse écrit un papier, un article par exemple, le propose à une revue.
La revue met en place un certain nombres de protocoles éditoriaux pour _éditer_ un texte :

- un auteur (chercheur) soumet son texte à un éditeur ;
- l'article est relu et commenté par des relecteurs et relectrices, qui sont des pairs, c'est-à-dire des scientifiques, comme la personne qui soumet son texte ;
- en fonction des retours l'auteur ou l'autrice doit corriger son texte ;
- l'éditeur publie le texte dans sa revue, et la rend disponible sous différentes formes : imprimée ou numérique ;
- les lecteurs et lectrices peuvent lire l'article moyennant un accès souvent payant, le coût étant souvent pris en charge par le laboratoire ou la bibliothèque ;
- souvent au terme d'un embargo de quelques années l'article sera disponible _librement_.

Donc si on résume à grands traits : un chercheur déjà payé, par des fonds publics, pour écrire des articles, va voir son article générer des bénéfices pour un éditeur qui aura certes assuré le travail de mise en forme et peut-être de diffusion, mais qui ne rémunère pas l'auteur ou l'autrice, pas plus que les relecteurs ou les relectrices.

Peut-être que vous avez pu noter le fait que certains distributeurs ont _ouverts_ leurs accès pendant la pandémie, mais seulement temporairement.
L'objectif, louable, était de permettre aux chercheurs de partager leurs résultats de recherche.
Oui c'est ce qui devrait être tous le temps le cas, sans frein financier.

Je ne vais pas vous parler de Sci-Hub, qui est une alternative illégale, sur laquelle la grande majorité de la production scientifique mondiale est diffusée librement.
Voilà je ne vous ai pas parlé de Sci-Hub:

===↓===

## 2. Numérique et enjeux sociaux

===note===
C'est un bien grand mot que _enjeux sociaux_, ce qui m'intéresse ici c'est plutôt la question de l'usage du numérique dans l'espace public et plus spécifiquement en société.

===↓===

### 2.1. Vous avez dit déconnexion ?
"Le prix de la déconnexion" par Evgeny Morozov ([https://blog.mondediplo.net/2017-02-23-Le-prix-de-la-deconnexion](https://blog.mondediplo.net/2017-02-23-Le-prix-de-la-deconnexion)) :

- se _déconnecter_ est une action désormais plurielle ;

<!-- .element: class="fragment" -->

- une injonction à la connexion ;

<!-- .element: class="fragment" -->

- les tentatives pour réguler nos usages semblent vaines ;

<!-- .element: class="fragment" -->

- la question de l'_uberisation_ et des microtâches ;

<!-- .element: class="fragment" -->

- le luxe du choix des plateformes et des services.

<!-- .element: class="fragment" -->

===note===
Il n'y a pas une déconnexion : par exemple en fonction des activités (travail, études), en distinguant les relations sociales diverses, et en prenant en compte certaines obligations (médecin, école, etc.).

Il y a une injonction à la connexion, et ce qui se passe à l'Université depuis plusieurs années en est un bon exemple, même en dehors de cette pandémie.
Presque toutes nos activités peuvent passer par des écrans connectés à un moment où un autre.

Réguler nos usages n'est pas simple : soit parce que les plateformes et les services que nous utilisons nous laissent peut de répit, soit parce que nous avons envie ou même besoin de rester connecter.

Et enfin il faut noter que choisir nos outils de communication, qui impliquent une connexion, est un luxe.
Parce qu'il faut déjà connaître ces alternatives aux grandes plateformes comme Facebook, Messenger, Whatsapp, Skype, Instagram, etc.
Ensuite il faut savoir les utiliser, voir apprendre aux autres _comment_ les utiliser.
Pour cela je vous renvoie à l'initiative d'un étudiant du Département des littératures de langue française (Louis-Olivier Brassard) : 31 raisons (de quitter Facebook) et 10 solutions pour s’en sortir ([https://byebyefacebook.loupbrun.ca](https://byebyefacebook.loupbrun.ca)).

Autant dire que tout cela semble presque désuet vu notre situation actuelle...
Vous, en tant qu'étudiants et étudiantes, vous vous retrouvez à devoir vous _connecter_ pour suivre des cours, tout en étant bien obligé de consulter régulièrement vos messages et courriels diverses.
Et tout en subissant les soubresauts des plateformes numériques qui nous contraignent, nous devons le reconnaître...

===↓===

### 2.2. Solutionnisme technologique

> Ce qui pose problème n'est pas les solutions qu'ils proposent, mais plutôt leur définition même de la question.  
> Evgeny Morozov, _Pour tout résoudre cliquez ici_, p. 18

===note===
La question du _solutionnisme technologique_ est largement traitée dans le livre d'Evgeny Morozov _Pour tout résoudre cliquez ici_.
Le "postulat" de ce livre est le suivant : en vantant les mérites de l'"efficacité, de la transparence, et de la certitude et de la perfection", et en supprimant "les tensions, l'opacité, l'ambiguïté et l'imperfection", la Silicon Valley limite notre capacité réflexive, et souhaite nous "enfermer" dans une prison numérique.
L'objectif du livre est de définir le "coût réel" de l'utopie, du "paradis" que dessine la Silicon Valley, et d'expliquer pourquoi nous ne l'avons pas défini plus tôt.

Une démarche solutionniste cherche à répondre à un problème _avec_ la technologie avant de l'avoir bien défini.

L'exemple parfait pour comprendre cela est l'apparition d'applications pour téléphone censé endiguer l'épidémie de Covid-19.
Quel est l'objectif ?
Permettre une identification des personnes contaminées pour éviter qu'elle rentre en contact avec d'autres personnes et ainsi limiter la propagation.
Sans outil numérique cela requiert un travail humain long et fastidieux (mais efficace si on y met les moyens).
Penser qu'une application va nous sauver de cette situation est peut-être un leurre, car pour que cela fonctionne il faut déjà plusieurs paramètres : le principal étant que tous le monde utilise l'application, et donc que tous le monde ait un téléphone intelligent.
Ensuite il faut s'assurer que l'application va faire exactement ce pour quoi elle a été conçu, ce qui n'était pas tout à fait le cas au début puisque quelques lignes de codes malencontreusement oubliées par l'un des développeurs envoyaient les coordonnées de chaque utilisateur et utilisatrice aux serveurs de Google.
Fort heureusement le programme a depuis été corrigé ([voir le problème en ligne](https://github.com/cds-snc/covid-alert-app/issues/1003)).

La technologie peut aussi proposer un choix à l’humain qui l’utilise, et donc déplacer la solution : non plus dans un fonctionnement technique binaire – appuyer sur un bouton ou attendre qu'un voyant s'allume – mais comme processus de décision que va prendre un être conscient et réfléchissant.
En bref il s'agit de remplacer le solutionnisme par le fonctionnalisme, comme le suggère Evgeny Morozov.

===↓===


## 3. Numérique et littérature

===↓===

### 3.1. Écriture numérique

- les outils d'écriture ;<!-- .element: class="fragment" -->
- l'écriture en réseau ;<!-- .element: class="fragment" -->
- le carnet ouvert ;<!-- .element: class="fragment" -->
- etc.<!-- .element: class="fragment" -->

===note===
Il s'agit ici de l'usage de l'informatique et de logiciels _pour écrire_.

Je vous renvoie à nouveau au livre de Matthew Kirschenbaum, _Track changes_, une enquête sur les pratiques d'écriture des auteurs et des autrices de littérature.
C'est une histoire des traitements de texte sous l'angle de la littérature, et on y constate que l'outil a une grande influence sur la façon d'écrire.
Un des exemples est le cas de George R R Martin et son utilisation d'un programme appelé WordStar, via un ordinateur qui n'est pas connecté à Internet.

L'écriture en réseau c'est soit le fait d'écrire en ligne, donc potentiellement à la vue de tous, soit le fait d'écrire à plusieurs.

===↓===

<!-- .slide: data-background="medias/wordstar.png" data-background-size="contain" -->

===↓===

### 3.2. Littérature informatique ?

- l'usage de l'hypertexte ;<!-- .element: class="fragment" -->
- l'interactivité ;<!-- .element: class="fragment" -->
- la maîtrise de la chaîne éditoriale (de l'écriture à la diffusion).<!-- .element: class="fragment" -->

===note===
Après la question de l'écriture, c'est celle de la production d'un artefact qui s'est posée pour les auteurs et autrices de littérature.
Le numérique a amené de nouvelles possibilités, c'est-à-dire produire des objets jusqu'ici impossible à réaliser avec le papier.
hypertexte et interactivité

Mais une nouvelle dimension : le fait de maîtriser de bout en bout la chaîne éditoriale, sans dépendre d'un éditeur.
Pour le meilleur et pour le pire : notamment le fait que beaucoup d'œuvres sont restées inconnues, et que la grande majorité des expérimentations du début des années est désormais perdue, ou peut-être archivées quelque part.
L'utilisation massive de Flash fait que certains sites (s'ils sont encore en ligne) seront très bientôt illisibles.
explication de flash

===↓===

<!-- .slide: data-background="medias/serial-letters.png" data-background-size="contain" -->

===↓===

<!-- .slide: data-background="medias/24h-adrian.png" data-background-size="contain" -->

===↓===


## Révisions

===↓===

### Révisions : définition de l'édition

Question : quelle est la différence entre l'édition et l'éditeur·trice ?<!-- .element: class="fragment" -->

- distinction entre le processus et la structure<!-- .element: class="fragment" -->
- édition : processus<!-- .element: class="fragment" -->
- éditeur·trice : peut être une instance éditoriale<!-- .element: class="fragment" -->

===↓===

### Révisions : qu'est-ce que le numérique

Question : quelle est la filiation du Web avec d'autres technologies de l'information ?<!-- .element: class="fragment" -->

- principe dans l'histoire des technologies de l'information : évolution et non rupture nette<!-- .element: class="fragment" -->
- le Web est un service d'Internet<!-- .element: class="fragment" -->
- le Web utilise des infrastructures déjà présentes (câbles sous-marins par exemple)<!-- .element: class="fragment" -->

===↓===

### Révisions : qu'est-ce que le numérique

Question : quelles sont les principales particularités du format HTML<!-- .element: class="fragment" -->

- deux points importants : langage de balisage et utilisation de l'hypertexte<!-- .element: class="fragment" -->
- format standardisé et maintenu par le W3C<!-- .element: class="fragment" -->
- format rétro-compatible<!-- .element: class="fragment" -->

===↓===

### Révisions : humanités numériques

Question : comment peuvent être qualifiés Internet et le Web en ce qui concerne le développement des humanités numériques ?<!-- .element: class="fragment" -->

- ce sont des _accélérateurs_

<!-- .element: class="fragment" -->

- Internet et le Web contribuent à une convergence des humanités et du numérique

<!-- .element: class="fragment" -->

- Internet et le Web permettent une diffusion rapide des réflexions

<!-- .element: class="fragment" -->

===↓===

### Révisions : question longue
Quel rôle a joué l'édition et l'édition numérique dans la constitution des humanités numériques ? Vos propos doivent être accompagnés de références et d'exemples illustratifs.

- place de la diffusion dans les humanités (numériques)<!-- .element: class="fragment" -->
- élément structurant<!-- .element: class="fragment" -->
- l'édition est un domaine pluriel<!-- .element: class="fragment" -->
- l'édition revêt une dimension réflexive<!-- .element: class="fragment" -->


===↓===

## Stylo
