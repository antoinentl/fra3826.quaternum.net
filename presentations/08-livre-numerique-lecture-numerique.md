## FRA3826 Théories de l'édition numérique

# 08 - Livre numérique et lecture numérique, qu'est-ce qui change ?

===note===
**Enregistrement.**

===↓===

# Préambule

- stage à la Chaire de recherche du Canada sur les écritures numériques : 3 crédits à l'hiver pour les étudiants en littératures de langue française (équivalent ) ;
- mercredi 4 novembre de 9h à 11h : rencontre **Les nouveaux visages de l'édition savante** (sur inscription).

===note===
Concernant vos évaluations de la semaine dernière, l’abandon sera permis jusqu’au 13 novembre, mais je vous transmettrai vos notes avant la prochaine séance qui sera asynchrone.

===↓===

# 08 - Livre numérique et lecture numérique, qu'est-ce qui change ?

===note===
Cette séance va être un peu dense, 

===↓===

## Sommaire de la séance 08 - Livre numérique et lecture numérique, qu'est-ce qui change ?

1. Définissons le livre avant de définir le livre numérique<!-- .element: class="fragment" -->
2. Livre numérique : définition légale et définition économique<!-- .element: class="fragment" -->
3. Le livre (numérique) et ses usages : la lecture numérique<!-- .element: class="fragment" -->
4. La littérature numérique ?<!-- .element: class="fragment" -->

===note===


===↓===

## 1. Définissons le livre avant de définir le livre numérique

===↓===

### 1.1. Quelle est votre définition du livre ?

===note===

===↓===

### 1.2. La difficulté de définir le livre
Sur quels éléments pouvons-nous nous baser ?

- matériel : un objet ;<!-- .element: class="fragment" -->
- physique : du papier ou des octets ;<!-- .element: class="fragment" -->
- contenu : ce qu'il comporte ;<!-- .element: class="fragment" -->
- économique : quel régime fiscal ? quel lieu de vente ?<!-- .element: class="fragment" -->

===↓===

### 1.2. La difficulté de définir le livre
Deux pistes (québécoises) :

- [Loi du livre](https://www.mcc.gouv.qc.ca/index.php?id=4436#c34150) : nombre de pages (reliées), genre, publications en série, etc.
- [Dépôt légal (BAnQ)](https://www.banq.qc.ca/services/bibliotheque_nationale/depot_legal/pub_assujetties.html) : nombre de pages (reliées), signes, non périodiques.

===note===
On constate le flou et la difficulté de normaliser quelque chose qui nous échappe.

===↓===

### 1.3. L'artefact
Proposition :

**Le livre est un objet numérique ou papier, produit via un processus d'édition, dans l'objectif de diffuser des contenus dans un espace économique.**<!-- .element: class="fragment" -->

===note===
Il faut qu'il y ait production, ainsi qu'édition.
Je fais le choix ici de déplacer la définition du processus d'édition de Benoît Epron et Marcello Vitali-Rosati vers le livre.

Les termes suivants sont importants :

- objet : quelque chose tangible (le numérique est tangible, certes différemment du papier), quelque chose de manipulable et de partageable ;
- production : il y a un geste, c'est un objet qui résulte d'une _fabrication_. "Fabriquer, cela signifie d’abord manipuler et détourner quelque chose qui fait partie du donné, le changer en artefact et le tourner vers l’application pratique." (Vilèm Flusser, _Petite philosophie du design_) ;
- diffuser : il y a une intention de rendre accessible un contenu, il y a une intention de faire voir ou lire (sentir ?) un contenu choisi et organisé ;
- contenus : il y a un propos _dans_ le livre (en plus du propos que représente l'existence du livre lui-même) ;
- économie : il ne s'agit pas forcément d'une économie financière, il peut s'agir d'une économie de la connaissance. Il faudrait trouver un terme plus générique, il s'agit d'un écosystème dans lequel les interactions concernent le livre.

===↓===

### 1.4. Des grands écarts

- littérature jeunesse : quelques pages colorées et reliées sans texte constituent-elles un livre ?<!-- .element: class="fragment" -->
- fanzine : une brochure en un seul exemplaire est-elle un livre ?<!-- .element: class="fragment" -->
- site web : à partir de quel moment pouvons-nous considérer un site web comme un livre ?<!-- .element: class="fragment" -->
- une lettre d'information ?<!-- .element: class="fragment" -->
- etc.<!-- .element: class="fragment" -->

===note===
Malgré tous les efforts possibles pour trouver une définition qui nous convienne et sur laquelle nous pourrions faire consensus, il y aura toujours des cas à part.
Vous avez d'ailleurs probablement d'autres exemples.

===↓===

## 2. Livre numérique : définition légale et définition économique

===note===
C'est une façon détournée de tenter de trouver un appui pour pouvoir définir le livre numérique : aborder des questions soit-disant concrètes comme la dimension légale ou la perspective économique.

===↓===

### 2.1. Quelle est votre définition du livre numérique ?

===note===
Je suis aussi preneur de votre point de vue selon lequel un livre numérique (quel qu'il soit) ne serait pas un livre.
Nous parlerons tout à l'heure de vos pratiques de _lecture numérique_.

===↓===

### 2.2. Définition légale (française)
> Définition : Ouvrage édité et diffusé sous forme numérique, destiné à être lu sur un écran.

> Note :

>1. Le livre numérique peut être un ouvrage composé directement sous forme numérique ou numérisé à partir d'imprimés ou de manuscrits.
>2. Le livre numérique peut être lu à l'aide de supports électroniques très divers.
>3. On trouve aussi le terme « livre électronique », qui n'est pas recommandé en ce sens.

>Voir aussi : liseuse.  
>Équivalent étranger : e-book, electronic book.

> Journal officiel du 4 avril 2012 ([source](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000025627105))


===note===
Cette définition arrive tardivement, en 2012, alors que l'arrivée du livre numérique peut être datée à 2006-2007.
Cela signifie que pendant plus de 6 ans il n'y a eu aucun encadrement légal du livre numérique en France.

Quelques points saillants :

- "sur un écran" : aucune précision n'est faite concernant un dispositif particulier. Que ce soit un écran de télévision ou une montre connectée, il doit s'agir avant tout d'un écran. La liseuse n'est par exemple pas évoquée. D'ailleurs, qu'est-ce qu'une liseuse ?

===↓===

### Aparté : la liseuse à encre électronique
La liseuse, ou _e-reader_, est un appareil mobile principalement destiné à lire des livres numériques, et équipé d'un écran à encre électronique.

===↓===

<!-- .slide: data-background="medias/liseuse.png" data-background-size="contain" -->

===↓===

### Aparté : la liseuse à encre électronique

- encre électronique : énergie nécessaire uniquement pour changer l'état de l'écran ;
- confort de lecture ;
- grande autonomie ;
- grande capacité de stockage ;
- dispositif → système d'exploitation → logiciel de lecture → fichiers EPUB

===↓===

### 2.2. Définition légale (française)
> Définition : Ouvrage édité et diffusé sous forme numérique, destiné à être lu sur un écran.

> Note :

>1. Le livre numérique peut être un ouvrage composé directement sous forme numérique ou numérisé à partir d'imprimés ou de manuscrits.
>2. Le livre numérique peut être lu à l'aide de supports électroniques très divers.
>3. On trouve aussi le terme « livre électronique », qui n'est pas recommandé en ce sens.

>Voir aussi : liseuse.  
>Équivalent étranger : e-book, electronic book.

> Journal officiel du 4 avril 2012 ([source](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000025627105))


===note===

Autres points saillants :

- nativement numérique ou issu d'une numérisation ;
- supports électroniques très divers : encore une fois ce n'est pas la liseuse qui est plebiscitée ;
- distinction entre le dispositif de lecture et le livre (le fichier).

===↓===

### 2.3. Le débat européen : bien ou service ?

- bien : acquisition ;<!-- .element: class="fragment" -->
- service : accès temporaire.<!-- .element: class="fragment" -->

===note===
C'est un autre débat qui a débuté au moment où la question fiscale semblait réglée en France et dans d'autres pays europèens.
Le livre, en France, est considéré comme un bien particulier, c'est pour cela qu'il dispose d'une TVA (taxe) spécifique de 5,5%, ainsi que d'un prix unique (via la loi Lang de 1981) contrairement aux différentes tentatives au Québec.
Si le livre numérique est un bien, sa TVA réduite peut être conservée, en revanche s'il s'agit d'un service alors sa TVA passe à 20%.
Ce débat était donc purement économique, mais derrière se jouait un enjeu plus intéressant pour nous : pourquoi pourrait-on considérer qu'un livre numérique est un service plutôt qu'un bien.
**Pourquoi selon vous ?**

La question portait donc également sur les possibilités d'accès au livre numérique, et notamment le sujet des mesures techniques de protection des fichiers, ou DRM en anglais.
Les DRM sont un moyen de protéger un fichier d'éventuels pillage, dans les faits c'est principalement un frein aux usages et au développement économique.
Concrètement, soit vous parvenez à faire fonctionner le DRM et vous n'y pensez plus (une fois votre liseuse connectée à votre compte Adobe par exemple), soit vous ne réussissez pas à utiliser ce qui apparaît comme une complication de plus.
Par exemple j'ai récemment essayé d'ouvrir un _vieux_ livre numérique acheté il y a quelques années, impossible de retrouver mon mot de passe Adobe, ou de le modifier.
J'ai télécharger une version piratée...
Bref tout cela pour dire que vous ne pouvez pas être en possession du fichier du livre numérique, alors vous n'achetez pas un bien mais un _service_.
Dans le cas d'Amazon et de son écosystème Kindle c'est encore pire.

Pour prolonger la réflexion : https://opee.unistra.fr/spip.php?article321, https://www.livreshebdo.fr/article/bien-ou-service-faut-il-choisir et https://www.plateforme-echange.org/Un-livre-electronique-verrouille-par-un-DRM-ne-peut-etre-compare-a-un-livre.


===↓===

## 3. Le livre (numérique) et ses usages : la lecture numérique

===note===
Il faudrait un cours de plusieurs semaines pour aborder ces questions en détail !

===↓===

### 3.1. Quels sont vos pratiques de lecture numérique ?

===note===
Je pourrais plutôt commencer par vous demander quelles sont vos pratiques de lecture (en général et non spécifiquement numériques).

Ensuite, quelles sont vos pratiques de lecture numérique tout écran !

===↓===

### 3.2. Qu'est-ce qu'une disposition de lecture ?

~Lire sur un écran immobile.~

===note===
De façon non scientifique : le test de la couette.
Voir le texte de François Bon _Après le livre_.

===↓===

### 3.3. La lecture numérique homothétique
L'écran à la place de la page imprimée ?

===note===
Répétition des pratiques de lecture issues du livre imprimé, mais avec des dispositifs numériques.

===↓===

### 3.4. La lecture nativement numérique ?
Est-ce qu'il y aurait des formes de lecture _nativement numériques_ ?

===note===
Parlons de l'article de Craig Mod.

À propos de Craig Mod, voir https://walkkumano.com/iseji/

Transition avec la partie 4.

===↓===

## 4. La littérature numérique ?
Quelques exemples :

- [_The Shape of Design_, Frank Chimero, 2012](https://shapeofdesignbook.com)
- [_Koya Bound, Eight Days on the Kumano Kodo_, Craig Mod & Dan Rubin](https://walkkumano.com/koyabound/)
- [_Seed_, Joanna Walsh, 2017](https://seed-story.com)
- [_L'incendie est clos_, Otto Borg, Abrüpt, 2019](https://www.antilivre.org/lincendie-est-clos/)

===note===
Nous reprendrons ces exemples, et d'autres, à la séance 11 L’édition numérique littéraire et l’édition numérique savante dans 3 semaines.

===↓===

## Livre numérique et lecture numérique, qu'est-ce qui change ? Les points essentiels à retenir&nbsp;:

- définir le livre : une entreprise périlleuse et subjective sans critères légaux et économiques ;<!-- .element: class="fragment" -->
- définir le livre numérique : s'inspirer du livre, usages homothétiques ;<!-- .element: class="fragment" -->
- le geste d'édition : en plus des formes et formats, le projet éditorial.<!-- .element: class="fragment" -->

===note===
Prochaine séance asynchrone, mais séance suivante où je vous proposerai de discuter de plusieurs questions liées au cours pendant 1h, je vous redonnerai des informations à ce sujet.
