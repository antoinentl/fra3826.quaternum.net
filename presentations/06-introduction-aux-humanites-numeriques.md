## FRA3826 Théories de l'édition numérique

# 06 - Introduction aux humanités numériques

===note===
L'objectif de ce cours est de vous permettre d'avoir une compréhension de cette approche interdisciplinaire.
Les humanités numériques, comme l'édition numérique, ne sont pas le simple ajout du numérique à un domaine ou une discipline déjà existante, les choses sont plus complexes.
Les humanités numériques sont un bon exemple d'évolution majeure dans nos pratiques et nos réflexions, il est donc important de comprendre ses origines et son panorama de pratiques.

Comme vous vous en doutez, la série de balados sur qu'est-ce que le numérique va grandement nous aider dans ce cours.

===↓===

## Sommaire de la séance 06 - Introduction aux humanités numériques

1. Aux origines : l'automatisation des procédés de recherche
2. Quelques dates importantes
3. Édition, littérature et humanités numériques
4. Introduction à Stylo

===↓===

## 0. Pourquoi aborder les humanités numériques dans ce cours ?

1. les humanités numériques : une approche interdisciplinaire (en SHS)<!-- .element: class="fragment" -->
2. questionnements nécessaires dans les champs de la littérature et de l'édition<!-- .element: class="fragment" -->
3. évolution des processus d'édition et de publication<!-- .element: class="fragment" -->


===note===
1. si toutes les disciplines sont touchées, alors l'édition et la littérature aussi
2. se poser des questions : "la capacité de ce mouvement à poser de bonnes questions aux différentes disciplines des humanités"
3. 

===↓===

## 1. Aux origines : l'automatisation des procédés de recherche

===note===
Nous allons rapidement parler du chapitre que vous deviez lire pour aujourd'hui, mais avant cela parlons rapidement du livre et de son auteur.

===↓===

### 1.1. Pierre Mounier et son livre

- Pierre Mounier : chercheur français impliqué dans l'édition numérique et les humanités numériques

<!-- .element: class="fragment" -->

- l'ouvrage _Les humanités numériques : Une histoire critique_ : portrait des humanités numériques, "champ de recherche autant que champ de bataille"

<!-- .element: class="fragment" -->

- "Réinventer les humanités par le numérique suppose de relever trois défis de taille" : rapport à la technique, politique, scientificité

<!-- .element: class="fragment" -->

===note===
On peut lire sur la page Wikipédia qui le présente que Pierre Mounier est ingénieur de recherche, mais on pourrait dire plus globalement qu'il est _chercheur_.
Il est notamment à l'origine du CLÉO, le Centre pour l'édition électronique ouverte, un centre de recherche qui a donné naissance à OpenEdition en France (puis au niveau européen).
Pierre Mounier a écrit un ouvrage important, _L'édition électronique_ avec Marin Dacos, que je vous conseille de consulter.
J'ai pu assister à plusieurs interventions de Pierre Mounier, et je me souviens très bien de l'une d'entre elles (à Marseille) où il avait expliqué que le seul livre numérique réellement numérique était Wikipédia (pour sa dimension réinscriptible).
Il a participé de façon très active à d'autres initiatives en faveur du livre numérique (vous pouvez d'ailleurs noter l'utilisation de l'expression "livre électronique" par Pierre Mounier, nous reviendrons sur cette particularité propre au domaine universitaire).

Son ouvrage est un très bon résumé de l'origine des humanités numériques et de sa situation actuelle en tant qu'approche interdisciplinaire.
Margot Mellet, doctorante au Département des littératures de langue française, a écrit un compte-rendu sur le site de _Sens public_, si vous ne souhaitez pas lire l'ouvrage dans son ensemble.

Les 3 défis de cet ouvrage :

- rapport à la technique : "Mais l’objectif de ces opérations est-il simplement d’obtenir des résultats plus rapides, plus étendus, plus fiables ? Rien n’est moins sûr, car l’histoire des technologies montre sans équivoque que celles-ci sont toujours chargées des représentations du monde provenant des milieux où elles sont créées."
- politique : sur la question de l'organisation des instances universitaires et des projets de recherche
- scientificité : est-ce que l'introduction de l'informatique et le traitement automatisé de données vont rendre les sciences humaines et sociales plus scientifiques ?

===↓===

### 1.2. L'origine : l'index
En 1949 Roberto Busa utilise des procédés informatiques pour l'analyse de sources textuelles :

- transcription de la _Somme théologique_ de Thomas d’Aquin

<!-- .element: class="fragment" -->

- utilisation de cartes perforées<!-- .element: class="fragment" -->
- constitution d'index (13 millions de fiches)<!-- .element: class="fragment" -->
- une nouvelle approche : utiliser l'informatique pour réaliser des tâches inédites<!-- .element: class="fragment" -->
- le texte devient base de données<!-- .element: class="fragment" -->

===note===
Pierre Mounier explique bien le projet du "Père Busa", soit la transcription de la _Somme théologique_ sur cartes perforées, pour constituer des index des mots et ainsi pouvoir travailler l'occurrence de certaines notions ou concepts.
Il faut vous représenter ces index comme des listes de mots dont la place dans le texte est donnée ; il est ensuite possible de savoir combien de fois telle série de lettres apparaît dans le texte.
Si cela peut paraître très simple aujourd'hui, autant vous dire que ce projet a pris plusieurs d'années à l'époque.

Il s'agit d'une nouvelle approche : "Cette approche utilise l’informatique non pour faciliter ou accélérer le travail du chercheur, mais pour conduire l’analyse à un niveau jusqu’ici inaccessible, permettant de proposer des interprétations inédites."
Le texte se transforme en base de données, il devient un objet.

===↓===

### 1.3. La relation entre industrie et recherche

- IBM utilise ce projet comme moyen de communiquer et pour améliorer son image<!-- .element: class="fragment" -->
- la course de la technique et de l'informatique doit conserver une image humaine<!-- .element: class="fragment" -->
- du traitement automatique des données quantitatives vers le traitement du langage<!-- .element: class="fragment" -->
- le duo chercheur-ingénieur<!-- .element: class="fragment" -->

===note===
Il convient d'interroger la relation entre domaine de recherche et industrie : pourquoi une entreprise américaine comme IBM voulait-elle s'impliquer (y compris financièrement) dans ce projet ?
Pierre Mounier l'explique mieux que moi, mais je voudrais insister sur un point en particulier : le fait que l'après-guerre est un moment clé dans l'industrialisation technique, et que IBM profite de ce projet _humaniste_ pour prouver qu'il y a une "continuité établie entre l’homme et la machine" (pour citer Pierre Mounier).
Une sorte de _greenwashing_ au moment où des critiques se font entendre contre l'informatisation galopante (voir notamment Jacques Ellul puis Gilbert Simondon).

Et il faut noter aussi le déplacement d'intérêt opéré par IBM et d'autres sociétés d'informatique : les machines sont les mêmes que ce soit pour le projet de Roberto Busa, les calculs balistiques ou encore les feuilles de paie.
La lexicométrie va se nourrir de projets de recherche pour permettre à d'autres champs d'évoluer.
Exemple : certains informaticiens naviguent entre des projets de recherche autour du langage (création d'une langue par exemple) et des sociétés qui font de l'analyse de langage pour créer des outils de reconnaissance comme dans les domaines bancaires ou des assurances.
Ce qui semble être une curiosité dans le domaine des sciences humaines et sociales n'en est pas une dans d'autres domaines des sciences naturelles ou physiques.

On retrouve ce type de duo dans des projets d'édition numérique : un éditeur ou une éditrice, qui se concentre sur les contenus, qui suit de près le travail de l'auteur ; et un ou une ingénieur·e qui met en place les briques techniques nécessaires.
Ces deux profils doivent travailler en bonne intelligence, et interroger ensemble leurs pratiques.

===↓===

### 1.4. Les petites mains
Il y a une invisibilisation des femmes dans le projet de Roberto Busa.

(Voir l'article de Melissa Terras : [https://melissaterras.org/2013/10/15/for-ada-lovelace-day-father-busas-female-punch-card-operatives/](https://melissaterras.org/2013/10/15/for-ada-lovelace-day-father-busas-female-punch-card-operatives/))

===↓===

<!-- .slide: data-background="medias/women-busa-project.jpg" data-background-size="contain" -->

===note===
Il me semble important de noter que le projet de Roberto Busa n'a pu être possible qu'avec l'aide de _petites mains_.

C'est encore un phénomène que nous pouvons observer dans le domaine de la recherche : des chercheurs considèrent que le travail de mise en forme et de balisage d'un contenu (comme un article ou un livre) doit délégué à une secrétaire.
Ce travail de balisage et de mise en forme est considéré comme trivial ou neutre, alors que c'est à ce moment qu'une bonne partie de la réflexion se met en place : comment structurer un propos, quelle dose de données ajouter à un texte, quels liens faire entre 
En tant que chercheur (ou plutôt apprenti chercheur) il m'importe de maîtriser cette partie, et de ne pas la déléguer.
Et il m'importe que les chercheur·e·s avec qui je travaille maîtrise également cette dimension essentielle de l'écriture scientifique.

Pour conclure sur ce point et pour citer Marcello Vitali-Rosati : " Il faut penser la matérialité de l’écriture. Ou mieux: la pensée n’est que cette matérialité."

===↓===

### 1.5. Pour une définition des humanités numériques

> Le développement des humanités numériques comme application du développement de l’informatique dans un champ spécifique permet à la fois d’en éclairer les ressorts profonds et d’interroger la place que peuvent occuper les humanités dans des sociétés sous l'influence des technologies numériques.  
> Pierre Mounier, _Les humanités numériques : Une histoire critique_

===note===
Les points essentiels de cette définition :

- il s'agit bien d'_application_ : ce terme porte en lui une dimension d'adaptation. L'outil doit toujours être ajusté, _réglé_
- 

===↓===

### 1.6. Pour compléter
À partir du chapitre "Histoire des humanités numériques" de Michael Sinatra dans _Pratiques de l’édition numérique_ ([http://www.parcoursnumeriques-pum.ca/histoire-des-humanites-numeriques](http://www.parcoursnumeriques-pum.ca/histoire-des-humanites-numeriques))

- Internet et le Web : accélérateurs<!-- .element: class="fragment" -->
- convergence entre les humanités et le numérique : éviter toute opposition du type humanité-technologie<!-- .element: class="fragment" -->
- réflexivité<!-- .element: class="fragment" -->

===note===
D'autres éléments ont contribué à la constitution des humanités numériques, que Pierre Mounier évoque ou que d'autres chercheurs mentionnent.
Internet et le Web ont permis une accélération, et un véritable changement de paradigme.
C'est clairement Internet et le Web qui ont amené cette dimension de _pratique_ propre aux termes _digital_ ou _numérique_, contrairement à l'informatique qui se concentre plus sur les moyens techniques mis en œuvre.

À la question "Le numérique est-il le sujet ou l’objet des humanités numériques ?", Marcello Vitali-Rosati et Michael Sinatra proposent de considérer une "convergence" entre les humanités et le numérique, et d'éviter toute opposition du type humanité-technologie.

J'insiste un peu lourdement sur ce point.
Ce texte révèle un point fondamental dans le champ des humanités numériques : la relation entre d'une part des méthodes et des outils, et d'autre part l'analyse de l'influence de ces méthodes et outils sur les contenus même de la recherche scientifique.
L'informatique, Internet et le numérique façonne désormais notre environnement, et nous devons avoir une démarche de réflexivité pour ne pas considérer cela comme de simples nouveaux outils dans la panoplie du chercheur.
Les enjeux de constitution et de diffusion du savoir sont au centre de ces réflexions, ainsi l'édition est une opportunité de lier pratique et théorie, d'exercer cette réflexivité.

===↓===

## 2. Quelques dates importantes

- 1949 : Roberto Busa et son _Index Thomisticus_

<!-- .element: class="fragment" -->

- 1960-1970 : émergence de travaux similaires

<!-- .element: class="fragment" -->

- 1978 : création de l'Association for Computers and the Humanities

<!-- .element: class="fragment" -->

- 1987 : création de la liste de diffusion "Humanist" ([lien](https://dhhumanist.org))

<!-- .element: class="fragment" -->

- 1987 : lancement du projet TEI

<!-- .element: class="fragment" -->

- 2004 : publication de _A Companion to Digital Humanities_ par Susan Schreibman, Ray Siemens et John Unsworth

<!-- .element: class="fragment" -->

- 2008 : A Digital Humanities Manifesto (UCLA) ([lien](http://manifesto.humanities.ucla.edu/2009/05/29/the-digital-humanities-manifesto-20/))

<!-- .element: class="fragment" -->

- 2010 : manifeste des digital humanities (THATCamp Paris) ([lien](https://tcp.hypotheses.org/318))

<!-- .element: class="fragment" -->

===note===
**1949**. Nous l'avons déjà vu, la première expérimentation de Roberto Busa est fondatrice.

**1960-1970**. D'autres travaux vont suivre pendant les années 1960 et 1970, principalement autour de la linguistique.
C'est à la même période (en 1965 pour être précis) que va être donnée une conférence importante : "Computers for the Humanities?".

**1978**. À la suite de toutes ces initiatives les chercheurs ressentent le besoin de formaliser la communauté qui est en train de se constituer, c'est la création de la première association autour des humanités numériques.
Il faut noter que l'on parle alors d'informatique, le terme _digital_ apparaîtra plus tard.
Ce détail est important pour comprendre quelle est la culture de l'époque, 

**1987**. C'est une année importante puisque c'est tout d'abord la naissance de la TEI...

**2004**. C'est l'année de publication d'une anthologie importante dans le champ des humanités numériques, l'occasion aussi de populariser le _nouveau_ terme _digital_ qui prend la place de _computing_.
les "Humanities Computing" deviennent "Digital Humanities".

**2008**. Un premier _manifeste_ est publié en 2008, un texte important.
Il y est notamment dit que les _digital humanities_ ne constituent pas un domaine unifié mais un ensemble de pratiques convergentes.
Il est également question de diffusion des connaissances.

Ce que l'on peut voir en creux à travers ces quelques dates, c'est que cette approche interdisciplinaire qui réunit une communauté active, est particulièrement réflexive : elle interroge ses propres pratiques et ses fondements théoriques en même temps qu'elle se constitue.

===↓===

## 3. Édition, littérature et humanités numériques
**humanités numériques ≠ SHS + logiciels**

===note===
Les humanités numériques ne se limitent pas à l'usage ou à la maîtrise de logiciels dans les domaines des sciences humaines et sociales.
Le cas récent de l'erreur du gouvernement britannique illustre bien le rapport que nous entretenons parfois avec les technologies : le gouvernement britannique a utilisé 

===↓===

### 3.1. Quelques domaines en exemples

- histoire : Histoire illustrée et interactive du Mile End ([http://memoire.mile-end.qc.ca](http://memoire.mile-end.qc.ca))

<!-- .element: class="fragment" -->

- histoire de l'art : Rethinking Guernica ([https://guernica.museoreinasofia.es](https://guernica.museoreinasofia.es))

<!-- .element: class="fragment" -->

- histoire : Mapping Gothic France ([http://mappinggothic.org](http://mappinggothic.org))

<!-- .element: class="fragment" -->

- philosophique : MARXdown ([https://marxdown.github.io/](https://marxdown.github.io/))

<!-- .element: class="fragment" -->

===note===
Il s'agit d'exemple de projets en humanités numériques, en dehors des champs de la littérature ou de l'édition, pour vous donner une idée de ce dont je vous parle depuis maintenant une bonne demi-heure.

Le premier projet est historique et cartographique.
Histoire illustrée et interactive du Mile End répertorie un certain nombre d'édifices historiques, et les situe sur une carte du quartier du Mile End à Montréal.
Le projet est déjà un peu daté, et il repose essentiellement sur la description précise de ces édifices et le placement sur une carte.

Rethinking Guernica est une plongée dans l'œuvre de Pablo Picasso ainsi qu'une recontextualisation historique.

Le troisième projet, Mapping Gothic France, est encore historique et cartographique, il s'agit d'utiliser des interfaces web pour permettre une représentation différente de phénomènes historiques.
Je cite les auteurs du projet : "Alors que les images peuvent être représentées de manière satisfaisante en deux dimensions sur un écran d'ordinateur, l'espace - en particulier l'espace gothique - exige une approche différente, qui englobe non seulement le volume architectonique mais aussi le temps et la narration."

Le projet MARXdown consiste en le rassemblement de textes de Karl Marx, en version web, et annoté par des chercheur·e·s.
Certes il s'agit d'un projet d'édition numérique plus que d'un projet en philosophie.
Il s'agit de l'association d'un balisage Markdown permettant de structurer facilement le texte, de le versionner puis de le mettre à disposition en version web.
Une _couche_ d'annotation permet de commenter des passages et d'établir une conversation.

Il y aurait potentiellement beaucoup d'autres projets à explorer.

===↓===

### 3.2. Littérature et humanités numériques
Quelques projets _humanités numériques_ en littérature :

- Les dossiers de Bouvard et Pécuchet ([http://www.dossiers-flaubert.fr/](http://www.dossiers-flaubert.fr/))

<!-- .element: class="fragment" -->

- Tout Voltaire ([https://artfl-project.uchicago.edu/tout-voltaire](https://artfl-project.uchicago.edu/tout-voltaire))

<!-- .element: class="fragment" -->

- Les manuscrits de Stendhal ([http://stendhal.demarre-shs.fr/](http://stendhal.demarre-shs.fr/))

<!-- .element: class="fragment" -->

===note===
Les dossiers de Bouvard et Pécuchet est un regroupement de feuillets pour un projet encyclopédique non achevé.

Tout Voltaire est un projet de recensement des textes

Les manuscrits de Stendhal est un projet de numérisation et de conversion des manuscrits de Stendhal.
Pour citer les personnes ayant travaillé sur le projet : "Chaque médium a sa spécificité : convivialité, hypertextualité, liberté de parcours et possibilité de recherches originales pour l’édition électronique; construction d’un nouvel « objet livre » au sens traditionnel, dont le contenu aura été renouvelé par le passage par la base documentaire et la redéfinition des corpus."

===↓===

### 3.3. Édition et humanités numériques
_Les sculptures de la villa romaine de Chiragan_ (Pascal Capus, Musée Saint-Raymond) :  
[https://villachiragan.saintraymond.toulouse.fr/](https://villachiragan.saintraymond.toulouse.fr/)

Pour en savoir plus : [https://www.youtube.com/watch?v=-bTGBPaJR3o](https://www.youtube.com/watch?v=-bTGBPaJR3o)

===note===
Je vais vous parler rapidement d'un projet que je connais bien puisque je l'ai en partie réalisé.
Ce projet d'édition numérique a été mené par Christelle Molinié (bibliothécaire) et Pascal Capus (conservateur de musée) du Musée Saint-Raymond de la Ville de Toulouse, et réalisé par Julie Blanc (doctorante et designeuse) et moi-même.

===↓===

## 4. Introduction à Stylo

===↓===

### 4.1. Stylo ?

- éditeur de texte sémantique pour la rédaction scientifique<!-- .element: class="fragment" -->
- écrire, structurer, prévisualiser, réviser, exporter<!-- .element: class="fragment" -->
- utilisation d'un langage de balisage léger (Markdown), d'un langage de description de métadonnées (YAML) et d'un langage de description bibliographique (BibTeX)<!-- .element: class="fragment" -->
- utilisation de briques logicielles existantes (et libres)<!-- .element: class="fragment" -->

===↓===

### 4.2. Démonstration

**[https://stylo.huma-num.fr](https://stylo.huma-num.fr)**

===↓===

### 4.3. Exercices pour la prochaine fois

- création d'un compte : [https://stylo.huma-num.fr](https://stylo.huma-num.fr)

<!-- .element: class="fragment" -->

- création d'un article comportant 3 niveaux de titres, des citations, des notes de bas de page, des références bibliographiques

<!-- .element: class="fragment" -->

- partage de votre article avec moi : antoine@quaternum.net

<!-- .element: class="fragment" -->

(Pour vous aider voici la documentation : [https://stylo-doc.ecrituresnumeriques.ca/](https://stylo-doc.ecrituresnumeriques.ca/))

<!-- .element: class="fragment" -->
