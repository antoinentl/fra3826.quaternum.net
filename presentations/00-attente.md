# Bienvenue dans le cours FRA3826 Théories de l'édition numérique

Merci par avance de couper votre micro (pour éviter les effets d'écho).

**La séance va commencer à 9h35.**

Vous pouvez utiliser le service de messagerie en bas à gauche si vous avez des questions.
