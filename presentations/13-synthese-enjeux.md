## FRA3826 Théories de l'édition numérique

# 13 - Séance finale
## Synthèse
## Enjeux contemporains et à venir de l'édition numérique

===note===

===↓===

## Sommaire de la séance 13 - Synthèse et enjeux

1. Synthèse : culture numérique<!-- .element: class="fragment" -->
2. Synthèse : qu'est-ce que l'édition numérique ?<!-- .element: class="fragment" -->
3. Une vue globale sur les enjeux actuels de l'édition numérique<!-- .element: class="fragment" -->
4. Édition numérique et pérennité<!-- .element: class="fragment" -->
5. Bilan du cours (discussion)<!-- .element: class="fragment" -->

===↓===

## 1. Synthèse : culture numérique

- une évolution sur le temps long, itérative ;<!-- .element: class="fragment" -->
- formats + protocoles ;<!-- .element: class="fragment" -->
- l'héritage analogique ;<!-- .element: class="fragment" -->
- les humanités numériques : un mouvement nécessaire (temporaire) ?<!-- .element: class="fragment" -->

===note===
Et à travers les formats et les protocoles, les questions politiques.
(Tout cela n'est pas neutre.)

L'héritage analogique concerne les pratiques d'écriture et de lecture liées au livre papier.

Les humanités numériques sont également un pas vers l'édition numérique.

===↓===

## 2. Synthèse : qu'est-ce que l'édition numérique&nbsp;?

- le livre numérique : objet numérique non identifié ?

<!-- .element: class="fragment" -->

- les rencontres (nombreuses) du livre et du numérique (avant le livre numérique) ;

<!-- .element: class="fragment" -->

- édition numérique : changement de paradigme ;

<!-- .element: class="fragment" -->

- édition numérique : multimodale et hybride ?

<!-- .element: class="fragment" -->

- édition ~~numérique~~

<!-- .element: class="fragment" -->

===note===
La question des compromis.

Concernant l'article (qui est bien plus avant-gardiste que ce que je viens de dire) :

- remarque de David :
- remarque de Céline : oui l'idéal étant de pouvoir travailler sur différentes _couches_ du texte, sans perdre de modifications s'il y a des interventions au même moment que des relectures. Un protocole d'édition comme Git permet cela, mais son usage est très (trop) complexe.

===↓===

## 3. Une vue globale sur les enjeux actuels de l'édition numérique

===note===
Difficile d'être exhaustif, tant les enjeux et les _défis_ sont importants, je vous propose d'en aborder quelques uns, que vous devrez bien garder en tête dans vos pratiques futures d'édition numérique.

===↓===

### 3.1. L'interopérabilité

- faire dialoguer les machines ;<!-- .element: class="fragment" -->
- ouvrir les logiciels, les formats et les protocoles ;<!-- .element: class="fragment" -->
- licence libre et documentation.<!-- .element: class="fragment" -->

===note===
C'est un peu la thématique sur laquelle je reviens continuellement, parce qu'elle conditionne nos usages de logiciels et de programmes.

L'enjeu ici est de pouvoir faire dialoguer des logiciels ou des programmes afin de faciliter le travail d'édition numérique, c'est la dimension _pratique_ de l'interopérabilité.

Mais il s'agit aussi de pouvoir _anticiper_ des usages que nous n'avons pas encore, et ainsi de ne pas se contenter de pouvoir faire dialoguer deux logiciels aujourd'hui, mais d'avoir toutes les possibilités possible de passer d'une solution technique à une autre, sans dépendre d'une entreprise à laquelle nous aurions confié nos données et leurs usages (pour information c'est exactement ce qui est en train de se passer à l'Université de Montréal).

Cela doit se traduire par 2 démarches parallèles :

- des licences ouvertes qui permettent la réutilisation et l'adaptation de programmes ou de logiciels ;
- la documentation de ces outils pour comprendre leur fonctionnement et leur évolution.

===↓===

### 3.2. Critical Code Studies

- le code n'est pas neutre ;<!-- .element: class="fragment" -->
- une intention humaine derrière chaque ligne de code.<!-- .element: class="fragment" -->

===note===
Il s'agit, à travers ce courant ou cette démarche scientifique, d'interroger comment le _code_ fonctionne, quelles sont les implications sociales, 

Il m'est difficile de présenter rapidement des questions extrêmement complexes, mais nous avons déjà pu aborder la question de la place des femmes dans les humanités numériques (les "petites mains" essentielles au travail de Roberto Busa), et il faudrait aussi aborder les questions intersectionnelles.
Le fait que la plupart des programmes informatiques ont été écrits par des hommes blancs occidentaux pose tout de même un problème.
Mais étant moi-même un homme blanc occidental, vous comprenez pourquoi je ne me sens pas légitime pour parler plus longuement de cette question.

Ce que je peux toutefois dire, c'est que le code n'est pas neutre, qu'il faut absolument se défaire de cette idée d'une technologie comme absente de toute intention.
N'oubliez pas que la moindre ligne de code est écrite par un humain, directement ou indirectement.
Les algorithmes ne sont par exemple pas exempt de déterminisme social et de biais culturels.
Et si quelqu'un vous dit que c'est une machine qui a écrit ce programme, demandez vous qui a codé cette machine.

Exemples de Stylo et de l'application du Canada.

===↓===

## 4. Édition numérique et pérennité

===note===
Nous allons maintenant nous attarder sur un enjeu à venir de l'édition numérique, et qui semble avoir retenu le plus votre attention : la question de la pérennité.
Cette question est en fait un ensemble de questions, que nous pouvons aborder sous 3 angles différents.

===↓===

### 4.1. La double question de l'accès aux données (sources et artéfacts)

- accès et lecture/modification des sources ;<!-- .element: class="fragment" -->
- accès et lisibilité des artéfacts produits.<!-- .element: class="fragment" -->

===note===
La première question, double, est celle de l'accès aux données.
Comment permettre une certaine pérennité des objets numériques produits dans le cadre d'une démarche d'édition ?
Et comment pouvoir modifier les sources de ces objets dans 5, 10 ou 20 ans ?
C'est exactement la question que s'est posée l'équipe de Getty Publications au moment de créer Quire.

L'exemple typique est celui d'un livre numérique au format application (du type application pour iOS), qui est généré par un logiciel fermé au format lui aussi fermé.
Le jour où ...

===↓===

### 4.2. L'archivage (dépôt légal numérique ?)

- le dépôt légal ;<!-- .element: class="fragment" -->
- changer continuellement de format.<!-- .element: class="fragment" -->

===note===
D'un point de vue collectif il faut trouver des solutions pour conserver les livres numériques.
Rôle des bibliothèques.

===↓===

### 4.3. La maintenabilité des programmes

- _qui_ met à jour les programmes informatiques que nous utilisons quotidiennement ?

<!-- .element: class="fragment" -->

- comment trouver un modèle de financement pour payer les heures de travail sur ces programmes ?

<!-- .element: class="fragment" -->

- un programme qui dure est un programme _vivant_.

<!-- .element: class="fragment" -->

===note===
C'est la grande question absente dans la plupart des discussions autour de la pérennité du livre et du livre numérique.
Souvent l'attention est portée surtout sur les objets (numériques) et moins sur les procédés qui permettent leur lecture ou leur production.
Ouvrir la licence d'un programme permet d'envisager que celui-ci soit réutilisé, modifié voir remplacé.

article à commenter

Anecdote concernant un programme (jekyll-scholar), utilisé dans le cadre d'un projet rémunéré.

===↓===

## 5. Bilan du cours (discussion)

- plus de baladodiffusions plus courtes<!-- .element: class="fragment" -->
- accompagnement sur les annotations<!-- .element: class="fragment" -->
- retour sur les prises de notes<!-- .element: class="fragment" -->
- cours inversés, place aux débats<!-- .element: class="fragment" -->

===note===
Qu'est-ce que les annotations vous ont apporté ?
