---
title: "Évaluation de mi-session — Proposition de correction"
bibfile: "data/bibliographie.json"
date: 2020-11-10
---
<!-- ## Informations importantes
Cet examen est à réaliser entre le lundi 19 octobre 12h et le samedi 24 octobre 12h.
Les copies, au format PDF (format lettre) et comportant vos prénoms et noms ainsi que votre matricule, doivent être déposées sur Studium dans l'espace créé pour l'occasion.

Questions et informations complémentaires : [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca) -->

Proposition de correction de l'évaluation de mi-session du cours FRA 3826 Théories de l'édition numérique.

## 1. Questions courtes
Chaque question courte doit être traitée.
Chaque réponse ne doit pas dépasser 500 signes espace compris (90 mots).
Les références et les exemples peuvent être utilisés pour chaque réponse.

### 1.1. Expliquez brièvement l'articulation entre les 3 fonctions du processus éditorial.
Les 3 fonctions ou étapes de l'édition, considérée comme un **processus éditorial**, sont **la fonction de choix et de production**, **la fonction de légitimation** et **la fonction de diffusion**. Ces 3 fonctions **se suivent dans le temps**, elles sont **liées de façon logique**. Enfin il y a _édition_ uniquement **si les 3 fonctions son présentes**.

### 1.2. Comment pouvez-vous décrire, avec vos propres mots, les concepts de centralisation et d'interopérabilité ? Vous pouvez utiliser des exemples.
Le concept de centralisation correspond à **un modèle de structure de communication** où les différents nœud d'un réseau sont connectés entre à partir d'**un même point**. Si ce point de rencontre, ce passage obligé, est supprimé, le réseau ne peut plus fonctionner. L'interopérabilité est la capacité de deux logiciels **à communiquer ensemble**, par le biais de **formats standards** ou de **protocoles**.

### 1.3. Choisissez une des particularités du format HTML et expliquez pourquoi vous pensez que celle-ci est majeure.
Une des grandes particularités du format HTML est **la distinction entre structure et mise en forme**, qui est d'ailleurs commune avec d'autres formats de balisage. Cette particularité est majeure parce qu'elle permet de **repenser la façon d'écrire et d'éditer du contenu** : le fait de pouvoir _travailler_ sur un texte dans se soucier de sa mise en forme tout en lui donnant **une structure sémantique logique** n'est pas facilement permis avec des outils comme les traitements de texte.

### 1.4. Qu'est-ce que le format du livre numérique apporte en terme de design ?
Le format du livre numérique, l'EPUB, est un format qui est **"redistribuable"** (voir la page "L’ebook redistribuable" de Jiminy Panoz, [https://jaypanoz.github.io/reflow/](https://jaypanoz.github.io/reflow/)), c'est-à-dire qu'il **s'adapte** à la taille de l'écran et que plusieurs de ses **paramètres d'affichage** peuvent être modifiés par le lecteur ou la lectrice.

### 1.5. Donnez un exemple de projet estampillé "humanités numériques" dans lequel le numérique apporte réellement une nouvelle dimension.
**Les manuscrits de Stendhal (http://stendhal.demarre-shs.fr/)** est un projet de numérisation et de conversion des manuscrits de Stendhal.
Les textes manuscrits sont affichable **en texte intégral**, proposant ainsi une recherche plus puissante, **des parcours de lecture** et des affichages simultanés entre la prise de vue et le texte converti (via l'usage de la TEI).
Il ne s'agit donc pas d'une simple mise en ligne d'une numérisation, mais bien d'**un accès à des textes repensé**.

## 2. Questions longues
Une question longue **au choix**, qui doit faire l'objet d'une réponse longue comprise entre 3000 signes espaces compris (500 mots) et 5000 signes (800 mots).
Chaque réponse doit recourir à des références issues du cours ou d'ailleurs.

### 2.1. Le code et la littérature (prise au sens large) ont plusieurs points communs lorsqu'il s'agit de publication ou d'édition. Proposez une argumentation pour défendre ou démonter cette affirmation.
Points importants à aborder :

- dans les deux cas du code et de la littérature il s'agit de texte ;
- le code comme la littérature sont destinés à être mis à disposition, à être _publiés_  {{< cite "epron_fonction_2018" >}} ;
- les questions de droits d'auteur ou de licences concernent ces deux domaines : qui peut accéder aux contenus, qui peut les réutiliser, etc. ;
- dans certaines expérimentations marginales le code informatique est rédigé comme de la littérature, dans d'autres les sources d'un texte littéraire sont mises à disposition comme du code ;
- code, texte, balisage et prose se mêlent, tout comme les humains et les programmes {{< cite "gelgon_dialogue_2018" >}}.

### 2.2. À partir de vos différentes lectures, comment pouvons-nous affirmer que l'"édition numérique" n'est pas la simple addition de l'édition et du numérique ?
Points importants à aborder :

- le numérique permet de réaliser des projets impossibles sans le numérique ;
- des projets d'édition numérique dans le domaine des humanités numériques tendent à prouver que le numérique apporte une réflexivité jusqu'ici faible ;
- les outils issus de l'édition numérique nous poussent à nous interroger sur nos pratiques, ils ne sont pas neutres {{< cite "kirschenbaum_track_2016" >}} ;
- certaines pratiques d'édition numérique ont reconfiguré la façon d'éditer {{< cite "ludovico_post-digital_2016" >}} ;
- il faut distinguer l'édition numérique de l'informatisation des métiers de l'édition (qui l'a précédée) ;
- l'édition numérique produit des nouvelles formes de livres, inédites : tabulaires {{< cite "vandendorpe_du_1999" >}} et multimédias.

### 2.3. Nous avons abordé en cours la question de la connexion/déconnexion comme un enjeu social. Après avoir rapidement exposé les tenants de ce sujet, exposez un point de vue en faveur de l'édition comme outil critique.
Points importants à aborder :

- l'accès à Internet et au Web étant étendu dans tous nos espaces et nos activités, notamment par le biais de terminaux mobiles, il devient difficile de se déconnecter, les sollicitations ou obligations professionnelles, sociales ou de loisir se multiplient {{< cite "morozov_prix_2017" >}} ;
- l'édition permet d'envisager des espaces asynchrones d'accès à l'information, que ce soit en imprimé ou en numérique ;
- l'édition numérique est aussi une occasion de réfléchir à nos pratiques de publication (y compris numériques) ;
- éditer est un acte critique {{< cite "dacos_ledition_2010" >}}.

### Références

{{< bibliography cited >}}
