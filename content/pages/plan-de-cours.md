---
title: Plan de cours
toc: yes
date: 2020-11-23
bibfile: "data/bibliographie.json"
---
Contact : Antoine Fauchié, [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)

## Note importante
En raison de la réorganisation liée à la pandémie, ce cours aura lieu à distance (synchrone et asynchrone).
Ce plan de cours est susceptible d'évoluer au fil des semaines, les étudiant·e·s seront prévenu·e·s au fur et à mesure.

## Objectifs du cours
Acquérir une culture numérique dans le domaine de l'édition tout en faisant le lien avec des études littéraires.
Identifier les éléments de rupture ou de continuité qui peuvent caractériser le passage au numérique, en comparant différentes formes éditoriales : traditionnelles issues de la culture de l’imprimé, et récentes émergeant de la culture du numérique. Porter un regard critique sur les enjeux sociaux, culturels, politique et juridiques dans les changements de pratiques de lecture et d’écriture, pour comprendre les nouvelles structurations, publications, diffusions et légitimations des contenus sur Internet.

À la fin du cours, les étudiant·e·s seront en mesure :

- de comprendre et d’expliquer les notions fondamentales à l’édition numérique ;
- d’identifier les outils et objets relevant de l’édition numérique ;
- de développer une pensée critique sur les différentes théories du numérique ;
- de prendre en main plusieurs outils et de développer leurs propres usages de ces outils.

## Organisation des séances
Voici les 4 types de séances pour ce cours à distance :

- **cours synchrones** : 7 séances de 2h (9h30-11h30). Séances en visio-conférence composées d'un cours magistral et d'un temps d'échange, séances obligatoires. Pour chaque cours synchrone un texte doit être lu. Un support de cours sera disponible pour chaque cours synchrone ;
- **permanences** : tous les vendredis de 9h30 à 11h30, hors cours synchrones, séances non obligatoires. Celles et ceux qui le souhaitent peuvent venir me poser des questions, échanger sur les cours, discuter des textes, partager des projets ;
- **cours asynchrones à écouter** : 6 baladodiffusions ou _podcasts_ de 30 minutes environ. Les permanences et les séances synchrones seront l'occasion de revenir sur ces enregistrements ;
- **cours asynchrones à lire** : 2 supports de cours à lire (en plus des supports des cours synchrones), accompagnés de ressources (vidéos, textes, etc.). Les permanences et les séances synchrones seront l'occasion de revenir sur ces supports.


## Évaluations
Trois évaluations sont prévues :

- 20% : participation pendant les séances synchrones, participation aux annotations lorsque cela est demandé, participation au site de ressources mis en place par les étudiant·e·s (projet de site de ressources optionnel) ;
- 40% : questionnaire en rapport avec les séances (travail individuel), à réaliser sur une période de 5 jours à mi-parcours de la session. 5 questions en rapport avec le cours, demandant des réponses de quelques lignes ; une question au choix parmi 3 questions, demandant une réponse plus longue d'une page environ. Les dates sont précisées dans le calendrier à la fin de ce document ;
- 40% : évaluation finale, travail individuel au choix parmi ces deux propositions : analyse d'une publication numérique existante (livre numérique, site web ayant une forte dimension éditoriale, objet numérique non identifié) ; descriptif d'un projet d'édition numérique (il s'agit bien du descriptif, et nullement de la _réalisation_ du projet envisagé). Le choix du sujet, pour chaque étudiant, sera validé courant novembre. Des précisions sur le format et le calendrier seront données au même moment.

### Contraintes pour l'évaluation finale

- utiliser le cours et la bibliographie pour argumenter vos propos ;
- structurer le document en parties et sous-parties, pour faciliter sa lecture ;
- proposer un texte entre 9000 et 15000 signes espaces compris ;
- utilisation de Stylo (un éditeur de texte sémantique que nous allons découvrir https://stylo.huma-num.fr) ou d'un outil similaire pour la production du document.

### Échéances pour l'évaluation finale

- **6 novembre** : date limite à laquelle les étudiant·e·s devront avoir envoyé un court descriptif de leur sujet (en quelques lignes) ;
- **20 novembre** : date limite à laquelle l'enseignant devra valider le sujet ;
- **16 décembre** : date limite de dépôt du travail individuel par les étudiant·e·s.

Il est possible d'envoyer la proposition de sujet _avant_ le 6 novembre, le sujet pourra également être validé _avant_ le 20 novembre.

### Exemple de structure pour les deux sujets de l'évaluation finale
**Analyse d'une publication numérique :**

- description courte : quelques lignes qui présentent brièvement le livre numérique, le site web ou l'objet numérique que vous avez choisi ;
- contexte de la publication : présentez les objectifs de la publication numérique, les intentions de l'auteur·e et de l'éditeur·trice. Vous devez effectuer des recherches, vous pouvez également analyser la démarche de diffusion de la publication ;
- résumé du contenu : qu'il s'agisse d'une œuvre de fiction, d'un essai ou d'une expérimentation littéraire, vous devez présenter le _contenu_ de la publication analysée de façon synthétique ;
- descriptions technique et ergonomique : présentez les choix techniques qui ont permis la production de cette publication, ou le format de consultation. La description ergonomique concerne la navigation dans la publication, un point sur la réalisation graphique peut également être ajouté ;
- stratégie éditoriale et commerciale : quels moyens ont été mis en place pour concevoir cette publication puis pour la diffusion ? Quelle est la réalité économique ou financière de cette publication ?
- comparaison avec d'autres publications numériques : quels points communs ou quels éléments de différenciation ;
- analyse critique (avec l'appui de références issues du cours ou de la bibliographie) : à partir des points précédents, rédiger un point de vue argumenté sur la publication ;
- conclusion : résumé synthétique avec une éventuelle ouverture.


**Descriptif d'un projet d'édition numérique :**

- présentation courte : quelques lignes qui présentent brièvement votre projet ;
- objectifs de la publication : quel est le type de contenu, quel est le ou les publics visés, quelles fonctionnalités va intégrer la publication ?
- appuis théoriques : justification des choix à partir de références (issues du cours, de la bibliographie du cours ou d'autres sources) ;
- comparaison avec d'autres publications numériques : votre projet peut s'inspirer d'une publication existante, auquel cas vous pouvez expliquer en quoi votre projet se démarque. Il est important que votre projet se positionne, soit en inscrivant votre projet dans un courant, soit en argumentant les éventuelles innovations qu'il apportera (vous n'êtes pas obligé d'innover) ;
- choix ergonomiques : comment naviguer dans la publication ? Il est également possible d'aborder les questions de design et de design graphique ;
- choix techniques : tentez de donner quelques pistes sur les choix techniques nécessaires pour la réalisation de votre projet ;
- calendrier de réalisation : au minimum 3 étapes à prévoir pour la réalisation du projet ;
- conclusion : résumé synthétique en cohérence avec la présentation courte et les objectifs de la publication.

## Liste des cours

1. Cours synchrone : Introduction
2. Cours synchrone : Pour une définition de l'édition, Introduction à une histoire des pratiques et supports éditoriaux
3. Cours asynchrone à écouter : Qu'est-ce que le numérique 1/3 ?
4. Cours asynchrone à écouter : Qu'est-ce que le numérique 2/3 ?
5. Cours asynchrone à écouter : Qu'est-ce que le numérique 3/3 ?
6. Cours synchrone : Introduction aux humanités numériques
7. Cours synchrone : Édition, politique et société
8. Cours synchrone : Livre numérique et lecture numérique, qu'est-ce qui change ?
9. Cours asynchrone à écouter : Qu'est-ce que l'édition numérique 1/2 ?
10. Cours asynchrone à écouter : Qu'est-ce que l'édition numérique 2/2 ?
11. Cours synchrone : L'édition numérique littéraire et l'édition numérique savante
12. Cours asynchrone à lire : Qu'est-ce que l'éditorialisation ?
13. Cours asynchrone à lire : Chaînes éditoriales et systèmes de publication
14. Cours synchrone : Enjeux contemporains et à venir de l'édition numérique

---

## Calendrier

| Date       | Cours  | Intitulé                         | Texte à lire |
|------------|--------|----------------------------------|--------------|
| 04/09/2020 | 1      | Cours synchrone : Introduction   |  |
| 11/09/2020 | 2      | Cours synchrone : Pour une définition de l'édition | (Epron et Vitali-Rosati, 2018) |
| 18/09/2020 | 3      | Permanence + Cours asynchrone à écouter&nbsp;: Qu'est-ce que le numérique ? 1/3 | (Berners-Lee, 1989) |
| 25/09/2020 | 4      | Permanence + Cours asynchrone à écouter&nbsp;: Qu'est-ce que le numérique ? 2/3 | (Boulétreau et Habert, 2017) |
| 02/10/2020 | 5      | Permanence + Cours asynchrone à écouter : Qu'est-ce que le numérique ? 3/3 | (Vandendorpe, 1999) |
| 09/10/2020 | 6      | Cours synchrone : Introduction aux humanités numériques | (Mounier, 2018) |
| 16/10/2020 | 7      | Cours synchrone : Édition, politique et société | (Morozov, 2017) |
| 19/10/2020 |        | **Mise en ligne du sujet de l'évaluation de mi-session** |  |
| 24/10/2020 |        | **Date limite de Dépôt par les étudiant·e·s de l'évaluation de mi-session** |  |
| 30/10/2020 | 8      | Cours synchrone : Livre numérique et lecture numérique, qu'est-ce qui change ? | (Mod, 2018) |
| 06/11/2020 | 9      | Permanence + Cours asynchrone à écouter : Qu'est-ce que l'édition numérique 1/2 ? | (Epron et Vitali-Rosati, 2018) |
| 13/11/2020 | 10     | Permanence + Cours asynchrone à écouter : Qu'est-ce que l'édition numérique 2/2 ? | (Bourassa et al., 2018) |
| 20/11/2020 | 11     | Cours synchrone : L'édition numérique littéraire et l'édition numérique savante | (Saemmer, 2007) |
| 27/11/2020 | 12 + 13 | Permanence + Cours asynchrones à lire : Qu'est-ce que l'éditorialisation ? Chaînes éditoriales et systèmes de publication | (Vitali Rosati, 2016) + (Fauchié et Parisot, 2018) |
| 04/12/2020 | 14     | Cours synchrone : Enjeux contemporains et à venir de l'édition numérique | _à déterminer_ |
| 16/12/2020 |        | **Date limite de Dépôt par les étudiant·e·s du projet d'édition numérique** |  |

---

## Séances détaillées

### 01. Cours synchrone : Introduction (04/09/2020)
Cours destiné à la présentation de l'enseignant, des étudiant·e·s, et du cours et de ses modalités.

En détail :

- pourquoi utiliser Jitsi ? Quelques mots sur les logiciels propriétaires et la centralisation des moyens de communication ;
- présentation de l'enseignant et des étudiant·e·s autour d'une contrainte ;
- présentation du cours :
  - objectif général : acquérir une culture numérique dans le domaine de l'édition tout en faisant le lien avec des études littéraires ;
  - description des 4 types de séances : cours synchrones, permanences, cours asynchrones à écouter, cours asynchrones à lire ;
  - plan de cours détaillé ;
- présentation du fonctionnement :
  - textes à lire pour chaque séance, puis progressivement à annoter pour chaque séance synchrone ;
  - prise de notes des étudiant·e·s avec CodiMD : introduction à Markdown ;
  - participation des étudiant·e·s à un site web de ressources à compléter ensemble, qui vient compléter le cours ;
- évaluations :
  - 20% : participation aux cours synchrones, annotations et ressources du cours
  - 40% : questionnaire (travail individuel)
  - 40% : projet d'édition numérique (travail individuel)

### 02. Cours synchrone : Pour une définition de l'édition, Introduction à une histoire des pratiques et supports éditoriaux (11/09/2020)
L'objectif de cette séance est de donner une définition de l'édition pour _casser_ les préjugés et ouvrir sur de nombreuses interrogations.

1. Qu'est-ce que l'édition ? échanges avec les étudiant·e·s
2. Petite histoire de l'édition
3. Des éditions : littéraires, savantes, etc.
4. Pratiques éditoriales et métiers de l'édition
5. Supports éditoriaux et circulation du savoir

Texte à lire : Epron, B. et Vitali-Rosati, M. (2018). La fonction éditoriale. Dans _L’édition à l’ère numérique_ (p. 6‑10). La Découverte.<br>
[https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/20642/Epron_Vitali-Rosati_EditionEreNumerique.pdf#page=6](https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/20642/Epron_Vitali-Rosati_EditionEreNumerique.pdf#page=6)


### 03. Cours asynchrone à écouter : Qu'est-ce que le numérique 1/3 ? (18/09/2020)
L'objectif de cette baladodiffusion est de donner une vue d'ensemble sur l'émergence du _numérique_ et ses trois événements majeurs (l'informatique, Internet et le Web).

1. L'informatique en quelques dates
2. Internet et le Web

Texte à lire : Berners-Lee, T. (1989). _Information Management: A Proposal_ [archive]. W3C. [https://www.w3.org/History/1989/proposal.html](https://www.w3.org/History/1989/proposal.html)


### 04. Cours asynchrone à écouter : Qu'est-ce que le numérique 2/3 ? (25/09/2020)
L'objectif de cette baladodiffusion est d'appréhender le numérique comme un espace structuré par des protocoles et des formats.

1. Protocoles
2. Formats

Texte à lire : Boulétreau, V. et Habert, B. (2017). Les formats. Dans _Pratiques de l’édition numérique_. Presses de l’Université de Montréal. [http://www.parcoursnumeriques-pum.ca/les-formats](http://www.parcoursnumeriques-pum.ca/les-formats)


### 05. Cours asynchrone à écouter : Qu'est-ce que le numérique 3/3 ? (02/10/2020)
L'objectif de cette baladodiffusion est de présenter les formats d'écriture et de lecture numérique que nous utilisons désormais toutes et tous au quotidien, et plus spécifiquement dans nos activités littéraires.

1. Formats d'écriture numérique
2. Formats de lecture numérique

Texte à lire : Vandendorpe, C. (1999). Linéarité et tabularité. Dans _Du papyrus à l’hypertexte: essai sur les mutations du texte et de la lecture_ (p. 39‑48). La Découverte. [https://vandendorpe.org/papyrus/PapyrusenLigne.pdf#page=39](https://vandendorpe.org/papyrus/PapyrusenLigne.pdf#page=39)


### 06. Cours synchrone : Introduction aux humanités numériques (09/10/2020)
L'objectif de cette séance est de découvrir les humanités numériques, cette approche interdisciplinaire dans laquelle l'édition numérique a une place fondatrice et structurante.

1. Aux origines : l'automatisation des procédés de recherche
2. Quelques dates importantes
3. Édition, littérature et humanités numériques
4. Introduction à Stylo

Texte à lire : Mounier, P. (2018). IBM ou International Busa Machines ? De l’informatique aux humanités. Dans _Les humanités numériques : Une histoire critique_ (p. 21‑43). Éditions de la Maison des sciences de l’homme. [http://books.openedition.org/editionsmsh/12033](http://books.openedition.org/editionsmsh/12033)


### 07. Cours synchrone : Numérique, politique, société, littérature (16/10/2020)
L'objectif de ce cours en ligne est de prendre le recul nécessaire pour observer les implications légales, politiques et sociales du numérique (entre hier et aujourd'hui).

1. Numérique et ouverture
2. Numérique et exercice de la politique
3. Numérique et enjeux sociaux
4. Numérique et littérature

Texte à lire : Morozov, E. (2017, 23 février). Le prix de la déconnexion [Blog]. _Silicon circus_. [https://blog.mondediplo.net/2017-02-23-Le-prix-de-la-deconnexion](https://blog.mondediplo.net/2017-02-23-Le-prix-de-la-deconnexion)


### 08. Cours synchrone : Livre numérique et lecture numérique, qu'est-ce qui change ? (30/10/2020)
L'objectif de cette séance est de (tenter de) définir ce qu'est un livre numérique à partir de plusieurs angles : légal, économique, d'usage. La question de la lecture numérique sera également abordée.

1. Définissons le livre avant de définir le livre numérique
2. Définition légale et définition économique
3. Le livre (numérique) et ses usages : la lecture numérique
4. La littérature numérique ?

Texte : Mod, C. (2018, 20 décembre). The « Future Book » Is Here, but It’s Not What We Expected. _Wired_. [https://www.wired.com/story/future-book-is-here-but-not-what-we-expected/](https://www.wired.com/story/future-book-is-here-but-not-what-we-expected/)


### 09. Cours asynchrone à écouter : Qu'est-ce que l'édition numérique 1/2 ? (06/11/2020)
L'objectif de cette baladodiffusion est de présenter en détail ce que recoupe l'édition numérique, en retraçant tout d'abord son histoire.

1. Les rencontres du livre et du numérique
2. Les formes de l'édition numérique

Texte à lire : Epron, B. et Vitali-Rosati, M. (2018). Enjeux stratégiques de la structuration des documents. Dans _L’édition à l’ère numérique_ (p. 34‑40). La Découverte. [https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/20642/Epron_Vitali-Rosati_EditionEreNumerique.pdf#page=34](https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/20642/Epron_Vitali-Rosati_EditionEreNumerique.pdf#page=34)


### 10. Cours asynchrone à écouter : Qu'est-ce que l'édition numérique 2/2 ? (13/11/2020)
L'objectif de cette baladodiffusion est d'aborder l'édition numérique comme pratique.

1. Cheminement du texte
2. Les formes matérielles de l'édition numérique (transitionnelles et artéfactielles)

Texte à lire : Bourassa, R., Haute, L. et Rouffineau, G. (2018). Devenirs numériques de l’édition. _Sciences du Design_, n° 8(2), 27‑33. [https://www.cairn.info/revue-sciences-du-design-2018-2-page-27.htm](https://www.cairn.info/revue-sciences-du-design-2018-2-page-27.htm)


### 11. Cours synchrone : L'édition numérique littéraire et l'édition numérique savante (20/11/2020)
L'objectif de cette séance est de distinguer une édition numérique littéraire (encore en cours d'émergence ?) et une édition numérique savante (en exercice depuis plusieurs décennies).

1. Qu'est-ce qu'éditer un texte littéraire en numérique ?
2. L'édition numérique savante : production (enjeux des formats de structuration)
3. L'édition numérique savante : diffusion (enjeux des données et des interfaces)

Texte à lire : Saemmer, A. (2007). Littératures numériques : tendances, perspectives, outils d’analyse. _Études françaises_, 43(3), 111‑131. [https://doi.org/10.7202/016907ar](https://doi.org/10.7202/016907ar)


### 12. Cours synchrone : Chaînes éditoriales et systèmes de publication (27/11/2020)
L'objectif de ce cours en ligne est de comprendre l'importance des chaînes éditoriales et des systèmes de publication _derrière_ chaque artéfact produit par l'édition (numérique).

1. Définition de l'expression "chaîne éditoriale" : vers des systèmes de publication ?
2. Présentation de 3 fabriques de publication emblématiques (Quire, Zettlr et Stylo)
3. Enjeux des choix techniques de fabrication et de production

Texte à lire : Fauchié, A. et Parisot, T. (2018). Repenser les chaînes de publication par l’intégration des pratiques du développement logiciel. _Sciences du Design_, 8(2), 45‑56. [https://antoinentl.gitlab.io/readme.book/](https://antoinentl.gitlab.io/readme.book/)


### 13. Cours synchrone : Enjeux contemporains et à venir de l'édition numérique (04/12/2020)
L'objectif de cette séance est de faire un synthèse sur les cours précédents et de dessiner quelques enjeux contemporains et à venir de l'édition numérique, plus spécifiquement en milieu littéraire, voici plusieurs propositions d'approches qui s'affineront dans quelques semaines :

- pérennité
- écologie(s)
- communs
- contrôle(s)

Texte à lire: _à déterminer_.

---

## Bibliographie
**Bibliographie et textes à lire disponibles via un groupe Zotero :**  
**<a href="https://www.zotero.org/groups/2547692/fra3826_-_thories_de_ldition_numrique/library">https://www.zotero.org/groups/2547692/fra3826_-_thories_de_ldition_numrique/library</a>**

{{< bibliography >}}

---

## Plagiat et fraude
"Tout acte de plagiat, fraude, copiage, tricherie ou falsification de document commis par une étudiante, un étudiant, de même que toute participation à ces actes ou tentative de les commettre, à l’occasion d’un examen ou d’un travail faisant l’objet d’une évaluation ou dans toute autre circonstance, constituent une infraction. Vous trouverez à l’adresse suivante les différentes formes de fraude et de plagiat ainsi que les sanctions prévues par l’Université : [https://integrite.umontreal.ca](https://integrite.umontreal.ca)"
