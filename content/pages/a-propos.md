---
title: À propos
date: 2020-10-06
---
Bienvenue sur le site web du cours FRA3826 Théories de l'édition numérique.

Ce cours regroupe les contenus du site, il est destiné aux étudiant·e·s qui suivent le cours à l'automne 2020.

Contact : Antoine Fauchié, [antoine.fauchie@umontreal.ca](mailto:antoine.fauchie@umontreal.ca)
