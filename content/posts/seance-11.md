+++
author = "Antoine"
title = "Séance 11 - L'édition numérique littéraire et l'édition numérique savante"
date = "2020-11-20"
description = "Onzième séance"
layout = "diapo"
diapo = "7"
notes = "https://demo.hedgedoc.org/EdEcCK7IS6OQ4G7gpUpU2A"
+++
## 1. Qu'est-ce qu'éditer un texte littéraire en numérique ?
Cette question aussi vaste que complexe, je ne peux pas la traiter en quelques minutes, nous allons plutôt observer les étapes d'édition d'un texte littéraire, puis constater la complexité de la littérature numérique.

### 1.1. Les étapes d'édition (et leurs outils)
Il est nécessaire de rappeler quel est le contexte de l'édition littéraire _avant_ de parler de l'édition numérique.
Ces étapes sont celles habituellement mises en œuvre pour publier un texte littéraire, qu'il s'agisse de fiction ou d'essai.

Quelques étapes (génériques) d'édition :

1. écriture du _manuscrit_ par l'auteur : **traitement de texte 1** ;
2. soumission du texte finalisé ou non finalisé à l'éditeur : **format 1** ;
3. premier avis sur le texte par l'éditeur : **traitement de texte 2** + **format 2** ;
4. reprise du texte par l'auteur : **traitement de texte 1** + **format 3** ;
5. phases de révision/correction orthotypographique : **traitement de texte 3** + **format 4** ;
6. mise en forme du texte, création d'une couverture : **logiciel de PAO** + **format 5** ;
7. soumission du BAT à l'auteur par l'éditeur : **format 6** ;
8. validation du BAT par l'auteur : **format 6** ;
9. envoi en fabrication : **format 7**.

Pourquoi analyser en détail ces phases et les outils associés ?
Parce que nous sommes ici dans le cas d'un travail d'édition sur des objets littéraires relativement simples, nous allons voir que les choses se compliquent dans le cas d'œuvres hypertextuelles, _hypermédiatiques_ ou multimodales.

### 1.2. La réalité de la littérature numérique
Avant de lire le texte d'Alexandra Saemmer, il faut faire plusieurs constats :

- pluralité : il n'y a pas _une_ littérature numérique, mais beaucoup de genres différents, Alexandra Saemmer en identifie 3 dans son texte comme nous allons le voir ;
- originalité : que ce soit en terme d'écriture, de parcours narratifs ou de formes ;
- invisibilité : il faut reconnaître que le _mouvement_ de la littérature numérique est assez méconnu, les œuvres ont du mal à dépasser le public d'initiés (qui est composé en majorité d'auteurs et d'autrices de littérature numérique) ;
- non-pérennité : ces objets littéraires peuvent être considérés comme des prototypes, qui dépendent de certaines briques technologiques. Le meilleur exemple est la fin très prochaine de Flash qui va plonger un pan de la littérature numérique dans le noir.

### 1.3. À propos de l'article d'Alexandra Saemmer
Quelques éléments de contextualisation avant de commencer : Alexandra Saemmer est une chercheuse spécialisée dans la littérature numérique.
Si cet article semble pour le moins daté — 2008 c'est déjà ancien pour le temps de l'édition numérique —, ces réflexions sont précieuses et marquent un tournant important pour l'édition numérique en particulier et l'édition en général.

Pour commencer Alexandra Saemmer fait une distinction importante entre _publication_ et _édition_ : ce qui relève de l'initiative d'auteurs, sans validation ou inscription dans un corpus plus large que ce que propose l'auteur lui-même ; ce qui correspond à une démarche plus structurée, avec une instance de légitimation (voir aussi de validation).

Cet article détaille les 3 genres de la littérature numérique (ce sont mes termes) :

1. figurative : relation entre l'image et le texte (animés ou fixes) ;
2. hypertextuelle : "dynamisation de l'écrit" via l'hypertexte ;
3. programmatique : "rôle du programme".

Et au centre de cela, probablement le plus important : la constitution de communautés, donc le fait que des auteurs se réunissent pour échanger sur leurs pratiques et sur la maîtrise d'outils de création et de production (sous le nez des éditeurs mais sans les inclure systématiquement).

Si le succès commercial de ces expérimentations littéraires est presque nul (ce qui est parfois également le cas de la quête de légitimité, hormis au sein d'un petit cercle), il y a pourtant un foisonnement extrêmement riche, varié et expérimental.
Le rôle d'Alexandra Saemmer est, selon moi et au-delà de cette recherche scientifique, de donner à voir toute cette variété en proposant une structure lisible et cohérente (ce qui n'est pas forcément le rôle de celles et ceux qui créent).

Alexandra Saemmer conclut sur les problèmes soulevés : lisibilité, visibilité, pérennité.

### 1.4. Y a-t-il une édition numérique littéraire ?
Plusieurs événements :

- la constitution de communautés et de structures pour les accueillir (Electronic Literature Organization, Remue.net) ;
- la recherche scientifique autour de ces mouvements et initiatives ;
- la création d'éditeur _pure players_.

À partir de ce que nous venons de voir la question serait donc de savoir s'il y a une édition numérique littéraire ?

Qu'est-ce qu'un éditeur _pure player_ ?
C'est un éditeur qui ne propose ses titres qu'au format numérique.
Plusieurs éditeurs sont apparus, dès les débuts du livre numérique à la fin des années 2000, ne voulant proposer que des livres numériques sans en imprimer.
Publie.net est particulièrement emblématique : cette maison d'édition créée par François Bon et désormais collectif, a été l'une des premières dans le paysage francophone à proposer uniquement des livres au format numérique.
Plusieurs maisons d'édition avaient déjà commencé à faire de l'édition numérique (comme 00h00 à la fin des années 1990), mais le fait de ne pas imprimer de livres était nouveau.
D'autres suivront, comme Walrus ou Numeriklivres.

Dans les années 2015-2016 il y a toutefois un virage important : le passage à l'imprimé de ces éditeurs pure player, agacés de n'avoir aucune reconnaissance dans le paysage littéraire pour la simple raison que le papier est absent de leur catalogue.
Ce passage à l'imprimé est aussi une tentative de rendre visible des livres dans les espaces physiques comme les salons ou les librairies, matérialisations tentées à plusieurs reprises avec des procédés ingénieux mais peu efficaces (cartes avec QR codes, bornes, etc.).
Plusieurs éditeurs pure player vont donc opter pour l'impression à la demande, puis pour des petits tirages.
(D'ailleurs la lecture du [carnet de bord de Publie.net](https://www.publie.net/category/carnet-de-bord/) est un très bon moyen de comprendre le métier d'éditeur lorsque le numérique est un choix central.)

Tout cela pour dire que la seule littérature numérique à voir rejoint le circuit commercial classique est la littérature numérique _homothétique_, hormis quelques belles réalisations de livres numériques enrichis comme la revue [D'ici là](https://www.publie.net/categorie-produit/dici-la/) chez Publie.net.

La réponse serait donc oui, il y a une édition numérique littéraire, qui est le reflet des changements provoqués par le numérique sur l'édition, et le reflet de la complexité de ces changements et influences.

## 2. L'édition numérique savante : production (enjeux des formats de structuration)

### 2.1. Préambule
Il est nécessaire de faire une distinction entre deux types d'édition savante, pour comprendre les enjeux littéraires et commerciaux, il existe donc (au moins) deux types d'édition savante :

- l'édition scientifique : des textes écrits puis validés par la communauté scientifique, publiés par des institutions rattachées à des universités. La question de la rentabilité n'est pas forcément une prérogative, les textes doivent d'abord exister ;
- l'édition savante grand public : des éditeurs _privés_ qui vont avoir autant d'exigence scientifique mais des contraintes de rentabilité, et un public plus large (les universitaires et les autres pour le dire rapidement).

Dans les deux cas il y a toujours la même exigence de qualité.

### 2.2. Des contraintes de formats
Distinction préalable :

- format de lecture : PDF et HTML (et papier si papier) ;
- format de _données_ : XML comme passage obligé.

L'édition savante est contrainte par les formats de lecture, d'une autre manière que l'édition littéraire : les textes sont lus de multiples façons, ce sont autant des livres que des documents de travail.
Par exemple il faut permettre autant une lecture sur écran qu'une impression sur des imprimantes non professionnelles ou faciliter une recherche dans des corpus et dans des documents.

Les formats de lecture numérique plébiscités sont avant tout le PDF puis l'HTML.

En terme d'affichage, nous pouvons prendre l'exemple de deux plateformes :

- Érudit : [article](https://www.erudit.org/fr/revues/etudfr/2007-v43-n3-etudfr1895/016907ar/) aux formats HTML, PDF puis XML ;
- Cairn.info : [article](https://www.cairn.info/revue-les-cahiers-du-numerique-2011-3-page-47.htm) aux formats HTML et PDF.

![](/images/html-pdf-xml.png)

### 2.3. Écrire a-t-il encore le même sens ?
Écrire _au temps du balisage_.

Ici je vous renvoie aux travaux de [Marcello Vitali-Rosati](https://vitalirosati.com/), ou à ceux d'[Arthur Perret](https://www.arthurperret.fr/) (doctorant en sciences de l'information).
La question est de savoir comment les chercheurs peuvent intégrer les questions de structuration _au moment d'écrire_.
Je balaie peut-être un peu vite la question de baliser ou de ne pas baliser, pour moi il est essentiel que le travail de rédaction ou d'édition soit aussi celui de la structuration.
La question pourrait être résumée à : qui balise ?
C'est-à-dire qui va se charger de faire le travail intellectuel de structuration de l'information.
Cela ne me semble pas possible que ce soit quelqu'un d'autre que l'auteur ou l'autrice, alors que pourtant actuellement ce sont des _petites mains_ qui s'en chargent (secrétaires de rédaction notamment).

### 2.4. Les outils de production

- classique : Word to XML ;
- nouvelle tendance : XML _first_ ;
- non conventionnel : chaînes modulaires ;

Nous reparlerons en détail de ces questions d'outils et de chaînes, mais nous pouvons tout de même en dire quelques mots.

Actuellement les éditeurs se retrouvent entre :

- des exigences de production d'articles et de monographies dans des formats riches, comme le XML ;
- des pratiques d'écriture peu avancées, avec des auteurs et des autrices qui ne savent pas utiliser autre chose qu'un traitement de texte (et encore lorsqu'il est réellement utilisé).

Exemple : pour soumettre un chapitre j'ai dû adopter les recommandations de l'éditeur, soit une feuille de style Word.
Cette feuille de style n'était en fait que la liste des différentes mises en forme, impossible d'appliquer facilement une structuration sur un document, chaque titre devait être reprise à la main...

Voici un exemple de publication en _single source publishing_, un article que j'ai co-écrit avec Thomas Parisot :

- l'article sur Cairn.info : https://www.cairn.info/revue-sciences-du-design-2018-2-page-45.htm
- la version web : https://antoinentl.gitlab.io/readme.book/
- les sources de l'article : https://gitlab.com/antoinentl/readme.book/-/tree/master/article-sciences-du-design


## 3. L'édition numérique savante : diffusion (enjeux des données et des interfaces)

### 3.1. La structuration des métadonnées fines

- édition savante : impératifs de référencement et de citabilité
- la complexité est aussi du côté commerce : [ONIX](https://www.editeur.org/8/ONIX/) (ONline Information eXchange)

La question du référencement est celle de la visibilité des articles et des ouvrages dans des moteurs de recherche généralistes et spécialisés.
La citabilité c'est le fait d'être en capacité de donner toutes les informations utiles pour que le document puisse être cité facilement : ces données bibliographiques peuvent par exemple être exposées pour être moissonnées par Zotero ou par des agrégateurs (niveau individuel ou niveau collectif).
Si vous affichez le code source d'un article [sur Cairn.info](https://www.cairn.info/revue-sciences-du-design-2018-2-page-45.htm) ou [sur Érudit](https://www.erudit.org/fr/revues/etudfr/2007-v43-n3-etudfr1895/016907ar/), vous pouvez facilement voir la richesse des métadonnées présentes.

### 3.2. Les données d'abord, les interfaces ensuite

- les données sont d'abord structurées ;
- pour ensuite être affichées dans des environnements divers ;
- et pour produire des formats variés.

La question est de savoir comment structurer des données : quel modèle ? Quel **schéma** ?
Les pratiques convergent vers [le schéma JATS](https://en.wikipedia.org/wiki/Journal_Article_Tag_Suite#Example).

### 3.3. Coup d'œil sur les technologies d'édition numérique
Technologies de l’édition numérique, par Julie Blanc et Lucile Haute :

[http://recherche.julie-blanc.fr/timeline-publishing/](http://recherche.julie-blanc.fr/timeline-publishing/)

## Ce qu'il faut retenir de L'édition numérique littéraire et l'édition numérique savante

- les deux faces de la littérature numérique : expérimentale (hors circuits classiques) et homothétique (commercialisable) ;
- les contraintes commerciales ne peuvent pas cohabiter avec les formes non conventionnelles ;
- l'édition numérique est un exercice de distinction entre écriture et lecture ;
- l'édition savante est un équilibre entre structuration, données, référencement et lisibilité ;
- évolution progressive des technologies.
