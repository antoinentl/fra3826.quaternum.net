+++
author = "Antoine"
title = "Séance 10 - Qu'est-ce que l'édition numérique 2/2"
date = "2020-11-13"
description = "Dixième séance"
layout = "podcast"
audio = "fra3826-qu-est-ce-que-l-edition-numerique-2-2.mp3"
notes = "https://demo.codimd.org/z3TS3DPbR6WMUXic_aevrw"
+++
## Introduction
Bienvenue dans cette baladodiffusion consacrée à l'édition numérique.

Voici le second épisode de ce podcast intitulé "Qu'est-ce que l'édition numérique ?".
Nous allons aborder cette fois les questions de cheminement du texte et des formes produites par l'édition numérique.
Quelles sont les étapes d'édition ?
Comment le numérique modifie ces étapes ?
Pourquoi parler de versionnement ?
Qu'est-ce que l'APIisation et l'édition numérique multimodale ?
Dans cet épisode je vais tenter de répondre à ces nombreuses et passionnantes questions.

## Rappel de l'épisode précédent
Avant de commencer je vais faire un rapide rappel de l'épisode précédent.
J'ai pris le temps de détailler quelles ont été les rencontres du livre et du numérique qui ont précédé l'émergence du livre numérique et qui ont permis la constitution de l'_édition numérique_.
Les questions des modalités de travail et de formats de fichiers ont ensuite été présentées, pointant le fait que l'édition numérique n'est pas le simple ajout du numérique à l'activité d'édition.
Nous n'avons pas évoqué le concept d'éditorialisation, ou très peu, il est pourtant essentiel.
Nous y reviendrons dans une prochaine séance.

## 1. Cheminement du texte
Ce que j'appelle le cheminement du texte c'est le parcours d'un contenu depuis une proposition d'un auteur jusqu'à la mise en circulation de ce texte.
Je parle bien ici des étapes d'édition, c'est-à-dire les différentes interventions sur un texte pour en faire un livre (ou un article), après qu'il ait été créé par un auteur ou une autrice, et avant qu'un médiateur ne le diffuse.
Je ne parle pas de la chaîne du livre dans son ensemble, qui comprend en plus la diffusion, la distribution, la vente, etc.
Ce qui m'intéresse ici est de décrire rapidement les étapes d'édition et ensuite d'analyser l'influence du numérique sur ces étapes.
L'édition numérique se distingue-t-elle par une reconfiguration de ces étapes ?
C'est ce que nous allons voir.

### 1.1. Les étapes du texte
Je vais tout d'abord parler des étapes du texte, en partant de plusieurs cas différents.
Il est complexe de trouver un modèle générique, c'est-à-dire un processus d'édition comprenant des étapes qui soient communes à plusieurs domaines éditoriaux très différents comme la littérature contemporaine, l'édition savante, la bande dessinée, la poésie ou la littérature de jeunesse.
Certaines étapes se retrouvent toutefois d'un domaine à l'autre, c'est ce que nous allons voir.

Pour commencer à décrire ces étapes d'édition classique, donc sans prendre en compte les questions numériques, prenons le domaine de l'édition savante.
C'est sans doute le champ éditorial le plus complet, en raison des nombreuses étapes de révision et de validation du texte.
Donc un auteur ou une autrice soumet par exemple un article à une revue, que ce soit dans le cadre d'un appel à texte ou via une contribution plus libre.
L'éditeur ou l'éditrice réceptionne le texte et fait un premier travail de sélection : est-ce que le sujet correspond aux thématiques de la revue ?
Est-ce que l'article pourra s'insérer dans un numéro spécifique, un dossier ou un rubrique ?
Si le texte répond déjà à ces contraintes, alors le comité éditorial de la revue soumet la proposition à des experts du domaine de l'article.
Dans l'édition scientifique tout article ou ouvrage doit être validé par d'autres scientifiques.
Que ce soit une révision aveugle (cela signifie que les réviseurs restent anonymes) ou une révision ouverte, il s'agit de commenter le texte et de demander des compléments lorsque cela est nécessaire.
Il peut y avoir plusieurs allers et retours entre l'auteur et les réviseurs, malheureusement il y en a souvent trop peu.
Pour témoigner en tant qu'auteur, je me suis déjà retrouvé dans deux situations opposées : soit un suivi trop léger qui ne m'a pas permis d'améliorer mon texte comme cela aurait été nécessaire, soit au contraire j'ai bénéficié d'un véritable accompagnement qui a été l'occasion pour le texte de gagner en qualité.
Une fois cette étape validée, le travail de structuration et de mise en forme du texte commence, intégrant l'éditeur et un ou une graphiste.
C'est ici qu'il y a la plus forte rupture dans la chaîne d'édition, en raison d'outils — comme les logiciels de publication assistée par ordinateur — que tous les interlocuteurs de l'instance éditoriale ne maîtrisent pas forcément.
Le texte est validé par l'instance éditoriale, puis soumis une dernière fois à l'auteur via un bon à tirer ou BAT.
Le BAT est la dernière étape avant la production du texte et sa publication, c'est une étape très importante qui peut générer plusieurs échanges avant qu'il y ait consensus.
Ensuite l'article est mis à disposition de lecteurs et lectrices selon des modalités très variables via des intermédiaires comme des diffuseurs, des distributeurs, des libraires, des bibliothécaires ou des agrégateurs.
Des contraintes sont alors imposées par les formats produits.

Dans le cas de l'édition littéraire, ou disons de littérature contemporaine, il y a moins d'allers-retours.
Les interlocuteurs sont moins nombreux, et le plus souvent la discussion autour du texte s'amorce alors que le texte n'est pas finalisé.
Les relecteurs et les relectrices (lorsqu'il y en a) vont travailler avec la maison d'édition et pas forcément avec l'auteur ou l'autrice, leur rôle n'est pas de valider le texte dans son contenu, mais de s'assurer qu'il respecte des normes orthotypographiques.
Les formats de sortie sont aussi moins nombreux, et le texte est bien moins structuré.

Le numérique n'est-il pas censé simplifier tout cela ?
Non, c'est bien plus compliqué.
Actuellement, concernant les étapes que je viens de vous présenter rapidement, nous pouvons noter deux comportements :

- le premier concerne un travail qui vient s'ajouter sans perturber les étapes d'édition classiques : le texte est enrichi pour qu'il puisse être convenablement intégré dans les plateformes de diffusion. Il s'agit essentiellement d'un balisage plus fin et d'ajout de métadonnées, que l'auteur voir même l'éditeur ne réalise pas, et qui permet ensuite de référencer correctement l'article ou l'ouvrage. Ou alors un nouvel intervenant sera chargé de produire une nouvelle version du livre, par exemple le format EPUB pour le livre numérique. Nous sommes ici dans ce que je qualifierais de l'édition numérique homothétique : il y a une conversion numérique sans proposer de dimension nouvelle ;
- le second comportement est plus rare : il s'agit d'une reconfiguration des étapes d'édition en prenant _réellement_ en compte le numérique et ses possibilités. Cela se traduit par une horizontalité des échanges et une perméabilité des phases d'édition. Les réviseurs peuvent par exemple travailler en même temps que les personnes chargées de la mise en forme du texte, la chaîne ne s'articule plus autour des outils mais autour des fonctions nécessaires à la réalisation de tâches définies.

Le point commun de ces deux comportements qui prennent en compte le numérique, c'est la volonté de permettre une éditorialisation : le texte va être inscrit dans un espace numérique, des interactions vont naître, et pour cela il faut préparer le document.
Pour que les lecteurs et les lectrices puissent s'emparer du texte, le partager, l'annoter, le conserver, etc., il faut qu'un certain nombre de critères soient remplis.
Je ne m'étends pas plus sur le concept d'éditorialisation que vous connaissez déjà et que nous aurons l'occasion d'étudier plus longuement.

### 1.2. De formats en formats
Le cheminement du texte est une histoire de changement continuel de formats, et je ne parle pas que de formats numériques, nous pouvons constater cela dès l'impression à caractères mobiles.
Avec l'impression à caractères mobiles l'enjeu a été de transposer un document manuscrit en un document imprimé, en passant par un processus d'encodage très minimaliste : remplacer le geste de la main linéaires par des signes en plomb modulables.
Dans le cas de l'informatique puis du numérique, de nouveaux formats sont apparus, mais globalement ce fonctionnement a été conservé.
Nous devons tout de même noter que l'imprimeur s'est retiré du jeu, là où il a longtemps géré la mise en forme des documents.
Désormais il se charge uniquement de recevoir le fichier d'impression et de produire des artefacts imprimés (ce qui est déjà une tâche très complexe), en jouant parfois le rôle d'iconographe.

Ces changements de formats nous rappellent un point déjà abordé plusieurs fois dans ce cours : l'interopérabilité.
Pour pouvoir dialoguer entre humains qui utilisent des outils informatiques, il faut que les formats puissent être interprétables, compréhensibles et modifiables par tous le monde (les humains et les machines).
Si chaque phase implique un format différent que l'interlocuteur suivant ne peut pas comprendre, alors la chaîne d'édition va se transformer en un processus composé de phases hermétiques, où la communication sera difficile entre deux étapes, et où les logiciels dictent les procédures plus que les intentions éditoriales.
Pour être honnête, c'est ce qui se passe encore actuellement dans la plupart des cas...
Il est difficile d'imaginer une activité d'édition numérique sans interopérabilité.
Encore aujourd'hui nous ne pouvons constater que des tentatives de correctifs, en essayant de faire dialoguer des logiciels comme des traitements de texte...
Il s'agit plus de palliatifs qu'une réelle prise en compte des possibilités du numériques, et en disant cela je ne blame pas les éditeurs et les éditrices qui ne disposent pas encore d'outils adaptés à leurs activités.

Lorsque je parle de cheminement du texte, j'évoque principalement la question de la gestion du contenu, de sa structuration et enfin des données autour du texte.
Il ne faut pas omettre pour autant un travail essentiel : la mise en forme du texte.
Il est vain de penser que l'activité d'édition peut se passer de designers, ou pire que tout éditeur ou éditrice peut devenir designer.
Ce sont des métiers complémentaires mais très différents.
Les seuls cas où un éditeur réalise lui-même le travail de mise en forme sont ceux où la mise en forme est absente (autrement dit le texte est difficilement accessible), ou ceux où l'éditeur est en fait un designer (c'est par exemple le cas avec les Éditions B42 ou Zones sensibles):
Le diable est dans les détails : les enjeux de lisibilité mais aussi d'originalité sont prépondérants dans une activité d'édition.

### 1.3. Le versionnement du texte
Pour le cheminement du texte j'ai déjà parlé des étapes d'édition, puis des questions de formats qui matérialisent en quelque sorte ces étapes.
Il faut maintenant que j'aborde la façon dont sont gérés les fichiers au cœur de ces étapes.
Car il ne s'agit pas de travailler seul·e sur quelques fichiers, comme nous pouvons le faire au quotidien, non l'édition requiert une gestion plus complexe des interventions sur de nombreux fichiers.
Comment travailler avec de multiples rôles sans perdre d'information ?

Tout d'abord il faut faire une distinction entre édition savante (qui peut regrouper l'édition purement scientifique, mais aussi les essais, les catalogues, bref tout ce qui comporte un appareil critique et qui nécessite une validation forte), et les autres domaines.
Dans le premier cas le _cheminement_ du texte est régit par des étapes de révision et de validation, ces étapes structurent le cheminement du texte comme nous l'avons déjà vu, et dans les autres domaines beaucoup plus de responsabilités est donné à l'auteur.

En ce qui concerne la gestion des fichiers, la situation réelle est quelque peu déstabilisante.
Je vous en dresse le portrait : plusieurs personnes travaillent sur plusieurs fichiers successifs, allant du fichier Word de l'auteur jusqu'au fichier InDesign de l'infographiste en passant par un autre fichier Word de l'éditeur (différent de celui de l'auteur).
Si vous arrivez à me suivre, cela signifie qu'il y a au moins 3 fichiers différents et non compatibles, qui vont eux-même avoir plusieurs _versions_ différentes.
L'éditeur va se retrouver avec un fichier `mon-texte-final-v2.doc` provenant de l'auteur, qu'il va transformer en un fichier (probablement mieux structuré) `livre-machin-relecture-ok.doc`, que l'infographiste transformera en `editeur-truc-livre-machin-bat-v3-final.indd`.
Très vite les versions de ces fichiers vont se multiplier, jusqu'à arriver à quelque chose comme `livre-truc-relecture-2020-11-08-v3-final-valide-ok.pdf`.
Autant dire que la gestion est complexe, sans parler du fait que naviguer _entre_ ces versions est quasi mission impossible.
La gestion des fichiers est donc dans la plupart des cas une suite de protocoles trop minimalistes qui ne répondent pas aux exigences de l'activité d'édition.

Ce dont je vais vous parler maintenant ne représente pas la réalité actuelle du fonctionnement des maisons d'édition, il faut le reconnaître.
Dans un cours intitulé _Théories de l'édition numérique_, nous devons défricher et analyser les initiatives qui relèvent de démarches foncièrement numériques.
Ainsi certaines maisons d'édition commencent à utiliser des procédés de versionnement très avancés, issus du monde de l'informatique, et qui leur donne une grande souplesse.
Nous en reparlerons, mais des éditeurs comme Abrüpt en littérature ou Getty Publications en édition savante, utilisent le système de versionnement Git pour gérer les différentes versions des fichiers.
Je ne peux pas vous faire un cours détaillé sur Git tout de suite, mais ce que je peux vous dire c'est que Git offre la possibilité d'enregistrer chaque version de travail sans perdre de données, et de pouvoir y naviguer via un système décentralisé de partage de versions.

La question que je vous propose d'explorer désormais est celle des objets qui vont résulter de ce cheminement.
D'ailleurs j'aurais pu prendre le problème à l'envers, et commencer par les questions de _formats de sortie_ pour ensuite remonter jusqu'aux _formats d'entrée_.
Bref, quels sont les objets produits à travers les étapes d'une démarche d'édition numérique ?

## 2. Les formes matérielles de l'édition numérique (transitionnelles et artefactielles)
Il faut faire une première distinction importante avant de débuter l'analyse des formes matérielles produites par l'édition numérique.
Il y a des formes transitionnelles, qui correspondent aux fichiers ou aux objets de travail, qui restent dans l'enceinte de l'instance éditoriale, et il y a des formes artefactielles qui sont les objets imprimés ou numériques qui seront diffusés.
(Je vais revenir sur l'usage du terme peut-être un peu suprenant d'_artefact_.)
D'un côté nous avons ce que nous pourrions qualifier des formats pivots, autour desquels s'articulent les étapes d'édition, et d'un autre côté ce qui résulte de ces étapes d'édition.

### 2.1. Les formes matérielles
Nous parlons bien de formes matérielles, même les fichiers sont des formes matérielles.
Les centres de données qui hébergent nos courriels, les sites web, les fichiers, toutes nos vies numériques, sont des infrastructures physiques, composés de murs en béton, de matériel informatique et de vraies personnes qui sont chargées du bon fonctionnement de tout cela.
Les tentatives pour aseptiser ces centres de données sont malheureusement réelles, comme pour nous faire croire que ces lieux sont hors de notre monde, comme en témoigne Alexander Taylor [dans un article récent](https://culturemachine.net/vol-18-the-nature-of-data-centers/data-center-as-techno-wilderness/) : aucun humain ne doit apparaître sur les photos très lisses des _data centers_.

Dans le domaine de l'édition numérique le format le plus plébiscité est l'EPUB pour le livre numérique, et les formats HTML et PDF pour l'article.
Ce sont les formats auxquels les lecteurs et les lectrices accèdent, et non les formats de travail ou même de stockage que les structures d'édition ou de diffusion utilisent.
Même si le web semble mieux répondre aux usages, il est encore trop souvent absent des formats proposés.
Je pense surtout aux livres numériques qui peinent à être mis à disposition sous des modes d'accès faciles.

Plusieurs initiatives dans le courant des années 2010 ont pourtant tentées ce virage vers le web, du côté du livre numérique.
Par exemple Readmill est un service qui a existé pendant quelques années, proposant de _charger_ des livres numériques au format EPUB pour ensuite y accéder depuis une interface web ou une application dédiée.
Ce fonctionnement répondait à un enjeu réel : celui de ne pas dépendre d'applications spécifiques et ainsi de lire un livre depuis n'importe quel navigateur ou fureteur web.

Je dois faire un aparté comme j'en ai l'habitude : le fait de proposer un accès _web_ à des contenus éditorialisés impose de nous poser la question du _streaming_.
Le _streaming_ ou flux en continu est un mode de distribution de contenus numériques.
Il apparaît avec de nouveaux modes d'accès à la musique, les plateformes les plus emblématiques du _streaming_ sont probablement Spotify et Netflix.
La définition du _streaming_ semble claire : il s'agit d'un flux de données, permettant d'écouter, de voir ou de lire des contenus sans avoir besoin de télécharger de fichiers.
Le problème du téléchargement et de la lisibilité des fichiers semble réglé, puisque tout passe par une application qui affiche ces contenus (le plus souvent un navigateur ou fureteur web, mais aussi des applications pour dispositifs mobiles ou des logiciels dédiés).
Dans les faits la question est plus complexe et mérite que l'on s'y attarde quelques instants.
Car si les fichiers ne sont pas téléchargés de façon _visible_ par l'utilisateur, ils sont stockés _en cache_ sur le dispositif de lecture — et certes un peu cachés.
À quel moment le streaming n'en est plus ?
À partir de quand le flux n'en est plus ?
La question n'est-elle pas plutôt celle de la licence lié aux usages : le streaming serait une licence très temporaire d'accès à des contenus.

Voilà pour la question de la matérialité, je vais maintenant parler des formats _transitionnels_.
Produire des formats comme du PDF, de l'EPUB ou de l'HTML est finalement un détail, encore faut-il avoir des données structurées, et des interfaces adaptées pour les manipuler ou les afficher.

### 2.2. Des formats transitionnels
La question est donc de savoir comment dialoguer entre humains.
Ces humains ont des compétences propres qui se traduisent par l'utilisation et la maîtrise d'outils spécifiques (un traitement de texte pour écrire et structurer, un logiciel de publication assistée par ordinateur pour mettre en forme ce texte, un logiciel de structuration de données pour enrichir le texte, etc.).
Ce que j'appelle des _formats transitionnels_ sont des formats de fichiers qui permettent un partage d'information entre plusieurs acteurs d'un processus.
Pour pouvoir éditer un texte il faut mettre en place un certain nombre de conventions, y compris des conventions techniques sur les choix de formats de fichiers informatiques.
Est-ce que le format utilisé pour les phases de réception, relecture, validation, commentaires et structuration est un format `.docx` ou un format Markdown ?
Est-ce que ces phases nécessitent un traitement de texte comme Microsoft Word ou un éditeur de texte ?
Avec tout ce que nous avons déjà vu ensemble, vous comprenez rapidement les enjeux soulevés ici par la question des _formats transitionnels_.

Je vais _encore_ revenir sur le concept d'interopérabilité : cette nécessité de pouvoir dialoguer entre humains et donc entre machines, c'est essentiellement celle de l'ouverture des formats.
Des formats fermés ou propriétaires imposent des outils spécifiques, et empêchent un dialogue fluide entre plusieurs logiciels différents.
J'ai déjà abordé cette question de nombreuses fois mais je peux redonner un nouvel exemple : si vous travaillez avec un format de traitement de texte de type Word, vous n'êtes pas assuré qu'il soit interprété de la même façon par des versions différentes du logiciel Microsoft Word.
Cela vous est peut-être déjà arrivé de recevoir un fichier `.doc` et de découvrir, au moment de l'ouvrir, que la mise en forme n'était pas la même que celle qu'avait créé votre interlocuteur ou votre interlocutrice.
Sans parler du fait que ces fichiers ne seront peut-être plus lisibles dans quelques années.
C'est exactement ce que décrit [un article du studio de design iA](https://ia.net/topics/multichannel-text-processing) (également disponible [en version française](https://www.quaternum.net/2016/08/31/traitement-de-texte-multicanal/)).
L'article va plus loin sur la question du modèle qu'impose un format comme Word, et des problèmes très profonds des traitements de texte (que ce soit Microsoft Word, LibreOffice Writer ou même des services en ligne comme Google Docs) : nous ne savons plus vraiment ce que nous faisons avec ces outils.
L'activité d'écriture et d'édition nécessite de se réapproprier des outils plus en adéquation avec les étapes d'édition.

Pour recentrer mes propos sur cette question des formats transitionnels, il y a différents types de _transitions_ :

- à l'intérieur de la chaîne d'édition, donc _entre_ les différentes étapes que j'ai déjà décrites ;
- à l'extérieur de la chaîne d'édition, dans le cas où certains intervenants ne font pas directement partie de l'instance éditoriales ;
- et dans le temps : est-ce que ces formats de travail seront encore lisibles dans une dizaine d'années ?

Enfin, pour finir sur cette question, si je parle de formats de fichier, les données textuelles existent sous forme de fichiers (quelque soit le modèle du processus), mais il faut les envisager comme structurées en base de données.
Ce point est un peu complexe et mériterait un plus long développement, cela signifie que pour produire les différents artefacts les processus vont se servir des données dont ils ont besoin en interrogeant les différents fichiers.
Nous pourrons prolonger ce point lors de la dernière séance, si vous le souhaitez.

### 2.3. Les formes des artefacts
Nous voilà enfin arrivés à la question des objets produits par l'édition numérique.
J'utilise le terme artefact pour plusieurs raisons que je vais tenter d'expliquer :

- ce terme désigne ce qui est produit par l'homme, ou plus précisément ce qui est produit par l'art ou l'industrie ;
- parler d'artefact permet de mettre l'accent sur la notion de _fabrique_ que définit très bien Vilèm Flusser dans _Petite philosophie du design_ : le concept de fabrique tel qu'il le définit permet de repenser le rapport de l'homme à la machine, à partir de ce qui est produit ;
- parler d'artefact est un moyen de mettre au centre de notre attention le fait qu'un livre est le résultat d'une conception et de la mise en place d'outils par l'homme pour produire quelque chose.

Voilà pourquoi je vous parle d'_artefacts_.

Je ne peux pas vous parler des artefacts numériques sans faire un détour par des objets que nous connaissons bien : les livres imprimés.
Le livre tel que nous le connaissons aujourd'hui est le résultat de plusieurs millénaires de recherche et de développement, période beaucoup plus longue que — par exemple — l'informatique.
Si le livre est un objet commun dans nos cultures, il n'en est pas moins complexe.
Sa fabrication nécessite de nombreuses compétences, tout en étant résolument _low tech_ (pas besoin de batterie pour lire un livre imprimé, par exemple).
Les coûts de fabrication sont très variables, que l'on fasse de l'impression offset ou de l'impression à la demande.
Il en vas de même pour la qualité des livres produits, même si globalement l'impression des pages est relativement bonne avec les dernières technologies d'impression numérique, utilisés pour l'impression à la demande.
Là où l'on observe une nette différence, c'est sur le façonnage, pour deux raisons : il y a beaucoup de manipulations donc les coûts sont très élevés ; il faut des machines spécifiques qui sont de moins en moins présentes dans les pays où sont les maisons d'édition (alors qu'il reste des imprimeurs proches des maisons d'édition).
Pour les livres imprimés nous pouvons clairement noter une fracture entre deux types de qualité de livres :

1. des livres de grande qualité, avec un effort sur l'impression et surtout sur le façonnage ;
2. des livres de très basse qualité, surtout sur le façonnage — des livres jetables d'une certaine façon puisqu'ils ont une durée de vie très limitée, mais très accessibles financièrement.

Alessandro Ludovico, dans _Post-Digital Print_, détaille cette situation qui existe depuis une dizaine d'années.
Il précise d'ailleurs qu'un même livre peut être produit sous deux versions différentes, permettant à deux publics différents d'y accéder (en fonction de leurs besoins et de leurs finances).

Le livre numérique homothétique a permis à des nouvelles pratiques de lecture d'émerger, sans pour autant proposer — dans un premier temps — de véritables nouvelles formes.
99,90% des livres numériques sont des livres numériques qui ne proposent aucune nouveauté par rapport à leur pendant imprimé, à part une table des matières _interactives_ ou quelques liens hypertextes.

Le livre numérique enrichi semblait la promesse du numérique pour l'édition, mais nous devons pourtant constater sa faible notoriété, que ce soit du côté des maisons d'édition et du côté des lecteurs et des lectrices :

- les coûts de production sont trop importants : un livre numérique enrichi nécessite de nombreuses personnes aux compétences variées (auteurs, vidéastes, musiciens, graphistes, etc.) ;
- les dispositifs de lecture ne sont pas toujours adaptés et le modèle de l'application a ses limites.

L'un des apports majeurs de l'édition numérique est la production de multiples artefacts pour un même texte.
Les livres imprimés existent déjà sous de multiples formes, par exemple le grand format et le format poche (ou le _hardcover_ et le _paperback_ en anglais).
Le numérique a renforcé et facilité cette propension à produire différents formats de consultation pour un même livre : format EPUB portable, format PDF figé, format HTML en ligne, format application pour le _streaming_, format de données pour des livres spécifiques.
Ce que fait Getty Publications depuis plusieurs années est à ce niveau particulièrement emblématique : plusieurs des livres de la maison d'édition du musée californien sont proposés sous 4 formats différents : PDF pour une impression à la maison, livre imprimé via une impression à la demande, format EPUB pour une lecture sur liseuse, format web avec la possibilité de zoomer sur les images, format tableur pour récupérer l'ensemble des données des notices du catalogue.

Il y a donc une hybridation entre des formats papier et numériques homothétiques, et des formats numériques comprenant des fonctionnalités inédites pour des usages bien spécifiques (et qui fonctionnent bien dans le domaine de l'édition savante).
Vous avez dit multimodale ?
Oui tout à fait, et qui plus est en faisant par ailleurs du _single source publishing_ : une même source est utilisée pour produire tous ces formats.

Dans le domaine de la littérature (de création), la maison d'édition suisse Abrüpt fait quelque chose de similaire, en proposant ce qu'ils appellent des _antilivres_ statiques et dynamiques, en plus du livre imprimé plus classique.
La différence, et l'apport ici, est l'[antilivre dynamique](https://www.antilivre.org) qui propose une vraie nouvelle disposition et expérience de lecture : il s'agit à la fois d'un dispositif minimaliste et _low tech_, et une version enrichie (et non homothétique) du texte imprimé.
Pour citer Abrüpt et constater un positionnement pour le moins radical :

> L’antilivre n’a pas de forme, son impermanence dispose de toutes les formes, il se transforme sans cesse, et son information brute ne connaît aucune fixité, aucune frontière, elle fragmente son essence, distribue le commun, déploie sa liberté au-devant de nos singularités cybernétiques.

L'édition numérique se caractériserait donc par des artefacts multimodaux et hybrides, ainsi que par la mise en accès des _données_ du texte (Getty Publications et Abrüpt publient les sources de leurs livres).

### 2.4. Du texte aux données : l'APIsation du livre
Un phénomène marginal peut être observé, en prolongement de ce qui a été présenté jusqu'ici : celui de l'APIfication du livre.
De quoi est-ce que je parle ?
Il s'agit du fait que les livres pourraient devenir des APIs, des interface de programmation, permettant à des applications de se greffer dessus pour afficher telle ou telle information.
Les _livres_ deviendraient plus des entrepôts de données ouverts aux usages et aux affichages les plus divers.
C'est ce qu'a entrepris la Chaire de recherche du Canada sur les écritures numériques dans son implication dans la maison d'édition les Ateliers de [sens public].
Les livres sont structurés d'une telle façon qu'ils peuvent être réutilisés pour des visualisations ou des nouvelles interfaces de lecture.
Il serait alors possible de créer un livre différent par lecteur ou lectrice.

## Ce qu'il faut retenir
Nous sommes arrivés à la fin de ce double épisode Qu'est-ce que l'édition numérique, il est nécessaire de revenir sur les points les plus importants de ces deux baladodiffusions :

- le processus de constitution de l'édition numérique est composé de nombreuses étapes qui ont toutes une influence sur l'édition d'aujourd'hui ;
- le numérique implique une reconfiguration des étapes d'édition : horizontalité des échanges et perméabilité des phases d'édition ;
- il faut distinguer une édition numérique d'une édition numérique homothétique : dans le second cas il n'y pas de réelle prise en compte des spécificités du livre ;
- versionner le texte est un processus de gestion nécessaire pour entrer de plein pied dans le numérique ;
- l'édition numérique nécessite la mise en place de protocoles permettant des formats transitionnels communs à tous les acteurs d'une chaîne éditoriale ;
- l'édition numérique est multimodale et hybride le papier et le numérique ;
- les livres peuvent devenir des APIs ouvertes aux usages les plus divers (mais encore faut-il anticiper ces usages) ;
- l'édition numérique est tout simplement une activité d'édition contemporaine, d'aujourd'hui, qui embrasse le numérique puisque celui-ci constitue désormais notre espace.

Voilà pour ce deuxième et dernier épisode de Qu'est-ce que l'édition numérique ?
