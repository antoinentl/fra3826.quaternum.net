+++
author = "Antoine"
title = "Séance 13 - Séance finale : synthèse et enjeux contemporains et à venir de l'édition numérique"
date = "2020-12-04"
description = "Treizième séance"
layout = "diapo"
diapo = "9"
notes = "https://demo.hedgedoc.org/zXj0P_1VS1i23XnpclsxoA"
+++
## 1. Synthèse : culture numérique

- une évolution sur le temps long, itérative ;
- formats + protocoles ;
- l'héritage analogique ;
- les humanités numériques : un mouvement nécessaire (temporaire) ?

Et à travers les formats et les protocoles, les questions politiques.
(Tout cela n'est pas neutre.)
L'héritage analogique concerne les pratiques d'écriture et de lecture liées au livre papier.
Les humanités numériques sont également un pas vers l'édition numérique.

## 2. Synthèse : qu'est-ce que l'édition numérique&nbsp;?

- le livre numérique : objet numérique non identifié ?
- les rencontres (nombreuses) du livre et du numérique (avant le livre numérique) ;
- édition numérique : changement de paradigme ;
- édition numérique : multimodale et hybride ?
- édition ~~numérique~~.

La question des compromis.

## 3. Une vue globale sur les enjeux actuels de l'édition numérique

Difficile d'être exhaustif, tant les enjeux et les _défis_ sont importants, je vous propose d'en aborder quelques uns, que vous devrez bien garder en tête dans vos pratiques futures d'édition numérique.

### 3.1. L'interopérabilité

- faire dialoguer les machines ;
- ouvrir les logiciels, les formats et les protocoles ;
- licence libre et documentation.
C'est un peu la thématique sur laquelle je reviens continuellement, parce qu'elle conditionne nos usages de logiciels et de programmes.

L'enjeu ici est de pouvoir faire dialoguer des logiciels ou des programmes afin de faciliter le travail d'édition numérique, c'est la dimension _pratique_ de l'interopérabilité.
Mais il s'agit aussi de pouvoir _anticiper_ des usages que nous n'avons pas encore, et ainsi de ne pas se contenter de pouvoir faire dialoguer deux logiciels aujourd'hui, mais d'avoir toutes les possibilités possible de passer d'une solution technique à une autre, sans dépendre d'une entreprise à laquelle nous aurions confié nos données et leurs usages (pour information c'est exactement ce qui est en train de se passer à l'Université de Montréal).

Cela doit se traduire par 2 démarches parallèles :

- des licences ouvertes qui permettent la réutilisation et l'adaptation de programmes ou de logiciels ;
- la documentation de ces outils pour comprendre leur fonctionnement et leur évolution.

### 3.2. Critical Code Studies

- le code n'est pas neutre ;
- une intention humaine derrière chaque ligne de code.

Il s'agit, à travers ce courant ou cette démarche scientifique, d'interroger comment le _code_ fonctionne, quelles sont les implications sociales, 
Nous avons déjà pu aborder la question de la place des femmes dans les humanités numériques (les "petites mains" essentielles au travail de Roberto Busa), et il faudrait aussi aborder les questions intersectionnelles — [ce billet de blog de Marc Jahjah](http://www.marcjahjah.net/4553-race-intersectionnalite-et-etudes-critiques-du-code-informatique) est un bon point de départ.
Le fait que la plupart des programmes informatiques aient été écrits par des hommes blancs occidentaux pose un problème énorme.

Le code n'est pas neutre, il faut absolument se défaire de cette idée d'une technologie comme absente de toute intention.
N'oubliez pas que la moindre ligne de code est écrite par un humain, directement ou indirectement.
Les algorithmes ne sont par exemple pas exempts de déterminisme social et de biais culturels.
Et si quelqu'un vous dit que c'est une machine qui a écrit ce programme, demandez vous qui a codé cette machine.

## 4. Édition numérique et pérennité
Nous allons maintenant nous attarder sur un enjeu à venir de l'édition numérique, et qui semble avoir retenu le plus votre attention : la question de la pérennité.
Cette question est en fait un ensemble de questions, que nous pouvons aborder sous 3 angles différents.

### 4.1. La double question de l'accès aux données (sources et artéfacts)

- accès et lecture/modification des sources ;
- accès et lisibilité des artéfacts produits.

La première question, double, est celle de l'accès aux données.
Comment permettre une certaine pérennité des objets numériques produits dans le cadre d'une démarche d'édition ?
Et comment pouvoir modifier les sources de ces objets dans 5, 10 ou 20 ans ?
C'est exactement la question que s'est posée l'équipe de Getty Publications au moment de créer Quire.

L'exemple typique est celui d'un livre numérique au format application (du type application pour iOS), qui est généré par un logiciel fermé au format lui aussi fermé.

### 4.2. L'archivage (dépôt légal numérique ?)

- le dépôt légal ;
- changer continuellement de format.

D'un point de vue collectif il faut trouver des solutions pour conserver les livres numériques.
Rôle des bibliothèques.

### 4.3. La maintenabilité des programmes

- _qui_ met à jour les programmes informatiques que nous utilisons quotidiennement ?
- comment trouver un modèle de financement pour payer les heures de travail sur ces programmes ?
- un programme qui dure est un programme _vivant_.

C'est la grande question absente dans la plupart des discussions autour de la pérennité du livre et du livre numérique.
Souvent l'attention est portée surtout sur les objets (numériques) et moins sur les procédés qui permettent leur lecture ou leur production.
Ouvrir la licence d'un programme permet d'envisager que celui-ci soit réutilisé, modifié voir remplacé.

## 5. Bilan du cours
Points de départ pour la discussion :

- plus de baladodiffusions plus courtes
- accompagnement sur les annotations
- retour sur les prises de notes
- cours inversés, place aux débats
