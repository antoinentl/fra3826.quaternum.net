+++
author = "Antoine"
title = "Ressources complémentaires"
date = "2020-12-07"
description = "En vrac"
+++
## Quelques baladodiffusions

- How Books Are Made : [https://howbooksaremade.com/](https://howbooksaremade.com/) ;
- La mécanique du livre : [https://www.editionsducommun.org/blogs/podcasts/](https://www.editionsducommun.org/blogs/podcasts/) ;
- Les figures de l'édition : [https://www.franceculture.fr/emissions/series/la-generation-editeurs](https://www.franceculture.fr/emissions/series/la-generation-editeurs) ;
- Le code a changé : [https://www.franceinter.fr/emissions/le-code-a-change](https://www.franceinter.fr/emissions/le-code-a-change).

## Bibliographie sélective

{{< bibliography src="data/bonus.json" >}}
