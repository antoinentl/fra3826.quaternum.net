+++
author = "Antoine"
title = "Séance 07 - Numérique, politique, société, littérature"
date = "2020-10-16"
description = "Septième séance"
layout = "diapo"
diapo = "5"
notes = "https://demo.codimd.org/HBAlPoKLSmiqdCFlRT9-ow"
+++
L'objectif de cette séance est de survoler plusieurs thématiques liées au numérique, à partir de cas concrets.
Le but de ce court cours est de démonter le solutionnisme technologique (donc que l'usage de la technologie à tout bout de champ c'est un peu fatiguant).
Pourquoi ?
Pour tenter d'envisager une alliance de la littérature et de la technologie, et donc qu'il peut être pertinent de recourir à la technologie pour créer et diffuser la littérature.

## 1. Numérique et ouverture

Le duo _numérique_ et _ouverture_ peut être compris de plusieurs façons.
Ce qui nous intéresse ici c'est de comprendre comment un domaine comme l'édition s'est constitué dans un environnement numérique qui est loin d'être homogène.
Cet environnement numérique est aussi économique, c'est probablement un des principaux angles morts de la série sur Qu'est-ce que le numérique.
Dès sa création le Web est une invention économique.
Économie de la connaissance comme vous l'avez compris, mais aussi économie financière.
Tim Berners-Lee a par exemple essayer de vendre le concept du Web à Microsoft, qui a refusé d'investir dans ce qui semblait alors un projet peu intéressant pour la grande firme américaine.
Grâce à cette immense erreur de jugement, le Web a pu devenir ce qu'il est — ou plutôt ce qu'il a été, car le Web n'est plus si ouvert...

En ce qui concerne _numérique et ouverture_, nous pouvons parler de deux phénomènes : le logiciel libre et le libre accès.

### 1.1. Le mouvement du logiciel libre

- liberté d'usage d'un programme :
  0. la liberté d'exécuter le programme, pour tous les usages ;
  1. la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins ;
  2. la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies) ;
  3. la liberté d'améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté ;
- une autre façon d'envisager l'économie : plus value et service, plutôt que brevet ;
- publier pour partager, faire connaître, contribuer, dupliquer ;
- le coût du logiciel libre (ou open source) : _Sur quoi reposent nos infrastructures numériques ?_ de Nadia Eghbal ([https://books.openedition.org/oep/1797](https://books.openedition.org/oep/1797)).

Nous l'avons déjà vu, le logiciel libre est à la fois un usage et une approche philosophique.
En terme d'usage cela implique la gratuité même si ce n'est pas le premier objet du logiciel libre : il s'agit avant tout de la liberté de l'utilisateur ou de l'utilisatrice (même si cette liberté implique dans la grande majorité des cas une gratuité d'accès et d'usage).
Je préfère parler de logiciel libre que de logiciel open source, le logiciel libre propose un projet moins centré sur le business et est en partie moins capitaliste.

Pour vous donner un exemple concret, le professeur Marcello Vitali-Rosati que vous avez déjà pu lire et qui enseigne au Département des littératures de langue française, ne souhaite pas utiliser du matériel qui repose sur du logiciel propriétaire.
Prenons l'exemple de la carte wifi de nos ordinateurs portables.
Pour fonctionner avec le système d'exploitation (probablement Mac ou Windows pour vous, peut-être un peu Linux pour certains et certaines d'entre vous, en tout cas c'est mon cas), la carte wifi utilise un petit programme appelé pilote.
Nombreux sont les pilotes propriétaires, ce qui signifie que vous ne pouvez pas savoir ce que fait le programme puisque sa licence ne vous autorise pas à savoir comment il fonctionne.
Et donc vous ne savez pas ce que fait ce programme, ce pilote.
Pendant plusieurs mois Marcello a préféré brancher une antenne wifi dont il savait le pilote libre sur son ordinateur portable, plutôt qu'utiliser le wifi de son ordinateur.

Le logiciel libre est aussi fondamentalement une autre façon d'envisager l'économie : les structures qui proposent du logiciel libre, donc un _produit_ potentiellement gratuit, vont proposer une plus value et des services associés.
Je m'explique : plutôt que de proposer une solution numérique fermée et payante, qui sera probablement la même pour tous le monde, une structure qui produit un logiciel libre pourra travailler avec un client pour développer de nouvelles fonctionnalités, à sa demande.
Le client ne vient donc plus pour acheter un logiciel mais pour avoir un nouveau service.
Les sociétés qui produisent du logiciel libre peuvent également baser leurs revenus sur de la maintenance de produit, ou sur des services associés.
Par exemple l'outil le plus utilisé pour créer des sites web, WordPress, est un logiciel libre.
La société qui le développe, Automattic, propose des services comme de l'hébergement de sites.

Le logiciel libre implique une pratique de publication du code.
Il n'y a rien d'obligatoire mais si vous créez un programme et que vous souhaitez que celui-ci soit libre, vous allez le mettre à disposition pour que d'autres personnes l'utilisent, voir y contribuent pour l'améliorer.
Il y a aussi beaucoup de programmes libres dupliqués, parce que les utilisateurs ou utilisatrices souhaitaient changer _légèrement_ les fonctionnalités.
Grâce au logiciel libre ils et elles ont simplement à copier le programme et à le modifier.
On parle aussi de forker un programme, nous reviendrons sur ce terme propre au développement informatique et au système de version Git.

En théorie cette idée que n'importe qui peut créer des programmes est très enthousiasmant, imaginez toutes ces petites mains qui créent des programmes informatiques et les partagent librement (dans tous les sens du terme).
En pratique si vous développez un programme informatique et que vous attirez de nombreux contributeurs et contributrices, cela peut vite devenir complexe à gérer.

### 1.2. Le libre accès

> L’idée de base de l’accès ouvert est simple : faire en sorte que la littérature scientifique soit disponible en ligne sans barrières liées au prix et sans la plupart des contraintes dues à des autorisations.  
> Peter Suber, _Qu’est-ce que l’accès ouvert ?_ ([https://books.openedition.org/oep/1600](https://books.openedition.org/oep/1600))

Le libre accès, ou accès ouvert, ou encore _open access_, est en quelque sorte un mouvement héritier du logiciel libre.
Pour l'open access ou libre accès, il s'agit de la question de la diffusion des contenus.
Il ne s'agit pas d'omettre les conditions de productions (ici de textes scientifiques, et non plus de programmes, mais vous noterez que nous restons dans le texte), mais d'ouvrir par défaut la diffusion et en partie la réutilisation.

Pour comprendre ce qu'est le libre accès il faut faire un rapide point sur le fonctionnement de la publication scientifique, _actuellement_.
Un ou une chercheuse écrit un papier, un article par exemple, le propose à une revue.
La revue met en place un certain nombres de protocoles éditoriaux pour _éditer_ un texte :

- un auteur (chercheur) soumet son texte à un éditeur ;
- l'article est relu et commenté par des relecteurs et relectrices, qui sont des pairs, c'est-à-dire des scientifiques, comme la personne qui soumet son texte ;
- en fonction des retours l'auteur ou l'autrice doit corriger son texte ;
- l'éditeur publie le texte dans sa revue, et la rend disponible sous différentes formes : imprimée ou numérique ;
- les lecteurs et lectrices peuvent lire l'article moyennant un accès souvent payant, le coût étant souvent pris en charge par le laboratoire ou la bibliothèque ;
- souvent au terme d'un embargo de quelques années l'article sera disponible _librement_.

Donc si on résume à grands traits : un chercheur déjà payé, par des fonds publics, pour écrire des articles, va voir son article générer des bénéfices pour un éditeur qui aura certes assuré le travail de mise en forme et peut-être de diffusion, mais qui ne rémunère pas l'auteur ou l'autrice, pas plus que les relecteurs ou les relectrices.

## 2. Numérique et enjeux sociaux

### 2.1. Vous avez dit déconnexion ?
"Le prix de la déconnexion" par Evgeny Morozov ([https://blog.mondediplo.net/2017-02-23-Le-prix-de-la-deconnexion](https://blog.mondediplo.net/2017-02-23-Le-prix-de-la-deconnexion)) :

- se _déconnecter_ est une action désormais plurielle ;
- une injonction à la connexion ;
- les tentatives pour réguler nos usages semblent vaines ;
- la question de l'_uberisation_ et des microtâches ;
- le luxe du choix des plateformes et des services.

Il n'y a pas une déconnexion : par exemple en fonction des activités (travail, études), en distinguant les relations sociales diverses, et en prenant en compte certaines obligations (médecin, école, etc.).

Il y a une injonction à la connexion, et ce qui se passe à l'Université depuis plusieurs années en est un bon exemple, même en dehors de cette pandémie.
Presque toutes nos activités peuvent passer par des écrans connectés à un moment où un autre.

Réguler nos usages n'est pas simple : soit parce que les plateformes et les services que nous utilisons nous laissent peut de répit, soit parce que nous avons envie ou même besoin de rester connecter.

Et enfin il faut noter que choisir nos outils de communication, qui impliquent une connexion, est un luxe.
Parce qu'il faut déjà connaître ces alternatives aux grandes plateformes comme Facebook, Messenger, Whatsapp, Skype, Instagram, etc.
Ensuite il faut savoir les utiliser, voir apprendre aux autres _comment_ les utiliser.
Pour cela je vous renvoie à l'initiative d'un étudiant du Département des littératures de langue française (Louis-Olivier Brassard) : 31 raisons (de quitter Facebook) et 10 solutions pour s’en sortir ([https://byebyefacebook.loupbrun.ca](https://byebyefacebook.loupbrun.ca)).

Pour compléter cet article voici quelques ressources indiquées pendant le cours :

- _Sur les chemins noirs_, de Sylvain Tesson, http://www.gallimard.fr/Catalogue/GALLIMARD/Blanche/Sur-les-chemins-noirs ;
- _Éloge du carburateur_, de Matthew B. Crawford, https://editionsladecouverte.fr/catalogue/index-__loge_du_carburateur-9782707160065.html ;
- deux émissions sur ou avec Matthew B. Crawford : [ "Contact" par Matthew B.Crawford](https://www.franceculture.fr/emissions/les-nouveaux-chemins-de-la-connaissance/contact-par-matthew-bcrawford) et [ Notre attention est-elle une ressource limitée ?](https://www.franceculture.fr/emissions/la-grande-table-2eme-partie/notre-attention-est-elle-une-ressource-limitee).

### 2.2. Solutionnisme technologique

> Ce qui pose problème n'est pas les solutions qu'ils proposent, mais plutôt leur définition même de la question.  
> Evgeny Morozov, _Pour tout résoudre cliquez ici_, p. 18

La question du _solutionnisme technologique_ est largement traitée dans le livre d'Evgeny Morozov _Pour tout résoudre cliquez ici_.
Le "postulat" de ce livre est le suivant : en vantant les mérites de l'"efficacité, de la transparence, et de la certitude et de la perfection", et en supprimant "les tensions, l'opacité, l'ambiguïté et l'imperfection", la Silicon Valley limite notre capacité réflexive, et souhaite nous "enfermer" dans une prison numérique.
L'objectif du livre est de définir le "coût réel" de l'utopie, du "paradis" que dessine la Silicon Valley, et d'expliquer pourquoi nous ne l'avons pas défini plus tôt.

Une démarche solutionniste cherche à répondre à un problème _avec_ la technologie avant de l'avoir bien défini.

L'exemple parfait pour comprendre cela est l'apparition d'applications pour téléphone censé endiguer l'épidémie de Covid-19.
Quel est l'objectif ?
Permettre une identification des personnes contaminées pour éviter qu'elle rentre en contact avec d'autres personnes et ainsi limiter la propagation.
Sans outil numérique cela requiert un travail humain long et fastidieux (mais efficace si on y met les moyens).
Penser qu'une application va nous sauver de cette situation est peut-être un leurre, car pour que cela fonctionne il faut déjà plusieurs paramètres : le principal étant que tous le monde utilise l'application, et donc que tous le monde ait un téléphone intelligent.
Ensuite il faut s'assurer que l'application va faire exactement ce pour quoi elle a été conçu, ce qui n'était pas tout à fait le cas au début puisque quelques lignes de codes malencontreusement oubliées par l'un des développeurs envoyaient les coordonnées de chaque utilisateur et utilisatrice aux serveurs de Google.
Fort heureusement le programme a depuis été corrigé ([voir le problème en ligne](https://github.com/cds-snc/covid-alert-app/issues/1003)).

La technologie peut aussi proposer un choix à l’humain qui l’utilise, et donc déplacer la solution : non plus dans un fonctionnement technique binaire – appuyer sur un bouton ou attendre qu'un voyant s'allume – mais comme processus de décision que va prendre un être conscient et réfléchissant.
En bref il s'agit de remplacer le solutionnisme par le fonctionnalisme, comme le suggère Evgeny Morozov.


## 3. Numérique et littérature

### 3.1. Écriture numérique

- les outils d'écriture ;
- l'écriture en réseau ;
- le carnet ouvert ;
- etc.

Il s'agit ici de l'usage de l'informatique et de logiciels _pour écrire_.

Je vous renvoie à nouveau au livre de Matthew Kirschenbaum, _Track changes_, une enquête sur les pratiques d'écriture des auteurs et des autrices de littérature.
C'est une histoire des traitements de texte sous l'angle de la littérature, et on y constate que l'outil a une grande influence sur la façon d'écrire.
Un des exemples est le cas de George R R Martin et son utilisation d'un programme appelé WordStar, via un ordinateur qui n'est pas connecté à Internet.

![](/images/wordstar.png)

### 3.2. Littérature informatique ?

- l'usage de l'hypertexte ;
- l'interactivité ;
- la maîtrise de la chaîne éditoriale (de l'écriture à la diffusion).

Après la question de l'écriture, c'est celle de la production d'un artefact qui s'est posée pour les auteurs et autrices de littérature.
Le numérique a amené de nouvelles possibilités, c'est-à-dire produire des objets jusqu'ici impossible à réaliser avec le papier.
hypertexte et interactivité

Mais une nouvelle dimension : le fait de maîtriser de bout en bout la chaîne éditoriale, sans dépendre d'un éditeur.
Pour le meilleur et pour le pire : notamment le fait que beaucoup d'œuvres sont restées inconnues, et que la grande majorité des expérimentations du début des années est désormais perdue, ou peut-être archivées quelque part.
L'utilisation massive de Flash fait que certains sites (s'ils sont encore en ligne) seront très bientôt illisibles.

![](/images/serial-letters.png)

![](/images/24h-adrian.png)


## Révisions

### Révisions : définition de l'édition

Question : quelle est la différence entre l'édition et l'éditeur·trice ?

- distinction entre le processus et la structure
- édition : processus
- éditeur·trice : peut être une instance éditoriale

### Révisions : qu'est-ce que le numérique

Question : quelle est la filiation du Web avec d'autres technologies de l'information ?

- principe dans l'histoire des technologies de l'information : évolution et non rupture nette
- le Web est un service d'Internet
- le Web utilise des infrastructures déjà présentes (câbles sous-marins par exemple)

### Révisions : qu'est-ce que le numérique

Question : quelles sont les principales particularités du format HTML

- deux points importants : langage de balisage et utilisation de l'hypertexte
- format standardisé et maintenu par le W3C
- format rétro-compatible

### Révisions : humanités numériques

Question : comment peuvent être qualifiés Internet et le Web en ce qui concerne le développement des humanités numériques ?

- ce sont des _accélérateurs_
- Internet et le Web contribuent à une convergence des humanités et du numérique
- Internet et le Web permettent une diffusion rapide des réflexions
