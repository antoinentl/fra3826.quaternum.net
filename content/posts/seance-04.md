+++
author = "Antoine"
title = "Séance 04 - Qu'est-ce que le numérique 2/3"
date = "2020-09-25"
description = "Quatrième séance"
layout = "podcast"
audio = "fra3826-qu-est-ce-que-le-numerique-2-3.mp3"
notes = "https://demo.codimd.org/-yD4cjrLS6uIINsdOmjf2w#"
+++
## Introduction
Bienvenue dans cette baladodiffusion consacrée au numérique.
Ce deuxième épisode d'une série de trois épisodes va aborder les questions de protocoles et de formats comme les deux éléments structurants l'espace numérique.
Le troisième épisode sera la conclusion de cette petite série, et prendra le temps d'explorer les formats de lecture et d'écriture.
Comme la dernière fois, j'ajouterai les ressources liées à cette baladodiffusion sur la page du cours.

Après un rappel de l'épisode précédent ce podcast se découpera en deux parties :

- une première relativement courte consacrée aux protocoles ;
- une deuxième partie plus longue sur les formats.

## 0. Rappels de l'épisode précédent
Dans le premier épisode nous avons vu les deux éléments qui permettent au _numérique_ d'exister :

- l'informatique : la machine à calculer qu'est l'ordinateur permet de traiter de l'information, avec une promesse d'universalité (tout les calculs sont possibles) ;
- le réseau Internet et le Web : Internet est un réseau de réseaux, une infrastructure permettant de mettre en place des services comme le Web. Internet et le Web sont basés sur des protocoles.

Dans ce premier épisode de **Qu'est-ce que le numérique ?**, il était déjà question de protocoles et de formats !
Souvenez-vous que le Web est basé sur le protocole HTTP et le format HTML, l'un permet de communiquer et l'autre de représenter l'information.

## 1. Des protocoles
Commençons donc par les protocoles, avant de parler des formats.

### 1.1. Qu'est-ce qu'un protocole ?
Un protocole est une série de règles ou de conventions permettant de faciliter la communication entre plusieurs personnes ou plusieurs machines.
Je ne vais pas parler ici de la dimension sociale de la notion de _protocole_, mais de deux autres dimensions que sont l'informatique ou la communication.
Un protocole informatique et un protocole de communication ont en commun de permettre l'échange de données entre deux entités, et en l'occurrence des machines.
N'oublions pas que s'il s'agit de faire dialoguer des machines, ces échanges de données ont pour finalité de permettre à des humains de communiquer entre eux ou avec une machine.

Dans le cas de l'informatique il peut s'agir de faire communiquer deux ordinateurs ensemble, mais aussi des composants, des objets connectés ou encore des processus.
Ces protocoles informatiques, tout comme les protocoles de communication, doivent reposer sur des standards.
C'est-à-dire qu'il faut convenir d'un ensemble de règles communes.
Ces règles doivent être décrites le plus précisément possible, et disponibles (donc publiées).
Nous pouvons prendre un exemple hors informatique pour comprendre de quoi il s'agit.
Dans le cadre d'un cours à l'université, le protocole de communication entre le professeur et les étudiants et étudiantes est implicite mais pourtant bien établi : il est convenu que c'est le professeur qui prend la parole et qui est écouté.
La prise de parole d'un ou d'une étudiante passe par un certain nombre de règles, comme le fait de lever la main et d'attendre que le ou la professeur ait fini de parler, etc.
Nous pourrions imaginer l'établissement d'un nouveau protocole : par exemple les étudiants et les étudiantes devraient commencer par échanger entre eux avant que le professeur ne puisse prendre la parole.

Les protocoles informatiques fonctionnent de la même façon, une machine va par exemple attendre de recevoir des données avant d'en envoyer de nouvelles à une autre machine, et ainsi de suite.
Les machines ont par contre besoin que les règles soient très claires et très bien décrites.
La programmation informatique consiste à écrire ces règles dans un langage que les machines pourront comprendre.

### 1.2. L'exemple des protocoles HTML et Web
Comme nous l'avons vu dans le précédent épisode, Internet est conçu autour d'en ensemble de protocoles de communication.
Et plus particulièrement TCP et IP.
Le Web est également basé sur un protocole de communication, HTTP.
Je ne vais pas décrire le fonctionnement technique précis de ces deux protocoles, mais revenir sur certains éléments intéressants que nous avons déjà abordés :

- l'échange de paquets est la base d'Internet. Un des protocoles très facile à comprendre (mais très complexe au niveau informatique) c'est le fait qu'il y a un processus de vérification pour s'assurer que les paquets arrivent tous à destination (sans forcément prendre le même chemin) et que chacun de ces paquets est complet (qu'il ne comporte pas d'erreur) ;
- dans le cas du Web, le protocole HTTP consiste en plusieurs méthodes permettant à une machine de demander à une autre machine de réaliser une action. Par exemple la méthode GET sert à demander une ressource à une autre machine, ou encore la méthode HEAD qui consiste à demander non pas la ressource mais des informations _sur_ la ressource.

Ces deux protocoles ont pour objectifs de faciliter la communication entre plusieurs machines.

### 1.3. Les protocoles comme éléments structurants
Les protocoles peuvent être considérés comme des éléments structurants de l'espace numérique.
Ils régissent la façon dont nous pouvons communiquer, ou la façon dont les machines peuvent communiquer — ce qui revient un peu au même.
Le fait que ces protocoles soient ouverts et standardisés garantit l'établissement d'un espace relativement ouvert, et de réseaux distribués et donc décentralisés.
La question des protocoles est une question politique, de la même façon que le choix des formats est aussi quelque chose de politique comme nous le verrons plus tard.

Il est nécessaire de faire ici une courte parenthèse sur les structures qui établissent les standards à l'origine de ces protocoles.
Dans le cas d'Internet l'IETF joue un rôle particulièrement important.
L'IETF ou Internet Engineering Task Force (https://ietf.org/) est l'organisme qui est chargé de l'établissement des standards, et de la promotion de ceux-ci.
L'organisation de ce groupe informel, international et indépendant peut sembler étonnante : il suffit d'adhérer pour participer aux groupes de travail, les propositions de ce groupe doivent ensuite être rédigées selon des règles strictes, c'est ce qu'on appelle les RFC (pour Request for comments) qui deviennent des documents officiels.
La plupart de ces RFC sont des documents techniques décrivant avec le plus de précisions possibles un processus.
Pour le Web c'est le World Wide Web Consortium (ou W3C, https://www.w3.org/) qui s'occupe de la standardisation notamment de HTML.

Malgré leur apparente neutralité, ces protocoles portent également une forme de normalisation.
Ces protocoles ont une influence sur la façon dont nous communiquons entre nous, même s'il s'agit de protocoles très techniques — des milliers — dont je serais personnellement bien incapable de vous parler en détail pour la majorité d'entre eux.
Alexander Galloway a fait un travail très important de décryptage des protocoles, et plus particulièrement TCP/IP et DNS (deux protocoles d'Internet).
Il a ainsi tenté de démontrer qu'Internet n'est pas un espace de liberté totale comme on le pense parfois (son livre _Protocol: how control exists after decentralization_ explique cela en détail : https://mitpress.mit.edu/books/protocol), et que cette liberté doit être acquise, en insistant sur le fait que l'espace numérique est un espace _matériel_.
Nicole Starosielski va plus loin en analysant la constitution de l'espace numérique à travers les câble sous-marins, soulevant les implications politiques de cet espace qui est désormais le nôtre — loin du supposé _virtuel_ (voir son livre _The Undersea Network_ https://www.dukeupress.edu/the-undersea-network).

Nous pouvons également constater que la situation actuelle des silos ou des jardins fermés est bien loin des promesses utopiques d'Internet et du Web.
Des réseaux sociaux comme Facebook, Twitter ou Instagram, ou encore des moteurs de recherche hégémoniques comme Google, enferment les utilisateurs, voir dictent ce qu'ils et elles peuvent lire ou écrire.
Après cette note pessimiste — mais néanmoins réaliste — parlons des formats et de leurs enjeux eux aussi politiques.

## 2. Des formats

### 2.1. Préambule : les formats
Pour reprendre une approche très générale, le terme _format_ est polysémique.
Vous vous doutez que je vais parler ici du format informatique ou numérique, pourtant la notion de _format_ recoupe d'autres dimensions dont l'une nous intéressent particulièrement, examinons-la rapidement : le _format_ de papier.

Dans le cadre d'un cours intitulé Théories de l'édition numérique, nous sommes obligés d'observer de quoi il s'agit.
Étant un français au Québec, et quelqu'un qui s'intéresse particulièrement au livre et à l'impression, le format de papier a été une de mes plus grande surprise en arrivant à Montréal.
En France le format standard est le format A4 (nous reparlerons de _standard_), un format ISO adopté par la majorité des pays dans le monde (nous reparlerons aussi très bientôt de cette norme ISO).
Le format A4 fait 21 centimètres par 29,7 centimètres, contre 21,6 centimètres par 27,9 centimètres pour le format US Letter ou Lettre, adopté en Amérique du Nord principalement.
Ma grande surprise a été de remarquer que le format US est beaucoup plus ergonomique que le format A4 : moins haut et plus carré, il tient mieux dans la main, semble plus équilibré et consomme moins de papier.
C'est un format qui invite à la lecture, contrairement à la froideur du format A4, plus imposant, plus _administratif_.
D'ailleurs ici au Québec il y a un format de papier pour les documents administratifs, très haut (et qui dépasse toujours des pochettes...).
D'un autre côté le format A4 fait partie d'un système cohérent, il s'inscrit dans une famille de formats de papier dont le maître (désolé pour ce terme...) est le format A0, de 1 mètre carré, et qui se décline en 9 formats (le format A0 étant divisé en deux A1, le A1 en deux A2, le A2 en deux A3, et ainsi de suite).

À travers ce que je viens de vous présenter en quelques instants, j'ai posé les bases des questions sous-jacentes de la notion de _format_ et sur lesquelles nous allons largement revenir :

- la question du standard et la question de la standardisation, que nous distinguons : quel est le statut d'un format et comment nous nous mettons d'accord pour l'adopter ? ou comment permettre une adhésion autour d'un format commun ?
- la question de la normalisation du format : qui décide du fonctionnement d'un format ? qui le met à jour ? qui le diffuse ?
- la question de l'adéquation et de l'utilisabilité : est-ce que le format répond à un besoin ? comment est-il utilisé ou détourné ? (Je ne vais pas faire une trop longue digression, mais par exemple le format GIF n'a pas du tout été pensé pour ses possibilités d'image animée.)
- enfin la question du contexte et de la relation _entre_ les formats : comment des formats différents peuvent communiquer entre eux ? pourquoi certains formats disparaissent au profit d'autres ?

### 2.2. Qu'est-ce qu'un format informatique ou numérique ?
Nous pouvons aborder le texte lié à cette baladodiffusion : le chapitre "Les formats", de Viviane Boulétreau et Benoît Habert, extrait de l'ouvrage _Pratiques de l'édition numérique_.
Les deux auteurs donnent une définition très complète du format informatique ou numérique.
Les quelques lignes en exergue donnent tout de suite le ton :

> Les contenus numériques sont, par nature, encodés. Pour pouvoir être partagée, une information doit être structurée selon des standards : les formats. Le choix d’un format a des implications profondes : les informations que l’on peut transmettre changent, ainsi que leur lisibilité, leur universalité, leur agencement, leur transportabilité, leur transformabilité, etc.

Outre les questions que j'ai déjà évoquées, il y a ici quelque chose d'essentiel : c'est la conséquence du choix du format, quel que soit le domaine concerné.
Si je reprends mon exemple du format de papier, le choix de celui-ci impliquera une contrainte quand à la diffusion : le format A4 n'étant pas compris au Canada ou aux États-Unis, et le format Lettre n'est pas compris en Europe.
(Et ne parlons pas du systèmes métriques, même s'il s'agit là plus de protocole que de format.)

Je vais maintenant reprendre de façon synthétique les éléments présentés dans ce chapitre :

- **les formats sont invisibles** : la plupart du temps nous ne nous en occupons pas, tout au plus ils sont liés à des usages différents, comme le format PDF que nous allons lire et le format DOC que nous allons créer ou éditer en plus d'être lu ;
- **le format est toujours lié à un outil**. Un fichier doit être lu, le format permet la lecture du fichier par un logiciel, et même par un ou plusieurs logiciels ;
- **le format évolue dans le temps**, tout comme l'outil qui est censé le lire. Nous verrons par la suite que ce n'est pas toujours le cas, l'un évolue parfois sans l'autre ;
- **le format est une mise en forme de données**, il s'agit d'organiser l'information de telle façon qu'un outil puisse la lire et ensuite que nous, humains, puissions la lire et l’interpréter. Je cite :

> un format, c’est quelque chose qui met en forme d’une manière conventionnelle des données destinées à représenter du texte, du son, de l’image, de la vidéo, ou une combinaison des quatre.

- ensuite les auteurs prennent l'exemple du format HTML comme cas typique d'organisation de données ;
- puis Viviane Boulétreau et Benoît Habert aborde une question véritablement crucial, celle de la _catégorie_ des documents, à savoir comment ils peuvent être utilisés ou modifiés — nous y revenons dans quelques instants ;
- il est aussi question de la dépendance _entre_ plusieurs documents aux formats différents : un document au format HTML peut par exemple _appeler_ une feuille de style au format CSS pour appliquer une mise en forme ;
- la contrainte de l'encodage est également évoquée, puis l'adéquation entre usages et formats, et enfin les formats multimédia.

Avant de revenir plus longuement sur certains de ces points, nous devons faire un intermède sur les catégories de formats.

### 2.3. Intermède nécessaire : la question des formats propriétaires, de l'open source et du libre
Les formats peuvent être séparés en deux groupes distincts :

- les formats dits _propriétaires_ ;
- les formats dits _libres_ ou _ouverts_.

D'un côté les formats propriétaires sont protégés, il n'est pas possible de connaître en détail leur fonctionnement ou de les modifier.
Pourquoi vouloir connaître les détails d'un format me direz-vous ?
Attention ici je parle bien de format et non de fichier, il ne s'agit pas de vouloir connaître le contenu d'un fichier (comme un document Word au format .doc par exemple), mais de connaître le _format_, donc la façon dont les données contenus dans un fichier sont structurées.
Par exemple si vous voulez lire un document du type Word, il faut que vous puissiez comprendre comment le format fonctionne.
Pendant longtemps cela n'était pas possible, condamnant les utilisateurs et utilisatrices de traitement de texte à disposer d'une version de Word pour ouvrir le format .doc.
Le format .doc était donc un format totalement opaque.
Le format qui l'a remplacé, le format .docx, peut être lu, mais avec des conditions très strictes.
J'introduis une précision importante par rapport à ce que je viens de vous expliquer : il existe des formats propriétaires opaques et des formats propriétaires publiés (pour reprendre les termes des auteurs de ce chapitre).
Un format propriétaire peut être lu, mais avec des conditions très strictes liées à des brevets, ce qui pose une question de pérennité.
Le jour où la structure détentrice du brevet décide que le format n'est plus autorisé à être lu par n'importe quelle application, le format ne sert plus à rien et les données ne sont donc plus lisibles.

D'un autre côté nous avons les formats dits _libres_ ou _ouverts_.
Cette fois plus de libertés sont liées au format, comme le fait de voir comment le format fonctionne, et ainsi de créer une application capable de le lire ou de le modifier — pour reprendre mon exemple précédent.
Comment distinguer un format propriétaire publié d'un format ouvert ?
Aucune entrave légale ne doit s'opposer à son utilisation — donc pas de brevet contraignant par exemple.
Les spécifications techniques de ces formats doivent être publiées, offrant les outils nécessaires à leur utilisation.

Est-ce que ces catégorisations ne concernent qu'une communauté qui souhaite créer une application de lecture d'un format, ou qui veut modifier un format ?
Non, ces questions nous concernent tous et toutes, y compris les personnes qui n'utilisent pas d'ordinateur.
Notre environnement est désormais structuré par le numérique, que nous le voulions ou non.
Et la question des usages permis par tel ou tel format conditionnent donc nos usages.
Le fait qu'un format soit propriétaire ou ouvert est une question éminemment politique, comme dans le cas des protocoles.
Le choix d'un format propriétaire s'explique principalement pour des raisons économiques, quoique nous pouvons remarquer que désormais les grandes firmes du numériques optent désormais pour des formats ouverts, soit pour des raisons d'adhésion ou pour permettre une pérennité.

### 2.4. Digression sur les catégories de logiciels
Puisque nous avons parlé de protocoles et de formats, il est nécessaire de parler aussi de logiciels et des catégories de logiciels.
Nous reviendrons plus longuement dans les prochaines séances sur les logiciels de lecture et d'écriture, mais je souhaiterais prendre quelques minutes pour parler de logiciels propriétaires, de logiciels _open source_ et de logiciels libres.
Pourquoi ?
Parce que cela est lié aux catégories de formats, comme vous allez le voir.

Les **logiciels propriétaires** sont comme les formats propriétaires opaques : vous ne pouvez pas voir comment ils fonctionnent ou les modifier.
Des brevets protègent ces logiciels, et les sanctions peuvent très importantes dans certains pays.
Les **logiciels dits _open source_** sont comme les formats propriétaires publiés : vous pouvez voir comment ils fonctionnent ou les modifier mais avec des conditions qui peuvent parfois être limitées.
En pratique aujourd'hui les logiciels open source proposent les mêmes types de condition d'usage que les logiciels libres.
Et enfin le **logiciel libre** qui propose à la fois un usage très ouvert similaire au logiciel open source dans les faits, et une philosophie particulière (je vous renvoie à l'article [En quoi l'open source perd de vue l'éthique du logiciel libre](https://www.gnu.org/philosophy/open-source-misses-the-point.html) sur le site du projet GNU pour en savoir plus sur cette distinction).

Pour comprendre un peu mieux la distinction entre logiciels propriétaires d'un côté et logiciels open source ou libres de l'autre, voici les 4 libertés essentielles permises par le logiciel libre (que vous pouvez retrouver sur le site du projet GNU : https://www.gnu.org/philosophy/free-sw.fr.html) :

0. la liberté d'exécuter le programme, pour tous les usages ;
1. la liberté d'étudier le fonctionnement du programme et de l'adapter à ses besoins ;
2. la liberté de redistribuer des copies du programme (ce qui implique la possibilité aussi bien de donner que de vendre des copies) ;
3. la liberté d'améliorer le programme et de distribuer ces améliorations au public, pour en faire profiter toute la communauté.

Les logiciels propriétaires peuvent la plupart du temps être exécutés, ou utilisés, mais dans un cadre spécifique et moyennant des coûts de licence.
Les autres libertés ne sont pas permises par les logiciels dits propriétaires.

Prenons un exemple qui nous concerne toutes et tous actuellement : le logiciel Zoom.
Le logiciel Zoom est un logiciel propriétaire, donc cela signifie qu'on ne peut pas savoir comment il fonctionne précisément.
Est-ce qu'il envoie des informations à des tiers ?
Est-ce qu'il accède à certaines informations de notre ordinateur pendant que nous nous en servons ?
Impossible de le savoir.
Certes je suis bien trop incompétent en informatique pour pouvoir identifier ces points dans le cas où le logiciel serait libre ou open source.
Mais Zoom utilise des protocoles standardisés, ce qui permet d'envisager une certaine interopérabilité.
Enfin Zoom contrôle totalement les données qui transitent par le logiciel, puisque les appels Zoom utilisent forcément les serveurs de Zoom.
Personnellement je ne sais pas qui peut accéder à ces données, en revanche si le système de visioconférence était installé sur les serveurs de l'Université de Montréal, je pourrais savoir qui y a accès.

L'autre exemple est celui du logiciel de traitement de texte Microsoft Word, un logiciel propriétaire qui utilisait il y a encore pas si longtemps un format lui aussi propriétaire, le .doc.
Pendant longtemps il n'était pas possible de lire ou de modifier un fichier au format .doc sans le logiciel Word.
Ce format n'était donc absolument pas interopérable, alors qu'il s'agissait du logiciel d'écriture probablement le plus utilisé au début des années 2000.
Pour en savoir plus sur l'histoire des traitements de texte je ne peux que vous conseiller la lecture de _Track changes_ de Matthew Kirschenbaum (voici un article qui permet d'en savoir plus : https://writeside.com/2017/03/02/writing-technology/).

Enfin, prenons un dernier exemple avec le logiciel DPS d'Adobe.
En 2012, le logiciel Single d’Adobe Digital Publishing Suite (DPS) est proposé dans le cadre d’un abonnement Creative Cloud (il s'agit de la suite de logiciels d'Adobe).
Le logiciel est conçu pour créer des publications numériques pour iPhone et iPad, je cite, « de manière totalement intuitive, sans avoir à écrire la moindre ligne de code ».
Ces publications peuvent ensuite être diffusées via le magasin d’applications d’Apple (l'App Store).
Des éditeurs et des designers vont alors s’en emparer pour proposer des livres numériques interactifs.
Deux ans après son lancement, le logiciel est soudainement retiré par Adobe suite à des problèmes répétés de mises à jour du logiciel lui-même.
Le problème est que les applications publiées doivent elles aussi être mises à jour afin de suivre les évolutions des systèmes d’exploitation d’Apple et rester disponibles sur l’App Store.
Sans le logiciel, le format (propriétaire) devient inutilisable et les applications ne sont aujourd’hui plus maintenues et donc plus disponibles.
Pour en savoir plus je vous indique sur la page du cours un article qui décrit ce problème : https://www.mac4ever.com/actu/96027_adobe-met-fin-a-son-offre-de-digital-publishing-sur-ipad-via-la-suite-single-edition.

Avant de refermer cette parenthèse, la licence d'un logiciel ne suffit pas à garantir des libertés numériques pour toutes et pour tous, à garantir un accès universel, à permettre une interopérabilité, ou encore à éviter des monopoles (comme cela a pu être le cas avec le traitement de texte Microsoft Word).
Aujourd'hui le débat se déplace sur les données, comme on peut le voir sur la question des grandes firmes numériques que sont Google ou Facebook, et qui développe du logiciel libre.
Aujourd'hui l'une des entreprises qui contribue le plus au logiciel libre est sans doute Microsoft, si vous m'aviez dit cela il y a 15 ans je me serais étouffé avec mon clavier.
Je referme donc cette digression sur les catégories de logiciels.

Revenons aux formats.

### 2.5. La normalisation des formats
En dehors de cette catégorisation, il est aussi possible de _normaliser_ un format.
Pourquoi certains formats sont-ils normalisés ?
Afin de permettre une plus grande adhésion et de garantir des fonctionnalités.
Mais quelle est la différence entre un standard et une norme ?
Un standard est l'ensemble des spécifications techniques permettant de décrire le fonctionnement d'un format par exemple.
La normalisation est la validation d'un standard par un organisme composé des membres de la communauté concernée par le format.

L'exemple que prennent Viviane Boulétreau et Benoît Habert dans le chapitre "Les formats" est celui du format PDF.
Pour compléter cet exemple je peux vous parler du mien.
Entre 2008 et 2014 j'étais chargé de mener des projets de numérisation de journaux anciens dans la région Rhône-Alpes en France (cette région compte plus de 7 millions d'habitants pour vous donner une idée), et à un moment donné l'un des formats de conservation de ces numérisations était le format PDF/A, un format PDF normalisé permettant une plus grande pérennité que le format PDF habituel développé par la société Adobe.
Pourquoi utiliser ce format ?
Je cite la fiche Wikipédia dédiée au format PDF/A :

> Le principal avantage de ce format est que les fichiers au format PDF/A sont fidèles aux documents originaux : les polices, les images, les objets graphiques et la mise en forme du fichier source sont préservés, quelles que soient l'application et la plate-forme utilisées pour le créer. 

Cette normalisation garantie que ce format aura les fonctionnalités prévues, et cela en publiant un document qui précise le fonctionnement du format.
Document sur lequel les producteurs de logiciel doivent s'appuyer pour produire des fichiers au format PDF/A.

Pour terminer sur cette question de la normalisation, il s'agit d'un référentiel incontestable et commun, qui propose des solutions concrètes à la fois d'ordre techniques et commerciales.
Il faut garder en tête que les normes sont utilisées dans le cadre de relations contractuelles, et qu'elles sont censées les simplifier.

Les auteurs du chapitre évoquent aussi le format XML, ou plutôt le "métalangage" pour reprendre leurs mots.
Nous reviendrons sur le format XML par la suite.

### 2.6. La relation entre les formats
Les formats sont liés entre eux, comme nous l'avons vu tout à l'heure avec l'exemple des formats HTML et CSS.
Cette relation entre les formats est très importante, en effet la puissance de certains d'entre eux réside dans le fait de déléguer (si l'on peut dire) certaines fonctionnalités.
La grande force du format HTML est de permettre une distinction entre structuration et mise en forme.
Pour le dire autrement, le format standardisé HTML se charge de décrire les contenus d'une page web en balisant les contenus (un titre, un paragraphe, un passage en emphase).
Le format CSS permet d'appliquer une mise en forme aux différents éléments selon leur balisage (un titre en corps 18, un paragraphe en corps 12, un passage en emphase en italique).
Imaginons deux secondes que le format HTML soit ouvert et le format CSS propriétaire, nous ne pourrions pas envisager cette mise en relation.

La contrainte ici est que ces deux formats doivent être développés de concert, leur adéquation n'est permise que parce que les spécifications sont publiques, et que des efforts sont mis sur la capacité à ces formats de cohabiter voir de communiquer d'une certaine façon ensemble.

Le web est l'exemple d'une technologie ou d'un système basé sur la modularité, c'est-à-dire que les différentes fonctions du web sont divisées en plusieurs formats ou protocoles.
Plutôt que de disposer d'un système monolithique, ici les différentes briques sont distinctes, et peuvent donc potentiellement être modifiées ou remplacées, n'engageant pas forcément le système dans son ensemble.
Mais nous aurons d'autres occasion de parler de ces enjeux d'interopérabilité et de modularité.

## 3. Ce qu'il faut retenir de ce second épisode
Nous arrivons à la fin de ce second épisode tentant de définir ce qu'est le numérique, voici les différents points qu'il faut retenir :

- un protocole est une série de règles établies afin de permettre la communications entre des machines ou des humains ;
- malgré leur dimension technique les protocoles portent des valeurs et des choix politiques, tout comme les formats ;
- même sans être spécialiste de l'informatique, il convient de s'interroger sur le fonctionnement de ces protocoles.
