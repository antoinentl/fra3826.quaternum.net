+++
author = "Antoine"
title = "Séance 03 - Qu'est-ce que le numérique 1/3"
date = "2020-09-18"
description = "Troisième séance"
layout = "podcast"
audio = "fra3826-qu-est-ce-que-le-numerique-1-3.mp3"
notes = "https://demo.codimd.org/-yD4cjrLS6uIINsdOmjf2w#"
+++
## Introduction
Bienvenue dans cette baladodiffusion consacrée au numérique.
Ce premier épisode d'une série de trois épisodes va aborder une très courte histoire de l'informatique, en quelques dates, ainsi qu'une présentation un peu plus longue de la création d'Internet et du Web.
Dans le cadre du cours Théories de l'édition numérique, il est essentiel de bien comprendre qu'est-ce que le numérique.
Ces éléments de définition nous serviront pour maîtriser ce qu'est l'édition numérique.
Et d'ailleurs dans cet épisode nous allons déjà parler d'édition numérique comme vous allez le voir assez rapidement.

## 1. Courte introduction de l'informatique
Il est impossible de présenter ce qu'est l'informatique en si peu de temps, je vais donc plutôt tenter de donner quelques clés de compréhension et quelques repères historiques.
Pour comprendre ce que recoupe l'informatique il faut expliquer brièvement ce qu'est un ordinateur, retracer le parcours depuis les calculateurs analogiques jusqu'aux machines programmables, décrire la machine de Turing et enfin présenter l'évolution des machines.

### 1.1. Qu'est-ce qu'un ordinateur ?
Tout d'abord il faut écarter l'idée que l'ordinateur serait une boîte noire, ou une machine dont le comportement serait aussi incompréhensible qu'imprévisible.
L'informatique s'est fortement complexifiée depuis une trentaine d'années, sans parler du fait que la plupart des terminaux sont désormais connectés à Internet, mais ce n'est pas pour cela qu'il faut considérer un ordinateur comme une chose mystérieuse.

### 1.2. Des calculateurs analogiques aux machines programmables
Si l'informatique est une science récente, et un ensemble de technologies développées au 20e siècle, il faut garder à l'esprit que l'informatique née dès l'antiquité, soit 4000 ans avant notre ère.
Le père de l'ordinateur c'est la machine à calculer : un projet qui démarre pendant l'antiquité avec le boulier, et qui se concrétise plus tard avec les inventions de Pascal ou de Leibniz.
La grande différence entre ces prémisses et l'ordinateur réside dans l'automatisme : l'ordinateur peut faire des calculs sans intervention humaine (il y a une absence de mécanique).

La date de 1936 est un basculement dans l'histoire de l'informatique : Alan Turing publie un article fondateur sur la calculabilité, qui résout un problème fondamental de logique, qui passera à l'époque inaperçu auprès de celles et ceux qui travaillent sur les machines à calculer.
En 1936 c'est aussi une époque où les états se réarment, et beaucoup d'efforts sont mis sur la cryptographie pour sécuriser les moyens de communiquer, d'où ce besoin de calculateurs.

### 1.3. La machine de Turing
La machine de Turing est une machine conceptuelle, très basique.
Elle n'existe pas en tant que telle, c'est simplement un modèle pour penser le principe de l'informatique.
Il n'y a par exemple pas de différence entre un ordinateur d'aujourd'hui et une machine de Turing.
On parle alors de "machine universelle", car elle traite l'information de façon simple.
La machine de Turing permet de faire n'importe quel calcul, elle traite l'information de façon universelle.
Alan Turing est un personnage emblématique dans l'histoire de l'informatique (et plus globalement dans l'histoire des sciences et des techniques).
Alan Turing a été reconnu très tardivement.

### 1.4. Des premiers ordinateurs à l'informatique d'aujourd'hui
Le premier ordinateur opérationnel en fonctionnement date de 1948 ou 1949, il s'agit de projets de recherche universitaire (et de recherches publiques, c'est important à préciser).
IBM a une place particulière dans la naissance des premiers ordinateurs : d'abord société développant des outils de recensement dès la fin du 19e siècle, c'est cette entreprise qui va lancer les premiers ordinateurs (d'abord à destination d'entreprises).

Dans les années 1970 sont développés des mini-ordinateurs : une machine de la taille d'une laveuse (oui oui), que les chercheurs pouvaient acheter pour un prix abordable (enfin un prix abordable pour un laboratoire de recherche, pas pour des particuliers comme vous ou moi).
Plus tard c'est le micro-ordinateur qui va arriver, comme outil accessible et d'une taille encore plus réduite, avec à la fois une interaction homme machine et une dimension ludique.
L'un des premiers ordinateurs personnels est proposé par la société Apple, avec un produit très abouti et une image attirante.
Il s'agit là d'une évolution majeure, puisque désormais l'ordinateur peut être utilisé par n'importe qui.
C'est donc à partir de 1984 que l'ordinateur prend la forme qu'on lui connaît, Apple y a beaucoup contribué (avec les interfaces homme-machine, la souris, etc.).

Les principales évolutions depuis les années 1980 sont de trois ordres :

- la miniaturisation des machines, en commençant par les processeurs toujours plus puissants ;
- la portabilité des machines, n'oubliez pas qu'un téléphone intelligent ou une tablette est un ordinateur ;
- et enfin le plus important : les usages.

Pour finir, il faut garder à l'esprit que deux pratiques ont permis un développement rapide de l'informatique :

- tout d'abord le jeu : les premiers automates ont été créés pour amuser, et le jeu vidéo a été un véritable moteur avec des sociétés comme Attari ;
- mais aussi la musique (dans une moindre mesure), dans la lignée des synthétiseurs.

## 2. Internet et le Web
Internet et le Web ont permis la constitution d'un _espace_ numérique, se basant sur l'informatique.
Il est primordial de bien distinguer Internet et le Web, voici quelques distinctions :

- Internet est un réseau de réseaux dans lequel des services sont mis en place, comme le web ;
- ou encore : le Web est compris _dans_ Internet : sans Internet, pas de Web ;
- ou enfin : Internet est l'infrastructure qui permet à un service comme le Web de fonctionner.

### 2.1. Des prémisses de l'Internet
Commençons par ce qu'est Internet, et plus précisément les prémisses de ce réseau de réseaux.

Voici quelques événements qui permettront à Internet d'être inventé :

- en 1858 le premier câble transatlantique est tiré entre les États-Unis et l’Europe pour interconnecter les systèmes de communication américains et européens, il s'agit alors du télégraphe ;
- en 1876 Graham Bell invente le téléphone, le réseau déjà existant pour le télégraphe permettra une propagation plus rapide du téléphone ;
- en 1952 le premier ordinateur est produit, c'est IBM qui conçoit l'IBM 701 pour la défense américaine. 19 exemplaires seront fabriqués ;
- en 1955 le premier réseau informatique à but commercial est mis en place. Il s'agit du réseau informatique SABRE (pour Semi Automated Business Related Environment), un réseau réalisé par IBM. Pourquoi parler de ce réseau SABRE ? Parce qu'il relie 1200 téléscripteurs à travers les États-Unis, il est utilisé pour la réservation des vols de la compagnie American Airlines. Et c'est un réseau distribué !

Ces différents événements nous permettent d'appréhender un point essentiel dans l'histoire des techniques de communication et des médias : il n'y a jamais de rupture nette, il s'agit d'évolutions lentes, d'inventions qui se suivent et se superposent.
Par exemple Internet n'est pas arrivé de nul part, d'autres inventions antérieures ont permis à Internet d'apparaître.

Nous pouvons ajouter une date importante dans cette liste sur les prémisses d'Internet : 1934.
C'est l'année où Paul Otlet préfigure Internet, bien avant l'arrivée de l'informatique.
Voici une citation qui nous en dit plus, extraite de son _Traité de la documentation_ :

> Ici, la Table de Travail n’est plus chargée d’aucun livre. À leur place se dresse un écran et à portée un téléphone. Là-bas, au loin, dans un édifice immense, sont tous les livres et tous les renseignements, avec tout l'espace que requiert leur enregistrement et leur manutention, [...] De là, on fait apparaître sur l’écran la page à lire pour connaître la question posée par téléphone avec ou sans fil. Un écran serait double, quadruple ou décuple s'il s'agissait de multiplier les textes et les documents à confronter simultanément ; il y aurait un haut parleur si la vue devrait être aidée par une audition. Une telle hypothèse, un Wells certes l'aimerait. Utopie aujourd'hui parce qu'elle n'existe encore nulle part, mais elle pourrait bien devenir la réalité de demain pourvu que se perfectionnent encore nos méthodes et notre instrumentation.

Voyons désormais non plus les _prémisses_ d'Internet, mais ses _origines_.

### 2.2. Les origines d'Internet
L'invention d'Internet s'inscrit dans une histoire beaucoup plus longue des communications.
Internet est une invention très récente à l'échelle de l'humanité, d'autres types ou techniques de communication existent depuis bien plus longtemps (comme le téléphone, utilisé depuis le milieu du 19e siècle).
D'ailleurs si vous voulez approfondir cette histoire des communications par un biais original, je vous conseille l'excellent ouvrage d'Alexandre Laumonier aux éditions Zones sensibles, _5/6_.
Il s'agit de l'histoire des échanges boursiers, et plus spécifiquement du trading à haute fréquence.
On apprend que beaucoup de systèmes de communication ont été inventés et développés pour faciliter et accélérer les échanges boursiers.
On y apprend notamment que certains câbles ont été posés au fond de l'océan de façon la plus droite possible, pour gagner quelques millisecondes dans les échanges boursiers à haute vitesse.
Quand on sait que de tels câbles qui passent dans les océans coûtent très chers (plusieurs millions de dollars), cette anecdote à de quoi faire réfléchir.
Ce livre, _5/6_, est aussi une belle démonstration qu'un réseau comme le web est développé aussi pour des raisons commerciales, et pas uniquement par des centres de recherche universitaire ou des instances militaires.
C'est ce qui fait la complexité d'Internet, ce n'est pas un espace uniforme, tout comme notre monde n'est pas uniforme.

Ainsi, dans la longue histoire des communications, plusieurs moyens ont été mis en place pour permettre à des ordinateurs de communiquer, bien avant Internet.
Ces systèmes posaient principalement deux problèmes, la centralisation et l'interopérabilité :

- la centralisation : c'est le fait de construire un réseau dont les nœuds de ce réseau sont tous connectés à un même point central. Si ce point central n'est plus disponible, alors le réseau ne fonctionne plus. Pour vous aider à comprendre, voici une analogie : le système postal est centralisé, c'est-à-dire que chaque lettre passe par une centrale de tri qui renvoie le courrier à l'adresse indiquée. Si la centrale de tri est fermée, le courrier ne peut pas être trier et donc il ne pourra pas être distribué. Autre exemple, français cette fois : dans les années 1980 le Minitel a vu son apparition. Il s'agissait d'un terminal (ressemblant à un ordinateur) permettant d'accéder au réseau centralisé Télétel. Le problème du Minitel est que chaque fournisseur de contenu doit passer par un système centralisé, ce qui laisse peu de liberté. Par ailleurs le Minitel est un pur produit de consommation, contrairement à l'ordinateur qui est un outil de création (même si nous en avons un usage assez couramment de consommation). Internet a été pensé pour ne pas dépendre d'un seul point central, même si comme nous le verrons plus tard, en pratique c'est plus nuancé ;
- l'interopérabilité : c'est le fait de mettre en place un certain nombre de protocoles et de standards pour permettre une communication entre plusieurs entités comme des machines informatiques. Dit autrement, l'interopérabilité est "la capacité à communiquer de deux logiciels différents", pour reprendre les mots de Stéphane Bortzmeyer dans son article de blog "C’est quoi, l’interopérabilité, et pourquoi est-ce beau et bien ?" sur le site Framablog (https://framablog.org/2019/06/12/cest-quoi-linteroperabilite-et-pourquoi-est-ce-beau-et-bien/). Avant Internet la plupart des moyens de communication informatique nécessitait des systèmes d'exploitation spécifiques ou des logiciels précis. Internet a introduit l'utilisation de protocoles et de standards pour que n'importe quelle machine puisse dialoguer avec n'importe quelle autre machine. Pour vous ou pour moi, ce principe peut paraître très _basique_, pourtant dans les années 1950 c'est une petite révolution.

Comme nous l'avons déjà vu, l'un des premiers réseaux distribués est SABRE, un réseau commercial permettant la connexion entre des terminaux de gestion de billets d'avion.
Internet est également un réseau distribué : il n'est pas _centralisé_, c'est-à-dire qu'il n'est pas organisé autour d'un nœud central ; il n'est pas _décentralisé_, c'est-à-dire que le réseau n'est pas organisé autour de différents nœuds, aucune route n'est prédéfinie pour connecter deux nœuds.
Internet entend résoudre le problème de connexion inter-réseau : le défi réside dans la connexion de plusieurs réseaux physiquement séparés pour ne former qu’un seul réseau logique.
Car Internet est un réseau de réseaux, capables d'être suffisamment _résilient_.
On comprend pourquoi l'armée s'est très vite intéressée puis impliquée dans le développement d'Internet.

Mais avant de revenir brièvement sur le fonctionnement d'Internet, nous pouvons préciser comment Internet a été inventé.
Avant Internet, donc, c'est ARPANET qui fait son apparition.
Lancé en 1966, ARPAnet est le premier réseau à transfert de paquets développé aux États-Unis par la DARPA.
Nous allons revenir très bientôt sur cette question des paquets.
Les premières recherches commencent en 1966, l’ARPA (pour Advanced Research Projects Agency Network) est créée en 1969 et les premières démonstrations officielles sont réalisées en octobre 1972.
ARPAnet utilise le concept de commutation de paquets.
Internet est une réalisation qui provient à la fois de recherches scientifiques et de l'implication de l'armée américaine.
Plus tard, ce sont des structures commerciales qui investiront du temps et de l'argent dans le développement du réseau Internet.
Et, depuis ses débuts, il y a toujours eu des organisations non gouvernementales, des associations et des citoyens loin des entreprises ou des structures militaires, qui ont permis à Internet d'être ce qu'il est.

### 2.3. Le fonctionnement d'Internet
Le fonctionnement d'Internet est assez complexe, nous pouvons toutefois le présenter à travers plusieurs de ses grands principes :

- Internet est basé sur le transfert de paquets : il s'agit d'un système distribué et neutre ;
- Internet a été pensé comme un réseau neutre, c'est pour cela que vous avez sans doute entendu parler de la "neutralité d'Internet" (ou "neutralité du web") ;
- TCP/IP sont les protocoles permettant à Internet d’exister.

Revenons en détail sur ces trois points.

Qu'est-ce que le transfert ou l’aiguillage de paquets ?
Cette technique permet la décentralisation, et ainsi d’éviter que des dégradations de certains points d'un réseau remettent en cause l’intégrité du réseau.
Internet est par exemple conçu pour survivre à une attaque nucléaire.
N'oublions pas que ce réseau de réseaux est développé au moment de la guerre froide.
Le transfert de paquets, ou commutation de paquets, est basée sur le découpage d'une information en plusieurs paquets, afin de faciliter son acheminement (plutôt que d'envoyer l'information complète).
Chaque paquet comprend un entête, cet entête est très précieux car il comporte toutes les données nécessaires pour savoir de quel groupe de paquets il fait partie, et qu'ainsi l'information transmise puisse être recomposée une fois tous ses paquets envoyés.
C'est ce qu'il faut retenir ici c'est le découpage d'une information en paquets, pour que chaque paquet puisse transiter sur le réseau, peut-être par différents chemins.

Deuxième point essentiel : Internet a été pensé comme un réseau neutre.
Aucun paquet n'est prioritaire sur un autre.
Ce réseau de réseaux n'est pas conçu pour donner par exemple la priorité à une information émanent d'une structure gouvernementale par rapport à une structure commerciale ou associative.
Depuis une quinzaine d'années ce projet utopique d'Internet est quelque peu terni, mais 
On comprend mieux pourquoi certains opérateurs voient d'un mauvais œil des plateformes très gourmandes en réseau comme Netflix.
Si le réseau est neutre, il nécessite bien un investissement et un entretien.
Certains acteurs utilisent plus le réseau que d'autres, ce qui a donné l'idée aux opérateurs de faire payer plus cher certains d'entre eux.
En Europe et en Amérique du nord, Internet reste un réseau relativement neutre, mais nous pouvons constater que ce principe est régulièrement attaqué (pour des raisons de gains financiers).
Aussi, au Canada ou aux États-Unis le prix des offres d'accès à Internet dépendent du débit disponible.
Une offre de 50Mo sera plus chère qu'une offre de 25Mo.
Voici une définition de la neutralité du net pour remettre un peu les choses à leur place :

> La neutralité du Net ou la neutralité du réseau est un principe devant garantir l’égalité de traitement de tous les flux de données sur Internet. Ce principe exclut par exemple toute discrimination à l’égard de la source, de la destination ou du contenu de l’information transmise sur le réseau.

Enfin, troisième point concernant les principes et le fonctionnement d'Internet : TCP/IP.
Sans rentrer dans les détails très techniques du fonctionnement d'Internet, il faut tout de même comprendre son mécanisme.
La suite TCP/IP est l’ensemble des protocoles utilisés pour le transfert des données sur Internet.
Elle est souvent appelée TCP/IP, d’après le nom de ses deux premiers protocoles : TCP (pour Transmission Control Protocol) et IP (pour Internet Protocol).
Dit autrement, TCP/IP représente, d’une certaine façon, l’ensemble des règles de communication sur Internet et se base sur la notion d’adressage IP, c’est-à-dire le fait de fournir une adresse IP à chaque machine du réseau afin de pouvoir acheminer des paquets de données (les fameux paquets dont je parle depuis tout à l'heure).
Voici les critères de fonctionnement de TCP/IP :

- le fractionnement des messages en paquets, comme nous l'avons déjà vu ;
- l’utilisation d’un système d’adresses (pour pouvoir identifier deux machines) ;
- l’acheminement des données sur le réseau (routage), pour pouvoir définir un ou plusieurs chemins liant deux machines ;
- et enfin le contrôle des erreurs de transmission de données, pour vérifier que les paquets soient bien acheminés.

Restons en là pour le fonctionnement technique d'Internet.
Tout cela est présenté de façon très rapide, avec probablement quelques raccourcis maladroits.
Si vous souhaitez en savoir plus sur le fonctionnement d'Internet, je vous conseille la série de vidéos intitulées "Il était une fois Internet" et disponible sur le site https://iletaitunefoisinternet.fr/
Plusieurs personnalités, souvent françaises, viennent présenter les briques qui constituent Internet.

### 2.4. Contexte d'apparition du Web
Comme Internet, le Web a ses penseurs qui l'anticipent très tôt.
Ted Nelson, dès 1965, va inventer le concept d'hypertexte, ensuite repris par Tim Berners-Lee.
Alors que l'informatique n'en est qu'à ses balbutiements, Ted Nelson théorise un type de lien entre des documents numériques, un lien qui n'est pas linéaire comme les pages d'un livre.
Ce qui est notable ici c'est que cette invention concerne la littérature, contrairement au projet du Web de Tim Berners-Lee qui sera plutôt documentaire.
Ted Neslon est influencé par Vannevar Bush, et notamment son memex.
La filiation est donc la suivante (pour raccourcir quelque peu) : Vannevar Bush, Ted Nelson puis Tim Berners-Lee et Robert Caillaux.

Il est temps d'aborder le texte lié à ce podcast : "Information Management: A Proposal" de Tim Berners-Lee.
Ce texte est une lettre d'intention, un ensemble d'arguments pour convaincre le CERN d'opter pour (je cite) une solution de gestion des documents basée sur un système hypertextuel distribué.
Nous sommes alors en 1989 et Internet est utilisé depuis déjà plusieurs années, principalement par des structures universitaires et des entreprises (l'usage d'Internet par des particuliers est encore relativement rare).
Les principaux services disponibles via le réseau Internet sont le courrier électronique ou l'échange de fichiers.
Attention, l'informatique des années 1980 commence tout juste à intégrer des interfaces homme-machine qui ressemble un peu à ce que l'on connaît aujourd'hui.
La plupart du temps les chercheurs utilisent des outils comme un terminal pour échanger des documents.
Imaginez un peu la situation : des centaines de chercheurs du CERN qui partagent entre eux des documents qui ne peuvent pas être connectés par des liens.
On comprend pourquoi l'invention de Tim Berners-Lee a été essentielle pour la recherche scientifique, et plus particulièrement la publication scientifique.

Donc dans ce texte Tim Berners-Lee propose une solution à un problème complexe : comment permettre une meilleure circulation des documents au sein de la grande organisation qu'est le CERN ?
Tim Berners-Lee fait plusieurs constats, dont le fait que l'information se perd dans la toile (ou "web") que constituent l'interconnexion de plusieurs milliers de personnes.
Pourquoi des informations sont perdues ?
Parce que le schéma organisationnel de l'information est _linéaire_ : pour résoudre ce problème pourquoi ne pas penser des systèmes d'information liés ?
Un modèle en arbre, ou l'utilisation de mots-clés ne suffisent pas à organiser une telle masse d'information, alors qu'un système _tabulaire_ permet d'envisager un autre schéma logique (nous reparlerons d'ailleurs de cette notion de tabularité par la suite).
L'hypertexte permet de lier de façon plus fine les nombreuses informations, sans dépendre des limites imposées par les arborescences de fichiers.
Tim Berners-Lee expose les contraintes, notamment le fait que plusieurs systèmes d'exploitation différents sont présents au CERN, et qu'il faut donc que l'application fonctionne de la même façon quelque soit l'OS.

Je ne m'étends pas sur les détails techniques exposés dans ce document, ce qu'il faut retenir c'est que le web a été inventé d'abord dans un but scientifique, et dans la perspective d'organiser le savoir.
Le Web applicatif que nous connaissons aujourd'hui, par exemple les systèmes de communications de chat ou de courriel intégrés dans le navigateur, les plateformes de musique ou de vidéo, ou même les bibliothèques numériques, découlent de cette _première_ invention.

### 2.5. Le fonctionnement du Web
Comment fonctionne le Web ?
Nous l'avons vu, Internet est un réseau de réseaux permettant à certains services de fonctionner.
Internet est le réseau, le Web est le service (et sans réseau, pas de service).

Le Web repose essentiellement sur les quatre éléments suivants :

- HTTP : un protocole de communication utilisant Internet ;
- l’hypertexte : les pages web sont reliées entre elles grâce à des liens hypertextes ;
- HTML : un langage de balisage pour décrire les pages web ;
- un serveur HTTP (là où sont hébergés les sites web) sert des ressources à un client HTTP (un navigateur ou fureteur).

Nous avons donc d'un côté un protocole de communication, HTTP (pour HyperText Transfer Protocol), et de l'autre côté un langage de balisage permettant de structurer l'information afin qu'elle puisse être affichée partout de la même façon, le HTML (pour HyperText Markup Language).
Nous constatons ici que lorsque nous parlons de web nous parlons de publication ! 
D’autres langages font partie du Web : CSS (pour Cascade Style Sheets, ou feuilles de style en cascade) qui permet de gérer la mise en forme, et JavaScript pour les interactions (omniprésent depuis une dizaine d'années).

Le W3C est l’organisme qui standardise les différents langages et les différents formats du Web : son but est de permettre une compatibilité des technologies du Web (HTML, CSS, mais aussi d'autres formats comme le SVG dont nous reparlerons).
Le leitmotiv du W3C est "Un seul web partout et pour tous".
L’accessibilité est notamment un chantier important du W3C.
Les navigateurs comme Firefox, Chrome ou Edge participent aux travaux du W3C, et implémentent les standards ou bataillent pour en proposer de nouveaux.
Oui batailler c'est vraiment le cas de le dire, comme par exemple lorsque l'équipe du navigateur Chrome (développé par Google) a insisté sur la standardisation d'une mesure technique de protection (ou DRM), une demande qui émanait en fait d'entreprises comme Netflix.

Enfin, et nous en reparlerons, le Web peut également être un outil de conception et de production.
Aujourd'hui des maisons d'édition conçoivent certaines de leur publication non pas avec un logiciel classique de publication assistée par ordinateur (comme InDesign), mais avec un navigateur (et un éditeur de texte).
La solution d'organisation des connaissances inventée par Tim Berners-Lee a été détournée tout en respectant l'idée originelle.

## 3. Ce qu'il faut retenir de ce premier épisode
Le premier épisode de Qu'est-ce que le numérique ? s'achève, il est temps de noter les points importants :

- l'ordinateur est un machine à calculer, l'informatique porte avec elle une promesse d'universalité ;
- il faut distinguer Internet et le Web : Internet est l'infrastructure et le Web est un service qui utilise cette infrastructure ;
- Internet et le Web sont basés sur des standards et des protocoles ;
- Internet et le Web sont interopérables et distribués ;
- enfin tout cela est bien matériel : pensez aux appareils que nous avons sous nos doigts ou dans nos oreilles, aux centres de données qui hébergent le fichier que vous écoutez, ou encore aux câbles transatlantiques dont certains requins semble raffoler.

Vous pourrez retrouver quelques-unes des ressources citées dans cette baladodiffusion :

- Pour une histoire de l'informatique, une émission de France Culture : https://www.franceculture.fr/emissions/les-passeurs-de-science-l-informatique/pour-une-histoire-de-l-informatique
- "Il était une fois Internet", une série de vidéos sur ce qu'est Internet : https://iletaitunefoisinternet.fr/
- le documentaire World Brain : https://www.arte.tv/fr/videos/050970-001-A/world-brain/
