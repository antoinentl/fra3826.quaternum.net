+++
author = "Antoine"
title = "Séance 05 - Qu'est-ce que le numérique 3/3"
date = "2020-10-02"
description = "Cinquième séance"
layout = "podcast"
audio = "fra3826-qu-est-ce-que-le-numerique-3-3.mp3"
notes = "https://demo.codimd.org/-yD4cjrLS6uIINsdOmjf2w#"
+++
## Introduction
Bienvenue dans cette baladodiffusion consacrée au numérique.
Ce troisième et dernier épisode d'une série de trois épisodes va aborder les questions de formats, non plus sur les enjeux de standards ou d'ouverture, mais sur ceux de lecture et d'écriture.
L'objectif de cette baladodiffusion est de présenter les formats d'écriture et de lecture numérique que nous utilisons désormais toutes et tous au quotidien, et plus spécifiquement dans nos activités littéraires.
Cet épisode est important car je vais présenter plusieurs éléments qui seront repris dans les prochains séances, et surtout celles concernant l'édition numérique et la lecture numérique.

Contrairement à ce que j'avais indiqué dans le plan de cours, je vais inverser les deux parties de cet épisode :

1. nous allons d'abord parler des formats de lecture numérique, pour comprendre qu'elle a été l'évolution récente des formats liée aux pratiques ;
2. pour ensuite aborder la question des format d'écriture numérique, et donc commencer à définir _comment_ nous écrivons en numérique, avant de pouvoir parler de comment nous _éditons_ en numérique.

## 0. Rappels de l'épisode précédent
Dans le précédent épisode j'ai lourdement insisté sur le fait que les protocoles comme les formats portent des enjeux politiques : pouvons-nous utiliser librement les formats ? Quelles sont les catégories de format et quelles contraintes imposent ces catégories ? Comment l'espace numérique se constitue autour de ces formats et des protocoles ?

J'ai brièvement commencé à parler des questions de lecture et d'écriture, au moins dans un sens _informatique_ : les ordinateurs lisent les fichiers et écrivent _dans_ ces fichiers.
Aujourd'hui nous allons parler plus spécifiquement de la lecture et de l'écriture des humains, et moins des machines.
Commençons !

## 1. Les formats de lecture numérique
Nous utilisons au quotidien plusieurs formats de fichier informatique, des formats qui sont destinés à être lus par l'entremise d'un ordinateur.
PDF, HTML, DOC, EPUB, autant de formats qui intègrent des contraintes techniques, des contraintes d'usage et des contraintes légales ou contractuelles.
Commençons par définir ce qu'est un "format de lecture numérique".

### 1.1. Définissons ce qu'est un format de lecture numérique
L'expression "format de lecture numérique" pourrait porter à confusion, il faut que je lève toute ambiguïté sur cette expression.
Pour être tout à fait clair il s'agit d'un format de fichier informatique, fichier qui est destiné à être lu par un humain avec l'aide d'un ordinateur.
Il s'agit de "lecture numérique" pour faire la distinction avec un fichier dont l'objectif serait qu'il soit _imprimé_, et donc dont la vocation serait d'être interprété par une machine avant d'être envoyé à une imprimante.
Nous allons voir comment un format de lecture numérique se distingue d'un format destiné à être imprimé, mais aussi les points communs qui peuvent exister entre ces deux catégories.

Avant de parler de formats de lecture numérique et donc de fichiers informatiques, il faut que nous parlions des questions de lecture, et des différence entre lecture linéaire et lecture tabulaire.
Pour cela nous allons utiliser le texte de Christian Vandendorpe : le chapitre "Linéarité et tabularité" extrait de son ouvrage _Du papyrus à l'hypertexte: essai sur les mutations du texte et de l'écriture_.
Ce livre, publié en 1999, est d’un apport majeur dans l’étude des médias et dans l’histoire du document.
Cet ouvrage représente une avant-garde théorique notable, au moment où l’hypertexte, Internet et le web étaient encore peu étudiés dans le domaine des sciences humaines et sociales.
C'est à travers une cinquantaine de concepts ou de notions que Christian Vandendorpe définit avec précision toutes les possibilités mais aussi tous les questionnements qu’impose l’hypertexte.
Et cela dans une perspective d’inscription, d’écriture, et de lecture.
Textualité, interactivité, images, livre, page, etc. Les sujets abordés par l’auteur sont nombreux, et souvent accompagnés d’exemples illustratifs.
_Du papyrus à l’hypertexte_ a également permis d’identifier, en creux, des points essentiels dans le domaine de l’écriture numérique.

Avant de commencer à parler du chapitre que vous avez lu, il est intéressant de voir comment l'ouvrage est construit et comment l'auteur l'a écrit.
Je cite Christian Vandendorpe dans l'introduction :

> Par honnêteté intellectuelle, autant que par esprit de recherche, l’essentiel de la présente réflexion a donc été d’abord rédigé à l’aide d’un outil d’édition hypertextuelle élaboré à cette fin et dont les fonctions se sont raffinées au fur et à mesure que se précisaient les besoins. Ce n’est qu’à l’étape finale de la rédaction que les pages ainsi créées ont été intégrées dans un traitement de texte et retravaillée en vue d’une publication imprimée. (p. 10)
  
La démarche de Christian Vandendorpe a donc été performative, même si le résultat (le livre édité et imprimé) n’en garde aucune trace.
C’est dommage, mais en même temps cette édition date de 1999 et les expérimentations en édition numérique étaient encore timides.

Les chapitres du livre, relativement courts, peuvent être lus de façon autonome dans la plupart des cas.
_Du papyrus à l'hypertexte_ appartient donc à cette catégorie d'ouvrage ou d'essai dont les parties peuvent à la fois être lues de façon indépendante, et qui forment aussi un ensemble cohérent.
Le lecteur peut donc recomposer son propre livre en réorganisant les chapitres, ou tout simplement en naviguant de partie en partie en fonction des liens qu'utilise Christian Vandendorpe pour lier les texte entre eux.

Je vous parle déjà de linéarité et de tabularité ici.
Pour distinguer rapidement ces deux termes, nous pouvons dire qu'une lecture linéaire est une lecture de bout en bout d'un texte, sans passer d'une partie pour revenir à une autre, avec cette dimension temporelle qui fait qu'on ne peut qu'_avancer_.
Une lecture tabulaire est au contraire une lecture qui navigue d'un fragment à un autre, qui se compose non pas en suivant le flot des lettres et des mots qui se suivent, mais en utilisant d'autre moyens comme la structuration du texte (en passant d'un chapitre à un autre sans forcément respecter l'ordre) ou l'hypertexte (permettant de lier plusieurs documents).

Il faut nuancer cette première définition, d'ailleurs Christian Vandendorpe nous prévient tout de suite : l'image fantasmée de la lecture papier forcément linéaire doit être vite oubliée.
Nombreux sont les types de livre qui ne sont pas conçus pour être lus de façon linéaire (comme le dictionnaire ou l'encyclopédie, mais aussi les livres dont vous êtes le héros), ou les livres qu'on ne lit pas de façon _linéaire_ — nous le savons bien à l'université dans nos usages de lecture fragmentée.
Christian Vandendorpe donne une dimension très intéressante au concept de tabularité : le fait que c'est le lecteur qui choisit les informations en fonction d'indices ou de moyens, contrairement à la lecture linéaire qui est préétablie.
La lecture linéaire est d'une certaine façon une lecture contrainte, qui respecte un agencement textuel ou graphique imposé par un auteur ou un éditeur.
Pourtant, et contrairement à la parole, le texte offre la possibilité d'être parcouru librement par le regard du lecteur ou de la lectrice, qui navigue ainsi de phrases en phrases à son gré.
Christian Vandendorpe donne un panorama tout en nuances de ce qui relève de la lecture linéaire et ce qui relève plus de la lecture tabulaire : il explore ainsi les genres littéraires qui se placent dans l'une ou l'autre de ces catégories.

Pourquoi utiliser ce texte qui parle finalement peu de lecture numérique ?
Parce qu'il est essentiel ici de comprendre à la fois la richesse du codex et aussi sa complexité.
(Le codex c'est le livre sous une forme paginée.)
L'hypertexte va permettre de prolonger les moyens déjà mis en œuvre dans les supports _analogiques_, quoique cela n'est pas du tout automatique :

> Précisons encore que, si un roman sur papier est loin d’être automatiquement linéaire, un hypertexte n’est pas non plus nécessairement non linéaire. (p. 47)

C'est dans le chapitre suivant que Christian Vandendorpe va réellement poser les principes de la tabularité pour le livre :

> Un ouvrage est donc dit tabulaire quand il permet le déploiement dans l’espace et la mise en évidence simultanée de divers éléments susceptibles d’aider le lecteur à en identifier les articulations et à trouver aussi rapidement que possible les informations qui l’intéressent. (p. 65)

La lecture numérique se caractériserait donc non pas par sa dimension faussement immatérielle — il n'y a plus de support matériel comme le papier, dans les faits c'est tout de même plus complexe puisqu'un ordinateur et des serveurs sont tout ce qu'il y a de matériel —, mais la lecture numérique se définirait plutôt par le fait qu'elle se compose au moment où elle s'exerce.
Et l'hypertexte joue ici un rôle essentiel.
Le numérique permet un déploiement de la tabularité qui dépasse la structuration du texte (comme les paragraphes, les passages en exergue, les chapitres, les volumes), et qui dépasse aussi l'utilisation de moyens _graphiques_.
En effet c'est ce que met en avant Christian Vandendorpe : c'est grâce à des indications visuelles que le lecteur peut _naviguer_ dans un texte ou entre des textes.
C'est peut-être le point d'achoppement de ce livre — au demeurant fort complet —, il faut aussi considérer le texte comme une organisation de données, et dont l'agencement dépend directement ou indirectement des humains.
Je peux vous donner un exemple pour illustrer cela : le livre numérique enrichi _Kadath, Le guide la cité inconnue_ de la maison d'édition Mnémos propose une navigation dans les chapitres du livre par le biais d'une carte.
Le livre numérique a la capacité de mémoriser les parties lues par le lecteur ou la lectrice, et peut ainsi ensuite indiquer au lecteur quelle partie suit, tout en lui laissant le choix sur son parcours de lecture.
C'est un système qui offre une fonctionnalité que le papier ne peut pas offrir.
(Attention il ne s'agit pas de hiérarchiser papier et numérique, mais de constater et d'analyser les différences.)
En parlant d'articulation entre papier et numérique, voyons justement quelle est l'influence du papier sur les formats dits de lecture numériques.

### 1.2. L'influence du papier
Lorsque nous regardons les formats de fichiers que nous lisons régulièrement, nous pouvons constater que les formats les plus fréquemment utilisés reproduisent des fonctionnement des usages liés _au papier_.
L'analogique a encore une grande influence, et des formats comme le PDF ou le DOC ou DOCX le prouvent avec beaucoup d'acuité.
Ces formats peuvent permettre une lecture tabulaire, mais il s'agit le plus souvent d'une lecture linéaire, et parfois encore plus qu'avec un livre imprimé : reconnaissons qu'il est moins évident de naviguer dans un long document PDF si nous n'avons pas à disposition des outils de navigation comme les liens hypertextes, la sélection de la page ou l'indication de l'_épaisseur_ du document.
Et que dire du document DOC qui imite jusqu'au format de page du papier, alors que bien souvent nous utilisons un traitement de texte pour créer et éditer des documents qui resteront dans nos ordinateurs et qui ne seront destinés qu'à être lus sur écran.
Le numérique hérite de nos pratiques de lectures sur supports imprimés.

### 1.3. Le changement de paradigme induit par l'HTML
Le format HTML introduit un nouveau paradigme : il n'est plus question d'afficher une interface qui reproduit la page papier, mais il est désormais possible de prendre la mesure de l'écran dans son entier, ainsi que des terminaux que sont le clavier ou la souris.
Le texte peut se présenter _au kilomètre_, et par défaut il s'adapte au contexte de lecture.
Les pages sont reliées entre elles grâce à des hyperliens, ce qui permet d'envisager des organisations complexes, sur plusieurs plans, et _entre documents_.

Mais qu'est-ce que le HTML ?
Je ne vais pas vous faire un cours complet sur ce format, quoi qu'il y aurait largement matière à plusieurs dizaines d'heures.
Et si le cours avait pris une disposition plus classique et moins distancielle, nous aurions pu apprendre ensemble quelques rudiments de HTML.
Donc l'HyperText Markup Language est, comme son nom l'indique, un langage de balisage hypertextuel.
Il repose sur un système de balises, et intègre l'hypertexte.
Un système de balises cela veut dire que la structure d'un document est signifiée par l'usage de balises, voici quelques exemples :

- pour indiquer qu'une suite de mots est un titre de niveau 1 (soit le titre principal), il suffit d'ajouter la balise ouvrante `<h1>` au début et la balise fermante `</h1>` à la fin. Dans ce cas il s'agit d'un bloc de texte ;
- pour indiquer qu'un passage est en emphase, et plus particulièrement en italique, il faut ajouter la balise `<em>` ouvrante au début et la balise `</em>` fermante à la fin. Dans ce cas il s'agit d'un élément en ligne, donc à l'intérieur d'un bloc et _dans_ le texte ;
- en plus de cela il est possible d'indiquer des métadonnées pour décrire la page (selon différents standards).

Il est par ailleurs possible d'intégrer des liens via un système d'adresse (à l'intérieur d'une même page, ou entre plusieurs pages d'un même site web, ou encore entre des pages de sites web différents), ce qui permet la constitution de documents complexes.

Le format HTML fait partie de la grande famille des langages de balisage, et il tend plutôt du côté des balisages complexes, contrairement aux balisages dits légers comme le format Markdown.
Le grand parent de ces langages est le SGML (https://fr.wikipedia.org/wiki/Standard_Generalized_Markup_Language), qui est plus un concept qu'un langage à proprement parlé.
Il est conçu dans les années 1980 pour se distinguer des langages de type script pensés pour l'impression, il s'agit alors d'une petite révolution : avec le GML (pour Generalized Markup Language) puis le SGML, nous passons à un langage **descriptif**.
La structure logique d'un document est distincte de sa mise en forme, placée dans un autre fichier.
(Nous aurons l'occasion de reparler du format XML, autre format très important en édition numérique.)

Car le format HTML est rarement seul, il est souvent accompagné d'une feuille de style, un fichier au format CSS (pour Cascade Style Sheet, ou feuille de style en cascade).
C'est ce format de fichier qui permet de gérer la mise en forme d'une page HTML.
Pour chaque balise un style peut être appliqué, voici quelques exemples (sans rentrer dans les détails), en reprenant les cas mentionnés juste avant :

- pour que nous puissions reconnaître _visuellement_ un titre de niveau 1, balisé grâce aux balises `<h1>`, nous pouvons indiquer en CSS que ce titre doit être dans telle taille de police, et en capitales par exemple ;
- pour le passage en emphase, il suffit de faire correspondre au balisage `<em>` un style typographique en italique ;
- etc.

Le format HTML est inventé par Tim Berners-Lee, l'inventeur du Web avec Robert Cailliau, comme une simplification de SGML tout en respectant son principe de fonctionnement.
HTML a connu des versions successives, jusqu'à HTML5 qui est une spécification évolutive.
Actuellement le format HTML est un langage standard maintenu par une communauté coordonnée par le W3C.
C'est un langage **vivant**.

### 1.4. Le format EPUB
Nous allons abondamment reparler du format EPUB, mais je dois vous en dire quelques mots tout de suite.
C'est le format utilisé pour le livre numérique — EPUB voulant dire E-publication, soit publication électronique.
Ce format est un moyen de rendre portable un format comme le HTML.
Pourquoi ce besoin de portabilité ?
Le HTML est un format qui s'utilise avec d'autres fichiers et formats.
Il n'est pas possible de mettre dans un même fichier un fichier HTML, une feuille de style CSS et des contenus multimédias comme des images ou des fichiers son ou vidéo.
La solution qui a été trouvée est un encapsulement de ces fichiers, comme une boîte dans laquelle on entrepose les différents fichiers correspondant aux chapitres, mais aussi les fichiers des polices typographiques, ou encore les feuilles de style et les images.
Cette _boîte_ comprend aussi un manifeste pour déclarer l'organisation des différents fichiers — et surtout l'agencement des parties de ce qui constitue un livre.

Vous pourriez me dire qu'il y a un déjà un format portable qui a fait ses preuves : le PDF.
Effectivement le format PDF est en bonne position, PDF voulant d'ailleurs dire Portable Document Format.
Mais le PDF est d'abord conçu pour conserver une mise en forme qui sera identique quelque soit le contexte de lecture.
Vous voulez transmettre un document dont la mise en page est déterminante ?
Avec le format PDF vous assurez un affichage identique quelque soit le dispositif de lecture.
Le format PDF a toutefois deux inconvénients majeurs :

1. si vous affichez un _grand_ document sur un petit écran, la lisibilité va vite être limitée. Plus vous zoomez sur le document et moins vous aurez d'informations affichées, cela vous obligera à _scroller_ de gauche à droite notamment. Contrairement à la plupart des sites web qui s'adaptent à la taille de nos écrans ;
2. vous ne pouvez pas modifier certaines propriétés du document, comme la taille des caractères.

Hors l'EPUB est un format _adaptatif_, il a été imaginé pour s'adapter à l'écran qui l'affiche, et pour que ses propriétés graphiques puissent être modifiées par son lecteur ou sa lectrice.
Pour vous donner une idée de ce que cela veut dire, lorsque vous lisez un EPUB sur une tablette vous pouvez choisir la police typographique, la taille des polices, l'interlignage, les marges, etc.
Que vous souhaitiez disposer d'un texte dense ou au contraire de lignes aérées, le format EPUB (et l'application qui lit ce format) vous permet de le faire.
Les fichiers intégrant le contenu sont au format HTML.
Pour comprendre ces spécificités du format EPUB, je vous conseille de regarder "L’ebook redistribuable" de Jiminy Panoz ([https://jaypanoz.github.io/reflow/](https://jaypanoz.github.io/reflow/)), il s'agit d'une page web expliquant les particularités avec des formats, vous trouverez la référence sur la page du cours.

Nous reparlerons de ce format EPUB, basé essentiellement sur le HTML comme nous venons de le voir, et dont le W3C a également la charge du maintien.

### 1.5. Un standard de fait
Le format HTML est désormais le format numérique le plus présent.
Toutes les interfaces de nos terminaux, que ce soient nos ordinateurs, nos téléphones, nos tablettes, nos montres ou les écrans d'affichage utilisent le plus souvent le HTML.
Même le format de livre numérique EPUB utilise le HTML !
Certes le format PDF est encore très employé, et prend une large part des documents hébergés sur le Web.
Il est impossible de déterminer le nombre de documents HTML par rapport aux documents PDF, sur le Web, mais certains sites utilisent ce format pour partager des informations dont la structuration est très complexe ou faute d'autres moyens.
Par exemple les sites web institutionnels en charge de la numérisation de documents patrimoniaux utilisent le PDF pour pouvoir afficher des images tout en conservant une couche de texte.
Mais désormais ces plateformes plébiscitent plutôt le format XML, transformé ensuite en HTML.

De par cette omniprésence — n'ayons pas peur des gros mots — du format HTML dans les espaces numériques, nous pouvons donc constater que le HTML est le format standard de lecture numérique.

Une nuance doit être apportée : depuis tout à l'heure je parle de formats de _lecture_ numérique, il faut bien distinguer ce _type_ de format de plusieurs autres types :

- le **format source** qui est à l'origine de ce format. Par exemple il peut s'agir d'un format XML dans lequel de nombreuses informations et données sont présentes, et dont seulement une partie va être utilisée pour produire un format qui sera lu sur écran (certaines données seront utilisées par quelques utilisateurs ou utilisatrices dans le cadre d'autres usages). Le format source est le format de conservation ;
- mais aussi le **format d'écriture**, qui peut être le même que le format source. Par exemple le format Markdown, celui que vous utilisez avec l'éditeur en ligne Codi, est un format d'écriture, mais qui peut être conservé sous forme de base de données, ou sous la forme d'un fichier XML.

Cette distinction mériterait un prolongement, nous y reviendrons dans les prochaines séances.
Si ce point n'est pas clair pour vous, pas d'inquiétude nous y reviendrons !

Enfin, pour être le plus juste possible je dois également préciser que le format HTML est en fait une famille de formats — au pluriel — HTML.
Il y a plusieurs types d'HTML qui diffèrent sur des questions techniques dont nous aurons peut-être l'occasion de reparler.
Si les différences en XHTML et HTML5 m'intéressent particulièrement, il faut reconnaître qu'elles ne vous seront peut-être pas utiles dans l'immédiat.
Retenez simplement que le format HTML standard et aussi le plus utilisé actuellement est le HTML5, tout simplement appelé HTML.

Avant de passer aux questions de formats d'_écriture_ numérique, retenons également que le HTML — ou le Web dans sa dimension de documents affichables et lisibles — est aujourd'hui le standard de la lecture numérique.
Est-ce qu'il s'agit pour autant d'une norme ?
Nous allons en reparler.

## 2. Les formats d'écriture numérique
Il est maintenant temps de parler de formats d'écriture numérique.
Pour cela nous sommes obligés de parler aussi des outils qui permettent d'écrire dans ces fichiers.
D'autres séances reviendront sur ces logiciels, applications et programmes.

### 2.1. L'omniprésence du traitement de texte
Premier constat : les traitements de texte sont omniprésents dans nos pratiques d'écriture.
Dans le cas où vous vous demanderiez ce qu'est un traitement de texte, il s'agit d'un logiciel pour créer et modifier des documents.
Microsoft Word, LibreOffice Writer, Pages, ou même Google Docs sont des traitements de texte.
Pour en savoir plus sur ces traitements de texte et leur influence sur les pratiques d'écriture dans le domaine littéraire, je vous renvoie à nouveau à la fabuleuse enquête de Matthew Kirschenbaum et son livre _Track Changes_.

Voici quelques notes basées sur la conclusion de ce livre.
Le traitement de texte Word a clairement marqué une rupture, notamment parce qu’il n’arrivait pas seul, mais avec une interface graphique – et non plus seulement DOS.
Nous n'avons pas abordé ce point mais l'informatique a débuté sans interface graphique.
Dans un premier temps seul le Macintosh est compatible avec Word 1.0, ce qui permet à Apple d'avoir un certain avantage.
Nous pouvons noter ici la différence de taille entre l’approche de la plupart des traitements de texte qui ont précédé Word 1.0 – et qui ont perdu ce que Matthew Kirschenbaum appelle la “word wars” : concevoir un outil pour écrire, et donc qui s’adresse aux écrivains, là où Word s’est très vite positionné comme un outil pour créer des documents.
La question financière est évidemment à l’origine de ce positionnement : il était plus rentable de créer un outil pour des tâches bureaucratiques plutôt que pour des écrivains et des écrivaines.
Si nous ne retrouverons jamais l’époque des années 1980 où il y avait de nombreux traitements de texte différents qui tentaient de répondre au mieux aux besoins des consommateurs, nous sommes tout de même sortis de vingt ans d’hégémonie.
Matthew Kirschenbaum prend l’exemple de Scrivener, un traitement de texte très particulier, créé par Keith Blount pour répondre à son besoin de découper ses longs textes en sections facilement identifiables et surtout manipulables.
Scrivener s'oppose au “monothéisme” imposé par Word — nous pourrions aussi parler du monolithisme de Word, ce qui signifie grossièrement que tout est enfermé dans une boîte noire.
Enfin pour Matthew Kirschenbaum le traitement de texte — ou word processing — n’est pas simplement de l’écriture car il s’agit d’une action réfléchie, choisie, qui nécessite un outil spécifique.
La distinction entre l'écriture (_writing_), la dactylographie (_type writing_) et le traitement de texte (_word processing_) nous servira à nouveau par la suite lorsque nous identifierons les différentes tâches de l'édition numérique.
Je referme la parenthèse du livre _Track Changes_ en insistant sur deux points :

- le traitement de texte est l'outil de référence pour écrire, alors que d'autres logiciels pourraient être sollicités (les traitements de texte sont majoritairement pensés pour faire de la bureautique, et non pour écrire des romans ou des travaux universitaires) ;
- il est important de distinguer différents types d'écriture.

Pourquoi parler de traitements de texte ?
Parce que les formats DOC, DOCX ou ODT sont des formats d'écriture numérique particulièrement utilisés, et ils sont produits par les traitements de texte.
Ces formats sont désormais majoritairement basés sur le format XML, ou pour être plus précis il s'agit de schémas XML spécifiques.
Mais l'objectif final de ces formats est, dans l'immense majorité des cas, de produire des documents imprimés.

### 2.2. Les bouleversements du format HTML et de l'hypertexte
Comme nous l'avons vu le HTML provoque des bouleversements dans les pratiques de lecture, mais c'est également le cas dans les pratiques d'écriture.
La séance consacrée à l'édition numérique savante et à l'édition numérique littéraire sera l'occasion de _voir_ ce que cela signifie avec plusieurs exemples concrets.
Écrire en HTML permet donc de construire un texte dans une perspective de lecture tabulaire.
Pourquoi ?

- parce que le format HTML est un format de balisage qui permet d'adopter une écriture sémantique et non plus graphique (vous donnez du sens au texte en plus de le caractériser)
- parce que la structure est séparée de la mise en forme
- et les liens hypertextes permettent de créer une navigation dans et hors du texte.

Dès les années 1990 des auteurs et autrices de littérature se sont emparés de ces possibilités offertes par le HTML, cela a été aussi le cas de chercheurs dans les domaines des sciences.
Mais comme je l'ai déjà dit je reparlerai de tout cela lors d'une prochaine séance.

### 2.3. Un standard/norme de l'écriture numérique ?
Je vais faire ici référence à un article que j'ai écrit en 2017, vous pouvez aller le lire en ligne si vous le souhaitez : [Markdown comme condition d’une norme de l’écriture numérique](http://www.reel-virtuel.com/numeros/numero6/sentinelles/markdown-condition-ecriture-numerique).
Je ne vais pas vous refaire mon argumentaire concernant le fait que le format HTML est le format de lecture numérique le plus répandu, je peux peut-être seulement ajouter que c'est aussi le choix le plus cohérent en quelques points :

- le format HTML est rétro-compatible. Rétro-compatible cela veut dire que les pages écrites en HTML de plus anciennes versions sont toujours lisibles malgré des versions plus récentes. En d'autres termes chaque nouvel ajout, ou chaque amélioration ne remet pas en cause les versions précédentes. Comme élément de comparaison, ou plutôt comme contre exemple, les anciennes versions de Word ne sont pas compatibles avec des versions plus récentes ;
- la séparation du fond (la structure) et de la forme ouvre beaucoup de perspectives, comme le fait de pouvoir rédiger un document sans se soucier de sa mise en forme. Dans le cas d'une pratique d'édition on imagine tout de suite l'avantage de la séparation de ces étapes ;
- enfin c'est un format léger qui peut être lu facilement par des logiciels différents, c'est un format interopérable.

Le HTML représente de grandes possibilités d'écriture, mais soyons réalistes.
Même moi qui connaît plutôt bien le fonctionnement de HTML et une bonne part des balises, je n'écris jamais en HTML, c'est un langage de balisage relativement pénible.
Même s'il reste lisible par un humain, il faut reconnaître que ce n'est pas l'idéal pour _écrire_.

Pour palier à cela une solution a été assez vite proposée : des éditeurs de texte WYSIWYG pour What You See Is What You Get, soit Ce que vous voyez est ce que vous obtenez.
En d'autres termes c'est le rendu graphique à l'écran qui sert de référence.
Une interface comportant des boutons permet de structurer le texte (niveaux de titre, listes, citation, gras, emphase), sans pour autant afficher les balises nombreuses et verbeuses.
Cette interface est donc un intermédiaire entre ce que l'utilisateur ou l'utilisatrice voit, et les données sont habituellement stockées dans une base de données.
Mais cette solution comporte quelques inconvénients :

- comme les traitements de texte les outils d'édition WYSIWYG entretiennent une confusion entre structuration et mise en forme, la sémantique se traduisant forcément par un rendu graphique spécifique ;
- les outils WYSIWYG ont la fâcheuse tendance à générer un code HTML de piètre qualité, cette considération technique est importante.

Il faudrait donc regarder du côté d'autres langages de balisage, plus simple à utiliser par celles et ceux qui écrivent, et transformables en HTML.

### 2.4. Le balisage léger
À partir de ce que je viens de vous expliquer vous avez sans doute déjà pensé à une solution dont nous avons déjà parlé : les langages de balisage léger, comme Markdown.
Les langages de balisage léger ont été créés pour permettre une écriture sans pour autant perdre la force de HTML.
Markdown fonctionne à partir de balises simples et peu verbeuses.
À partir de quelques indications comme des tirets, des mots-croisillons ou des étoiles, il est possible de structurer de façon logique un texte.
Markdown allie un moyen de structurer de façon sémantique un texte, tout en conservant affichées ces indications.
Les éditeurs de texte comprenant le Markdown ajoute simplement ce qu'on appelle une coloration syntaxique, c'est-à-dire qu'en fonction du balisage le texte sera affiché avec des couleurs différentes, sans supprimer les balises.
C'est ce qu'on appelle le principe du WYSIWYM : What You See Is What You **Mean**, soit ce que vous voyez est ce que vous signifiez.

Il reste à transformer un fichier Markdown en HTML, puisqu'en soit le format Markdown n'est utile que pour l'_écriture_.
Il existe de nombreux convertisseurs, soit intégrés à des outils d'écriture, soit externes comme Pandoc.
Ces questions de conversion feront l'objet d'autres séances.

Un langage de balisage léger comme Markdown pourrait être considéré comme le standard, voir la norme, de l'écriture numérique.
En conservant les avantages de l'HTML (l'écriture sémantique, la distinction entre structure et mise en forme) tout en supprimant ses inconvénients (notamment un balisage trop verbeux, nécessaire pour les programmes mais difficile à lire pour nous autres humains), un langage de balisage est un moyen très simple et très puissant d'écrire pour le numérique.

## 3. Ce qu'il faut retenir de ce troisième épisode
Ce troisième et dernier épisode de la série Qu'est-ce que le numérique nous a permis d'amorcer une analyse des relations entre analogique et numérique, entre imprimé et dynamique.

Voici quelques points essentiels à retenir :

- les lectures linéaires et tabulaires pré-existent au numérique, mais le numérique peut développer ou augmenter les possibilités de la tabularité ;
- les formats de lecture numérique comme le PDF ou le DOC ou DOCX héritent des pratiques d'écriture analogique ;
- le HTML et des formats dérivés comme l'EPUB intègrent de nouvelles possibilités ;
- si le HTML peut être considéré comme le format standard de la lecture numérique au sens large, ce sont plutôt des langages de balisages léger comme le Markdown qui sont des formats standards d'_écriture_ numérique.

Cette série consacrée à une définition du numérique s'achèvent, merci de m'avoir écouté.
