+++
author = "Antoine"
title = "Séance 12 - Chaînes éditoriales et systèmes de publication"
date = "2020-11-27"
description = "Douzième séance"
layout = "diapo"
diapo = "8"
notes = "https://demo.hedgedoc.org/MIP281bIQO2j6d4EYrHiDg"
+++
## 1. Définition du concept de "chaîne éditoriale" : vers des systèmes de publication ?

### 1.1. Chaîne éditoriale ?

> L’ensemble des **méthodes** et des **outils** permettant de **concevoir**, **fabriquer**, **produire** et **diffuser** un livre ou une publication. Une chaîne de publication réunit des logiciels propres à la gestion du texte et des images, mais aussi les différents rôles d’un processus éditorial que sont l’auteur, l’éditeur, le correcteur, le graphiste, etc.

Le concept de _chaîne_ est quelque peu galvaudé, ou tout du moins daté, mais il définit bien certaines démarches d'édition qui lient les étapes et les acteurs d'un processus d'édition.
Attention ici il ne faut pas confondre _chaîne éditoriale_ et _chaîne du livre_, dans le premier cas il s'agit d'un processus d'édition alors que la chaîne du livre regroupe les acteurs du livre et dépasse ce cadre.

### 1.2. La quête du single source publishing
Le concept de _single source publishing_ définit le fait d'utiliser une même source pour produire de multiples formats.
Ce concept repose sur le principe de séparation du contenu et de la mise en forme, mais d'autres modèles existent (voir [la thèse de Robin de Mourat](http://www.these.robindemourat.com/) à ce sujet).

Voici deux images qui illustrent bien cette question d'une publication basée sur une seule et même source :

![](/images/single-source-publishing-2.jpg)

![](/images/single-source-publishing.jpg)

## 2. 5 systèmes de publication (en édition savante)
Nous allons explorer le fonctionnement des 5 _systèmes de publication_ suivants :

1. Lodel
2. Métopes
3. Scalar
4. Manifold
5. Quire

### 2.1. Lodel
Lodel est un système de gestion de contenu (ou CMS en anglais) pour l'édition scientifique :

- **objectifs** : proposer un outil (relativement) simple d'utilisation pour éditer, publier et diffuser des publications scientifiques (articles et monographies) ;
- **protocole éditorial** : édition dans Lodel, avec ou sans une préparation préliminaire avec un traitement de texte (modèles et feuilles de styles spécifiques) ;
- **format(s) d'édition** : le travail peut être fait en 2 étapes (traitement de texte puis Lodel), dans Lodel les données sont conservées dans une base de données ;
- **format(s) produit(s)** : XML et XML-TEI, métadonnées ;
- **diffusion** : XML valides pour une diffusion sur les plateformes d'OpenEdition.

Lodel est clairement orienté pour gérer des contenus scientifiques avec des riches appareils critiques, et dans la perspective d'une diffusion sur les plateformes francophones.
Exemple : https://journals.openedition.org/corela/11734.

- **principes** : une solution clé en main, développée par l'une des principales plateformes de diffusion scientifique en sciences humaines en France (ou dans l'espace francophone) ;
- **contraintes** : ce CMS est loin d'être simple à installer. Son utilisation nécessite une formation adéquate (normal pour un outil aussi puissant) ;
- **actualité** : le code de Lodel est en train d'être revu en profondeur, il repose sur des briques logicielles vieillissantes.

J'ai moi-même essayé d'installer Lodel, j'ai abandonné. Le code va être revu en profondeur pour s'adapter à des solutions plus contemporaines.

### 2.2. Métopes
L'infrastructure Métopes, pour Méthodes et outils pour l’édition structurée, permet la production de formats XML pour la diffusion scientifique, en se basant sur le principe du _single source publishing_ :

- **objectifs** : produire des articles et des monographies pour les plateformes de diffusion scientifique ;
- **protocole éditorial** : édition avec un traitement de texte via l'utilisation de feuilles de styles, puis génération des formats XML via différents outils ;
- **format(s) d'édition** : essentiellement du .docx et du .odt pour l'écriture et l'édition, puis XML ;
- **format(s) produit(s)** : un format XML-TEI qui peut être transformé en de nombreux formats (HTML, .indd, XML pour Cairn.info ou OpenEdition, etc.) ;
- **diffusion** : ce n'est pas le but de la chaîne, même si de nombreux formats de diffusion pour des plateformes francophones sont pris en compte (OpenEdition, Cairn.info et Érudit).

![](/images/metopes.png)

Qu'est-ce que XSLT ?
Un moyen de transformer un fichier balisé en un autre fichier balisé, avec une construction logique et en arbre sémantique.

Métopes en détail :

- **principes** : proposer une chaîne puissante qui se connecte à des outils existants (traitements de texte et InDesign) pour l'édition scientifique et la diffusion scientifique ;
- **contraintes** : installer et utiliser les outils de Métopes qui viennent _s'ajouter_ ;
- **actualité** : Métopes travaille par exemple à l'interconnexion avec d'autres outils (comme Stylo).

Métopes est un système de publication agnostique, qui utilise comme format pivot XML-TEI, mais qui peut prendre en entrée tout type de formats (structurés et balisés).

### 2.3. Scalar
Scalar est une plateforme de publication pour les humanités numériques :

- **objectifs** : permettre la création, l'édition et la publication de contenus académiques numériques et enrichis via une interface simple d'utilisation (monographies ou apparentées) ;
- **protocole éditorial** : gestion de contenus façon CMS, avec un modularité de ces contenus et de leurs enrichissements ;
- **format(s) d'édition** : interface web, bases de données ;
- **format(s) produit(s)** : web ;
- **diffusion** : directement sur la plateforme Scalar, avec des données exposées pour le web sémantique.

![](/images/scalar.png)

Quelques exemples : http://scalar.cdla.oxycreates.org/ ou https://scalar.usc.edu/nehvectors/thirdworldmajority/.

Scalar en détail :

- **principes** : permettre une grande souplesse dans la gestion de contenus très divers, y compris des contenus multimédias, avec une maîtrise de la chaîne complète (création, édition, publication, diffusion) ;
- **contraintes** : plateforme monolithique et centralisée, édition façon CMS et WYSIWYG assez fastidieuse (et datée) ;
- **actualité** : des outils de versionnement et d'édition sont désormais intégrés.

L'outil est un peu vieillissant même s'il reste très puissant.
C'est moins sexy que Manifold, mais aussi plus riche en possibilités.
[La documentation](https://scalar.usc.edu/works/guide2/index) est complète.

### 2.4. Manifold
Manifold est une plateforme collaborative pour l'édition scientifique :

- **objectifs** : permettre d'éditer facilement des publications scientifiques aux formats numériques (monographies) ;
- **protocole éditorial** : import de formats de traitement de texte (type .docx), ajout de données et de contenus enrichis, annotations et commentaires ;
- **format(s) d'édition** : base de données, interface web ;
- **format(s) produit(s)** : web, EPUB ;
- **diffusion** : Manifold est une plateforme d'édition **et** de diffusion.

Le positionnement de Manifold est très similaire à celui de Scalar, mais plus orienté texte et avec une dimension beaucoup plus contributive (annotations, commentaires, brouillon suivable).
Un travail d'UX important a été apporté, c'est fluide et rapide.

Exemple : les livres de University of Minnesota Press.
[Cut/Copy/Paste, Fragments of History, Whitney Trettien](https://manifold.umn.edu/projects/cut-copy-paste).

Manifold en détails :

- **principes** : proposer une chaîne de publication (relativement) facile à installer, facile à utiliser et libre/open source. Il est possible d'éditer avec l'interface web _ou_ via les fichiers statiques (Markdown, YAML) ;
- **contraintes** : même si Manifold est basé sur des briques libres, c'est un système monolithique ;
- **actualité** : l'outil a renforcé l'intégration de formats type .docx, avec l'abandon de l'éditeur Markdown.

Manifold est aussi intéressant sur son adaptation à la réalité : la plupart des auteurs ne souhaitant pas travailler avec Markdown, l'éditeur de texte a été supprimé du système (mais il est possible d'importer du Markdown ou du YAML).
Manifold est, malheureusement, plus une plateforme de diffusion qu'une plateforme d'_édition_.
[La documentation](https://manifoldapp.org/docs/) est très complète.

### 2.5. Quire
Une chaîne modulaire basée sur des outils du développement web :

- **objectifs** : produire plusieurs formats à partir d'une même source (_single source publishing_) ;
- **protocole éditorial** : éditeur de texte et versionnement, possibilité de brancher un CMS ou une interface en WYSIWYG ;
- **format(s) d'édition** : contenus et données balisés (Markdown, YAML, JSON)
- **format(s) produit(s)** : HTMLs, PDFs, EPUB, etc.
- **diffusion** : site web indépendant, vente de livre imprimé, dépôt de données

Quire a été en place par Getty Publications (la maison d'édition du musée Getty) pour éviter l'écueil des systèmes de publication numérique monolithiques.
Voici le fonctionnement schématisé de la chaîne Quire :

![](/images/chaine-modulaire.svg)


Exemple : https://www.getty.edu/publications/ambers/.
La documentation est très complète : [https://gettypubs.github.io/quire/](https://gettypubs.github.io/quire/).

Quire en détails :

- **principes** : ne pas dépendre d'un logiciel monolithique, maîtriser les outils de la chaîne éditorial, pouvoir faire évoluer la chaîne, constituer une communauté d'éditeurs et d'éditrices ;
- **contraintes** : utilisation d'un éditeur de texte et d'un logiciel de versionnement. Toutes les briques sont libres _sauf_ PrinceXML (le générateur de PDF) ;
- **actualité** : communauté qui se constitue autour de l'outil, évolutions régulières, abandon de PrinceXML.

Par ailleurs les éditeurs de Getty Publications utilisent le versionnement comme principe de gestion des fichiers (surtout pour les corrections).
Ce qu'il faut prendre en compte c'est que les personnes qui vont utiliser ces outils sont des éditeurs ou des éditrices, et plus spécifiquement l'équipe du département numérique de Getty Publications.
Il y a donc tout une dimension de formation, même si [la documentation](https://gettypubs.github.io/quire/) est très fournie.

À lire en complément :

- https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/
- http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/
- https://publications-prairial.fr/balisages/index.php?id=321

À noter que Quire pourrait aussi produire des formats XML sans trop de difficultés.

## 3. Enjeux de fabrication, de production et de diffusion

- éradiquer le traitement de texte ?<!-- .element: class="fragment" -->
- deux positionnements : bases de données VS fichiers en plein texte ;<!-- .element: class="fragment" -->
- le tiercé gagnant :<!-- .element: class="fragment" -->
  1. single source publishing<!-- .element: class="fragment" -->
  2. chaîne modulaire<!-- .element: class="fragment" -->
  3. indépendance et pérennité<!-- .element: class="fragment" -->
- l'enjeu de la diffusion et du référencement : être trouvable et visible.<!-- .element: class="fragment" -->

## Ce qu'il faut retenir de Chaînes éditoriales et systèmes de publication

- le concept de chaîne éditorial tend vers des _systèmes_ sous l'influence du numérique ;
- les chaînes d'édition numérique _varient_ en fonction des objectifs ;
- 3 tendances observées :
  1. l'enrichissement des contenus (multimédias, annotations, commentaires)
  2. une recherche d'utilisabilité (écriture et lecture)
  3. bases de données complexes vs données à plat
