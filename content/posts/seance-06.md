+++
author = "Antoine"
title = "Séance 06 - Introduction aux humanités numériques"
date = "2020-10-09"
description = "Sixième séance"
layout = "diapo"
diapo = "4"
notes = "https://demo.codimd.org/dnEczO7lR4-X7EtFlv2_bQ"
+++
L'objectif de ce cours est de vous permettre d'avoir une compréhension de cette approche interdisciplinaire.
Les humanités numériques, comme l'édition numérique, ne sont pas le simple ajout du numérique à un domaine ou une discipline déjà existante, les choses sont plus complexes.
Les humanités numériques sont un bon exemple d'évolution majeure dans nos pratiques et nos réflexions, il est donc important de comprendre ses origines et son panorama de pratiques.

Comme vous vous en doutez, la série de balados sur qu'est-ce que le numérique va grandement nous aider dans ce cours.

## 0. Pourquoi aborder les humanités numériques dans ce cours ?

1. les humanités numériques : une approche interdisciplinaire (en SHS)
2. questionnements nécessaires dans les champs de la littérature et de l'édition
3. évolution des processus d'édition et de publication


## 1. Aux origines : l'automatisation des procédés de recherche
Nous allons rapidement parler du chapitre que vous deviez lire pour aujourd'hui, mais avant cela parlons rapidement du livre et de son auteur.

### 1.1. Pierre Mounier et son livre

- Pierre Mounier : chercheur français impliqué dans l'édition numérique et les humanités numériques ;
- l'ouvrage _Les humanités numériques : Une histoire critique_ : portrait des humanités numériques, "champ de recherche autant que champ de bataille" ;
- "Réinventer les humanités par le numérique suppose de relever trois défis de taille" : rapport à la technique, politique, scientificité.

### 1.2. L'origine : l'index
En 1949 Roberto Busa utilise des procédés informatiques pour l'analyse de sources textuelles :

- transcription de la _Somme théologique_ de Thomas d’Aquin ;
- utilisation de cartes perforées ;
- constitution d'index (13 millions de fiches) ;
- une nouvelle approche : utiliser l'informatique pour réaliser des tâches inédites ;
- le texte devient base de données.

Il s'agit d'une nouvelle approche : "Cette approche utilise l’informatique non pour faciliter ou accélérer le travail du chercheur, mais pour conduire l’analyse à un niveau jusqu’ici inaccessible, permettant de proposer des interprétations inédites."
Le texte se transforme en base de données, il devient un objet.

### 1.3. La relation entre industrie et recherche

- IBM utilise ce projet comme moyen de communiquer et pour améliorer son image ;
- la course de la technique et de l'informatique doit conserver une image humaine ;
- du traitement automatique des données quantitatives vers le traitement du langage ;
- le duo chercheur-ingénieur.

Il convient d'interroger la relation entre domaine de recherche et industrie : pourquoi une entreprise américaine comme IBM voulait-elle s'impliquer (y compris financièrement) dans ce projet ?
Pierre Mounier l'explique mieux que moi, mais je voudrais insister sur un point en particulier : le fait que l'après-guerre est un moment clé dans l'industrialisation technique, et que IBM profite de ce projet _humaniste_ pour prouver qu'il y a une "continuité établie entre l’homme et la machine" (pour citer Pierre Mounier).
Une sorte de _greenwashing_ au moment où des critiques se font entendre contre l'informatisation galopante (voir notamment Jacques Ellul puis Gilbert Simondon).

### 1.4. Les petites mains
Il y a une invisibilisation des femmes dans le projet de Roberto Busa.

(Voir l'article de Melissa Terras : [https://melissaterras.org/2013/10/15/for-ada-lovelace-day-father-busas-female-punch-card-operatives/](https://melissaterras.org/2013/10/15/for-ada-lovelace-day-father-busas-female-punch-card-operatives/))

C'est encore un phénomène que nous pouvons observer dans le domaine de la recherche : des chercheurs considèrent que le travail de mise en forme et de balisage d'un contenu (comme un article ou un livre) doit délégué à une secrétaire.
Ce travail de balisage et de mise en forme est considéré comme trivial ou neutre, alors que c'est à ce moment qu'une bonne partie de la réflexion se met en place : comment structurer un propos, quelle dose de données ajouter à un texte, quels liens faire entre 
En tant que chercheur (ou plutôt apprenti chercheur) il m'importe de maîtriser cette partie, et de ne pas la déléguer.
Et il m'importe que les chercheur·e·s avec qui je travaille maîtrise également cette dimension essentielle de l'écriture scientifique.

Pour conclure sur ce point et pour citer Marcello Vitali-Rosati : " Il faut penser la matérialité de l’écriture. Ou mieux: la pensée n’est que cette matérialité."


### 1.5. Pour une définition des humanités numériques

> Le développement des humanités numériques comme application du développement de l’informatique dans un champ spécifique permet à la fois d’en éclairer les ressorts profonds et d’interroger la place que peuvent occuper les humanités dans des sociétés sous l'influence des technologies numériques.  
> Pierre Mounier, _Les humanités numériques : Une histoire critique_

### 1.6. Pour compléter
À partir du chapitre "Histoire des humanités numériques" de Michael Sinatra dans _Pratiques de l’édition numérique_ ([http://www.parcoursnumeriques-pum.ca/histoire-des-humanites-numeriques](http://www.parcoursnumeriques-pum.ca/histoire-des-humanites-numeriques)).

- Internet et le Web : accélérateurs ;
- convergence entre les humanités et le numérique : éviter toute opposition du type humanité-technologie ;
- réflexivité.

À la question "Le numérique est-il le sujet ou l’objet des humanités numériques ?", Marcello Vitali-Rosati et Michael Sinatra proposent de considérer une "convergence" entre les humanités et le numérique, et d'éviter toute opposition du type humanité-technologie.

Ce texte révèle un point fondamental dans le champ des humanités numériques : la relation entre d'une part des méthodes et des outils, et d'autre part l'analyse de l'influence de ces méthodes et outils sur les contenus même de la recherche scientifique.
L'informatique, Internet et le numérique façonne désormais notre environnement, et nous devons avoir une démarche de réflexivité pour ne pas considérer cela comme de simples nouveaux outils dans la panoplie du chercheur.
Les enjeux de constitution et de diffusion du savoir sont au centre de ces réflexions, ainsi l'édition est une opportunité de lier pratique et théorie, d'exercer cette réflexivité.


## 2. Quelques dates importantes

- 1949 : Roberto Busa et son _Index Thomisticus_ ;
- 1960-1970 : émergence de travaux similaires ;
- 1978 : création de l'Association for Computers and the Humanities ;
- 1987 : création de la liste de diffusion "Humanist" ([lien](https://dhhumanist.org)) ;
- 1987 : lancement du projet TEI ;
- 2004 : publication de _A Companion to Digital Humanities_ par Susan Schreibman, Ray Siemens et John Unsworth ;
- 2008 : A Digital Humanities Manifesto (UCLA) ([lien](http://manifesto.humanities.ucla.edu/2009/05/29/the-digital-humanities-manifesto-20/)) ;
- 2010 : manifeste des digital humanities (THATCamp Paris) ([lien](https://tcp.hypotheses.org/318)).

Ce que l'on peut voir en creux à travers ces quelques dates, c'est que cette approche interdisciplinaire qui réunit une communauté active, est particulièrement réflexive : elle interroge ses propres pratiques et ses fondements théoriques en même temps qu'elle se constitue.


## 3. Édition, littérature et humanités numériques
**humanités numériques ≠ SHS + logiciels**

Les humanités numériques ne se limitent pas à l'usage ou à la maîtrise de logiciels dans les domaines des sciences humaines et sociales.

### 3.1. Quelques domaines en exemples

- histoire : Histoire illustrée et interactive du Mile End ([http://memoire.mile-end.qc.ca](http://memoire.mile-end.qc.ca)) ;
- histoire de l'art : Rethinking Guernica ([https://guernica.museoreinasofia.es](https://guernica.museoreinasofia.es)) ;
- histoire : Mapping Gothic France ([http://mappinggothic.org](http://mappinggothic.org)) ;
- philosophique : MARXdown ([https://marxdown.github.io/](https://marxdown.github.io/)).


### 3.2. Littérature et humanités numériques
Quelques projets _humanités numériques_ en littérature :

- Les dossiers de Bouvard et Pécuchet ([http://www.dossiers-flaubert.fr/](http://www.dossiers-flaubert.fr/)) ;
- Tout Voltaire ([https://artfl-project.uchicago.edu/tout-voltaire](https://artfl-project.uchicago.edu/tout-voltaire)) ;
- Les manuscrits de Stendhal ([http://stendhal.demarre-shs.fr/](http://stendhal.demarre-shs.fr/)).

Les manuscrits de Stendhal est un projet de numérisation et de conversion des manuscrits de Stendhal.
Pour citer les personnes ayant travaillé sur le projet : "Chaque médium a sa spécificité : convivialité, hypertextualité, liberté de parcours et possibilité de recherches originales pour l’édition électronique; construction d’un nouvel « objet livre » au sens traditionnel, dont le contenu aura été renouvelé par le passage par la base documentaire et la redéfinition des corpus."


### 3.3. Édition et humanités numériques
_Les sculptures de la villa romaine de Chiragan_ (Pascal Capus, Musée Saint-Raymond) :  
[https://villachiragan.saintraymond.toulouse.fr/](https://villachiragan.saintraymond.toulouse.fr/)

Pour en savoir plus : [https://www.youtube.com/watch?v=-bTGBPaJR3o](https://www.youtube.com/watch?v=-bTGBPaJR3o)


## 4. Introduction à Stylo
### 4.1. Stylo ?

- éditeur de texte sémantique pour la rédaction scientifique ;
- écrire, structurer, prévisualiser, réviser, exporter ;
- utilisation d'un langage de balisage léger (Markdown), d'un langage de description de métadonnées (YAML) et d'un langage de description bibliographique (BibTeX) ;
- utilisation de briques logicielles existantes (et libres).

### 4.2. Démonstration
**[https://stylo.huma-num.fr](https://stylo.huma-num.fr)**

Documentation : [https://stylo-doc.ecrituresnumeriques.ca/](https://stylo-doc.ecrituresnumeriques.ca/)

### 4.3. Exercices à réaliser pour le 16 octobre

- création d'un compte : [https://stylo.huma-num.fr](https://stylo.huma-num.fr)
- création d'un article comportant 3 niveaux de titres, des citations, des notes de bas de page, des références bibliographiques
- partage de votre article avec moi : antoine@quaternum.net

(Pour vous aider voici la documentation : [https://stylo-doc.ecrituresnumeriques.ca/](https://stylo-doc.ecrituresnumeriques.ca/))
