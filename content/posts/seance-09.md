+++
author = "Antoine"
title = "Séance 09 - Qu'est-ce que l'édition numérique 1/2"
date = "2020-11-05"
description = "Neuvième séance"
layout = "podcast"
audio = "fra3826-qu-est-ce-que-l-edition-numerique-1-2.mp3"
notes = "https://demo.codimd.org/br04hp81StSubXzORpQCwA"
+++
## Introduction
Bienvenue dans cette baladodiffusion consacrée à l'édition numérique.

Voici le premier épisode de ce podcast intitulé "Qu'est-ce que l'édition numérique ?".
Comme vous vous en doutez déjà, nous en parlons en fait depuis le début du cours Théories de l'édition numérique.
Dans ce premier épisode nous allons commencer par un détour, comme j'en ai l'habitude, afin de comprendre quelles ont été les rencontres qui ont précédées la formation d'un domaine "édition numérique".
Entre le fait que le premier livre numérique a été inventé en 1971, ou l'informatisation des métiers de l'édition, l'édition numérique est le résultat d'un long processus.
Nous allons ensuite aborder les questions des _formes_ de l'édition numérique, je vais revenir sur les questions de formats et en partie de protocoles.
J'ajouterai des ressources dans la transcription de cette baladodiffusion.
Voilà pour le plan de ce premier épisode de "Qu'est-ce que l'édition numérique ?", le prochain épisode sera consacré au cheminement du texte et aux productions de l'édition numérique.

Nous pouvons donc commencer cet épisode.

## 1. Les rencontres du livre et du numérique
Avant de parler _vraiment_ d'édition numérique nous devons donc faire un léger détour.
L'édition numérique ne surgit pas de nulle part, tout comme le codex ou le procédé d'impression à caractères mobiles.
Il y a une filiation entre l'édition dite _classique_ et l'édition dite _numérique_, à tel point que les deux s'entremêlent désormais — je reviendrai d'ailleurs tout à l'heure sur la question de l'hybridation chère à Alessandro Ludovico.

Le livre et le numérique ont eu de nombreuses occasions de se rencontrer durant les cinquante dernières années, avant de voir émerger le livre numérique et l'édition numérique.
Nous allons prendre un moment pour explorer ces rencontres.

### 1.1. L'informatisation des métiers du livre
L'informatisation des métiers du livre débute dès l'arrivée de l'informatique à la fin des années 1940, avec la mise en place de systèmes d'information (catalogues) pour les bibliothèques.
Ces dernières mettent en place des moyens d'organiser et de classer l'information, mais aussi de la rendre accessible.
Les bibliothèques sont de véritables pionnières dans le domaine du livre, bien plus réactives et ... que d'autres acteurs du livre comme les maisons d'édition ou les librairies, malgré cette image poussiéreuse qu'on leur donne parfois.
Ces systèmes de gestion de l'information, ces catalogues informatisés, vont être les premiers pas de cette rencontre entre le livre et le numérique.

C'est ensuite l'édition qui va connaître de grands bouleversements au début des années 1980 avec les premiers ordinateurs personnels (Mac), équipés de traitements de texte et de logiciels de publication assistée par ordinateur.
D'abord les traitements de texte, dont nous avons déjà parlé ici avec le formidable ouvrage de Matthew Kirschenbaum _Track Changes_, sur les premiers ordinateurs Mac (avec interface graphique) ou Windows (avec une interface DOS plus complexe à manipuler).
La mise en page des livres commence donc à se faire sur ordinateur, et les techniques de composition et d'impression font un passage depuis la photocomposition (très contraignante) à l'informatique (beaucoup plus souple même si les débuts de la PAO n'ont pas non plus été exaltants).
Les maisons d'édition s'équipent, puis les auteurs à leur tour.
À la fin des années 1990, l'amont de ce que l'on appelle la chaîne graphique est entièrement numérisée, puis c'est le cas de la phase prépresse — la prépresse correspond aux traitements nécessaires juste avant l'impression, cette tâche est assurée par l'imprimeur.
Les imprimeurs vont également adopter de nouveaux outils : les imprimantes numériques (à jet d'encre ou laser toner).
Avec cette deuxième rencontre du livre et du numérique, la partie production, nous pouvons constater l'influence des technologies sur les techniques de conception et de fabrication.

La partie diffusion suit ensuite, avec les distributeurs-diffuseurs et les librairies.
Les distributeurs-diffuseurs, ce sont ces structures chargées de faire connaître un livre et de l'acheminer jusqu'à l'instance qui va permettre l'accès au livre.
C'est le _maillon_ entre la maison d'édition et la librairie, le point de vente ou la bibliothèque.
Si certains éditeurs assurent une partie de la diffusion, la plupart du temps il y a des structures dédiées à ce travail de mise en relation entre le livre et l'intermédiaire (libraire ou bibliothécaire).
Le rôle du diffuseur est d'acheminer les informations sur le livre, cela comprend le référencement dans des bases de données spécialisées, mais aussi un travail humain de présentation de catalogues d'éditeurs et d'éditrices.
Le distributeur est la structure qui se charge non pas d'acheminer l'information sur le livre, mais le livre lui-même.
C'est un travail de stockage, de livraison et de gestion des retours (car tous les livres en librairie ne sont pas vendus, et le libraire ne peut pas les stocker indéfiniment).
La diffusion et la distribution est une double tâche souvent intégrée dans une et même structure.
L'informatisation de la diffusion et de la distribution cela signifie plusieurs choses :

- la mise en place de bases de données référençant les livres ;
- des systèmes de gestion des commandes pour faire le lien entre le diffuseur (ou l'éditeur) et la librairie ou la bibliothèque ;
- des moyens de suivre les ventes voir les comportements des lecteurs dans le cas du livre numérique.

Enfin les librairies ont suivi cette évolution parfois dans la douleur, ces commerces pouvant se passer de systèmes complexes pour une bonne partie d'entre elles.

Voilà pour cette première rencontre du livre et du numérique : l'informatisation des différentes parties de la chaîne du livre.
Les bibliothèques, l'édition puis la diffusion et les points de vente.
Ce que nous pouvons noter à ce niveau, c'est que cette informatisation des métiers du livre — ou des maillons de la chaîne du livre — s'est faite avec une accélération de la circulation des livres.

### 1.2. Les communautés de lecteurs et de lectrices sur le Web
Le début des années 1990 connaît une révolution majeure : Internet.
Ce nouveau moyen de communication va permettre à des communautés de lecteurs de se constituer, de façon beaucoup plus large et rapide qu'avec les cercles de lecture ou les systèmes de correspondance postale.
D'abord avec des systèmes comme [Usenet](https://fr.wikipedia.org/wiki/Usenet), ces communautés vont grandir avec le Web et de nombreux outils facile à prendre en main.
Parmi de nombreuses initiatives, Zazieweb est probablement la première d'une ampleur importante, et qui aura posé les bases de plateformes comme Babelio ou Sens Critique encore actives aujourd'hui (contrairement à Zazieweb).
Les lecteurs et les lectrices se retrouvent pour partager leurs lectures, et font un travail de soutier pour référencer les livres.

Pourquoi aborder cette question des communautés de lecteurs et de lectrices ?
Parce que c'est grâce à un travail long, exigent, de passionnés et de passionnées que des livres et des maisons d'édition vont se faire connaître.
De nouveaux intermédiaires s'ajoutent aux libraires et bibliothécaires.
C'est ce qui permettra, plus tard, à une certaine littérature numérique de se diffuser largement — enfin toute proportion gardée par rapport aux circuits classiques.

### 1.3. La connexion des acteurs du livre
Dans le prolongement de cette mise en relation des lecteurs et lectrices, Internet et le Web est aussi une formidable opportunité pour les acteurs du livre — c'est-à-dire les différents maillons de la chaîne du livre — de se connecter.
Non pas que cela n'était pas possible _avant_ Internet et le Web, mais cela a fortement facilité leur mise en relation.

Il s'agit donc de la troisième rencontre du livre et du numérique, après l'informatisation des métiers du livre et la constitution de communautés de lecteurs et lectrices sur le Web.

### 1.4. La littérature numérique
La quatrième rencontre est plus expérimentale.
Bien avant l'apparition du livre numérique au début des années 2000, des récits numériques font leur apparition, principalement via l'utilisation de l'hypertexte.
Il faut distinguer deux types d'expérimentations différentes : d'un côté la publication _dématérialisée_ — même si comme nous l'avons déjà vu rien n'est jamais vraiment dématérialisé —, et de l'autre la création _nativement_ numérique, en exploitant les ressources offertes par ce nouveau médium qu'est le Web.

Pour la publication dématérialisée, les auteurs et les autrices s'emparent du Web en ouvrant leurs carnets d'écriture, ils partagent leurs travaux en cours, ou construisent des sites collectifs comme des revues numériques.
Il s'agit d'une ouverture bien plus large que ce qu'avait pu faire les auteurs via des moyens analogiques.
Et ce qui nous semble naturel pour nous, c'est-à-dire que des créateurs et des créatrices disposent d'espace de visibilité sur Internet et le Web, ne l'était pas du tout dans les années 1990.
Des initiatives individuelles apparaissent, puis collectives, le collectif Remue.net ([https://remue.net/](https://remue.net/)) en est un bon exemple : d'abord espace personnel de François Bon à la fin des années 1990, Remue.net se transforme en revue littéraire puis en collectif.

Pour les expérimentations en littérature numérique, des initiatives voient le jour dans les années 1990, réservées à un public très confidentiel.
Que ce soit des récits _à tiroir_ avec des pages simplement liées entre elles par des liens hypertextes, ou des expérimentations plus graphiques, le panorama est très large.
J'ai déjà présenté quelques projets qui vont dans ce sens, pour découvrir de nombreux exemples je vous invite à aller voir le Consortium of Electronic Literature ([http://cellproject.net/search/site/](http://cellproject.net/search/site/)).


### 1.5. Le commerce en ligne
Enfin, le commerce du livre sur Internet se développe avec la création d'Amazon en 1994.
Oui 1994, c'est-à-dire au tout début du Web.
Amazon est d'abord une librairie en ligne, avant de devenir plus tard un site de vente d'à peu près n'importe quoi.
Amazon a profondément marqué le paysage du livre et la chaîne du livre :

- il est devenu un point de vente incontournable, que ce soit pour les personnes dépourvues de librairies proches ou même pour les lecteurs et les lectrices des grandes villes ;
- Amazon s'impose comme un négociateur intransigeant, forçant les éditeurs à revoir leur marge, à ce sujet au milieu des années 2010 il y a eu plusieurs bras de fer entre Amazon et des très grands groupes d'édition ;
- et enfin pire ennemi des libraires, concentrant tous les préjudices supposés d'Internet et du Web pour les petits commerces, en sachant que certaines librairies ou maisons d'édition ont elles-mêmes _vendues leur âme au diable_ (si je puis dire) en utilisant la place de marché Amazon (ou _market place_) pour vendre leur livre. Je me souviens d'une discussion avec un éditeur basé à Grenoble, cela devait être en 2013 ou 2014, et ce dernier expliquait qu'il ne pouvait plus se passer d'Amazon puisque 30% de son chiffre d'affaires passait désormais par Amazon (et il s'agit bien de vente directe de l'éditeur et non des ventes indirectes via d'autres revendeurs présents sur Amazon).

À la question "est-ce qu'Amazon est un laboratoire d'innovations ?", la réponse pourrait être oui, tant sur les modèles de vente et de marketing, mais au détriment d'une vision à long terme, tant d'un point de vue humain qu'écologique.
Mais est-ce qu'Amazon est réellement le problème ?
Probablement pas.
Il faudrait plutôt regarder du côté du manque d'appareil légal qui empêcherait des salariés d'être aussi mal payés, ou du côté du manque d'alternatives même si des acteurs du livre tentent de mettre sur pieds des projets pertinents.
Enfin, pour finir sur ce passage sur Amazon, le problème n'est surtout pas lié au numérique, ce dernier ne fait que donner plus d'écho et de puissance.

### 1.6. Une réelle nouveauté ?
Il ne faudrait pas croire que parce que le numérique (avec l'informatique, Internet et le Web) fait son entrée dans la chaîne du livre et plus particulièrement dans l'édition, les métiers se _technicisent_.
L'activité d'édition est déjà une activité oh combien technique, même avant l'apparition de l'imprimerie à caractères mobiles, il s'agit donc plus d'un glissement que d'un basculement, pour prendre une image un temps soit peu physique.

Par ailleurs ces rencontres entre le livre et le numérique, des rencontres constitutives de l'édition numérique, surviennent bien avant le livre numérique tel que nous le connaissons aujourd'hui et qui apparaît dans les années 2006-2007.

Pour observer ces modifications avec un point de vue moins centré sur l'édition et plus sur les industries culturelles, je vous invite à aller lire Bertrand Legendre avec son ouvrage _Ce que fait le numérique au livre_, ou à l'écouter dans [une conférence de deux heures disponible en ligne](https://dlis.hypotheses.org/5282).

Nous observons plusieurs phénomènes en parallèle, dont nous ne pouvons pas dire avec certitude qu'il s'agit de conséquences de ces différentes rencontres entre le livre et le numérique, mais que nous devons observer et analyser :

- il y a une une accélération des processus de production et de diffusion, d'abord en raison de l'apparition de nouvelles techniques d'impression comme l'imprimerie à caractères mobiles ou l'offset, et de nouveaux moyens de transport du livre. Le numérique ne vient alors qu'ajouter une nouvelle dimension ou accroître des moyens déjà existants. Par exemple le fait de pouvoir plus facilement et rapidement modifier les épreuves d'un livre, avec l'usage de l'informatique, impacte toute la chaîne du livre, tout comme les récentes évolutions des procédés d'impression (nous reparlerons de l'impression à la demande au prochain épisode). Cette accélération est particulièrement visible dans le domaine académique avec l'injonction de publier beaucoup et souvent, phénomène connu avec l'expression "[Publish or Perish](https://en.wikipedia.org/wiki/Publish_or_perish)" (ou _publier ou périr_). Si vous arrêtez de publier vous disparaissez ;
- le deuxième phénomène est la segmentation des métiers du livre et plus spécifiquement des métiers d'édition : les maisons sous-traitent de plus en plus les différents métiers de l'édition comme les responsables de collection, les correcteurs, les infographistes ou designers graphiques, les responsables de fabrication ou les attachés de presse. Si cette pratique semble plus courante et depuis plus longtemps, en Amérique du Nord, c'est un phénomène relativement récent en France, et tout aussi surprenant quand on prend en compte le fait que le travail est majoritairement _salarié_ dans ce pays d'Europe. Ainsi ces différents métiers ne sont plus intégrés au sein de la maison d'édition, mais les instances éditoriales font appel à des indépendants pour réaliser des tâches jusqu'ici véritablement _intégrées_ dans les maisons d'édition.

Voici donc quelques éléments, en plus de la série de podcasts sur Qu'est-ce que le numérique ?, qui permettent de poser le décor de l'édition numérique.
Pour savoir où nous sommes il est toujours utile de savoir d'où nous venons, tentons d'approfondir l'état de la situation avec la définition des formes de l'édition numérique, et plus particulièrement les contraintes et les formats.

## 2. Les formes de l'édition numérique
Je dois faire attention ici à ne pas créer une confusion inutile : je vais parler des _formes_ de l'édition numérique et non des formes des _artefacts_ issus de l'édition numérique.
Tout ce qui concerne les formes de production de l'édition numérique fera l'objet d'une des parties du prochain épisode.
Je n'irai donc pas plus loin que la surface en ce qui concerne les différences entre une édition numérique homothétique ou une édition numérique.

### 2.1. Une reconfiguration du travail d'édition
Je vais _encore_ faire un détour, encore plus important que les précédents d'un point de vue historique : parlons du premier livre numérique, qui date de 1971.
Je vous ai déjà présenté l'apparition du livre numérique comme un événement majeur dans l'histoire du livre, survenu en 2006-2007, et lié à l'accumulation des trois phénomènes suivants :

1. la standardisation d'un format pour le livre numérique, et plus spécifiquement le format EPUB, sous l'égide de l'IDPF puis du W3C ;
2. deuxièmement la constitution de catalogues de livres numériques du côté des maisons d'édition, permettant aux lecteurs et lectrices de disposer d'un choix conséquent ;
3. et enfin troisièmement la mise sur le marché d'une liseuse à encre électronique offrant un réel confort de lecture tout en étant relativement bon marché.

Il faut donc distinguer l'apparition _du_ livre numérique de la création du premier livre numérique, qui sont deux phénomènes différents, mais fortement liés.
Tout cela pour vous dire que je ne veux pas vous parler de cela, mais du premier livre numérique créé en 1971 par Michael Hart dans un contexte universitaire.
Je vous replace le contexte : en 1971 l'informatique est déjà née, en revanche la micro-informatique n'en est qu'à ses débuts et Internet permet de connecter quelques ordinateurs.
Le 4 juillet 1971, jour de la fête nationale aux États-Unis, le jeune étudiant qu'est Michael Hart décide de _taper_ la Déclaration d'indépendance des États-Unis pour la partager via des moyens informatiques.
Le texte est saisi sur le clavier d’un terminal du Materials Research Lab, il s'agit du laboratoire informatique de l'Université de l'Illinois.
Fait notable, le texte est saisi en caractères majuscules, les caractères minuscules n’existent alors pas encore.
Michael Hart envoie le fichier texte de quelques kilo-octets par Internet à plusieurs de ses contacts, pour fêter ce 4 juillet.

Pourquoi cet événement est important ?
(Pas le 4 juillet mais bien ce premier livre numérique.)
Tout d'abord parce qu'il s'agit d'une entreprise d'encodage, aussi minimaliste soit-elle, et sans être la première.
En allant regarder [une version plus récente du fichier sur le site du projet Gutenberg](http://www.gutenberg.org/cache/epub/1/pg1.txt), il y a une tentative de structuration de l'information — qui ressemble à un langage de balisage léger.
Quand je parle d'encodage il s'agit bien d'inscrire un texte dans l'objectif de le diffuser sous différentes formes, et pas d'une prise de vue photographique qui serait alors une numérisation.
La différence est très importante, et vous pouvez la faire vous-même : prenez n'importe quel type de document comme un courrier papier de l'université et convertissez-le en numérique, vous avez alors la possibilité de le prendre en photo ou de le re-taper à la main sur votre ordinateur en utilisant par exemple un traitement de texte.
Dans le premier cas vous obtiendrez une prise de vue, une image avec laquelle vos interactions seront limitées (sauf si vous faites un traitement type OCR ou reconnaissance de caractères) ; de l'autre vous avez un texte manipulable, un encodage qui est une _conversion_ de ce document imprimé, vous passez de l'analogique au numérique.

C'est également un événement important car ce travail d'encodage est réalisé dans une optique précise : le diffuser via le réseau Internet.
Le geste de Michael Hart n'est pas seulement un geste de conservation ou d'encodage dans l'objectif de _travailler_ le texte, mais bien de l'encoder pour ensuite pouvoir le publier, le diffuser.

Ce format en plein texte, ou texte brut, et plus précisément le format TXT, ne permet pas une structuration très précise de ce document qui le mériterait pourtant.
En 1971 les limites sont telles que le [codage UTF-8](https://fr.wikipedia.org/wiki/UTF-8) n'existe pas encore, le texte est inscrit en majuscules, ce qui ne facilite pas sa lecture.
Mais malgré tout il s'agit d'un format à la fois "robuste, portable, commode et durable".
Je détourne ici les 4 qualificatifs utilisés par Anthony G Mills, et cités par Alberto Menguel dans son chapitre "La forme du livre" extrait de son livre _Une histoire de la lecture_, pour définir le livre.
_Robuste_ car ce format est encore lisible 50 ans plus tard (même si le fichier actuel diffère légèrement du fichier original), _portable_ car le fichier ne pèse que quelques kilo octets (ce qui est peu même pour des supports limités comme les disquettes), _commode_ car il est lisible sur tout type de dispositif informatique, et enfin _durable_ car ce format est pérenne et pourra toujours être lisible.

Pourquoi ce premier livre numérique bouleverse le travail d'édition ?
Tout d'abord je ne suis pas en train de dire que cette expérimentation a été remarquée dans les années 1970 comme les prémisses de l'édition numérique ou même comme le premier livre numérique, cela n'a pas du tout été le cas.
Nous constatons seulement après coup un bouleversement parce que cette entreprise implique une modification des pratiques.
L'encodage d'un livre avec des moyens informatiques dans un objectif de diffusion numérique — certes assez rudimentaires à l'époque.

### 2.2. Des contraintes venues d'ailleurs
Cet événement, pris parmi d'autres, nous permet de constater que les choix de diffusion ont une influence sur les modalités de production du livre.
Le travail d'édition ne peut pas s'effectuer de la même façon si un livre doit être envoyé par message électronique ou s'il prendra la forme d'un imposant volume imprimé.
L'édition est habituée depuis longtemps à utiliser de nombreuses contraintes techniques, notamment en lien avec les exigences de production imprimée, que ce soit la qualité du papier, le détail typographique, le temps d'impression ou la circulation du livre.
Ce que nous apprend ce premier livre numérique, c'est que les contraintes techniques de création et de production peuvent provenir d'ailleurs, dans le sens où elles ne sont pas forcément spécifiques au domaine du livre ou de l'édition.
C'est ce qu'expliquent bien mieux que moi Benoît Epron et Marcello Vitali-Rosati dans le chapitre "Enjeux stratégiques de la structuration des documents" de leur livre _L'édition à l'ère numérique_.
Je les cite :

> Il s’agit d’un renversement important, puisqu'une part incontournable de l’activité éditoriale devient de fait conditionnée par des choix techniques issus de secteurs d’activité parfois très éloignés de l’édition. (p. 35)

Comment cela se traduit-il concrètement ?
Par exemple par le fait que les maisons d'édition ont dû appréhender un format comme l'EPUB pour la production de leurs livres numériques.
Certaines d'entre elles ont parfois sous-traitées à outrance cette production, mettant en danger leur savoir-faire dans le cas où le sous-traitant produisait des fichiers de mauvaise qualité, ou quand il a fallu mettre à jour des milliers de livres en raison d'une modification des standards.
L'introduction de ces contraintes fortement techniques, accompagnées de protocoles spécifiques, nous amènent donc à la question de la maîtrise des formats.

### 2.3. La maîtrise de nouveaux formats
Je vais prolonger l'exemple du format EPUB, en continuant d'utiliser le texte de Marcello Vitali-Rosati et de Benoît Epron.
Les éditeurs ont donc eu deux possibilités pour s'adapter à ce nouveau format et au nouvel environnement qu'il impose : former les équipes ou s'entourer de spécialistes (des prestataires ou des sous-traitants).
Le paradigme est très différent de l'apprentissage de nouveaux logiciels, car ici il s'agit d'acquérir une _culture numérique_, et non de savoir utiliser quelques fonctionnalités d'un énième logiciel.
Pour le dire autrement, c'est comme si je vous disais que l'édition numérique se résumait à Stylo, l'éditeur de texte que vous avez récemment découvert, alors qu'il ne s'agit que d'_une_ fabrique de publication [parmi beaucoup d'autres](https://www.quaternum.net/fabriques).
Par contre Stylo pose des principes que partagent nombre de chaînes de publication.
A contrario, un logiciel comme InDesign, aussi précis soit-il pour produire un format PDF destiné à être imprimé, est une solution qui n'apporte pas 

Enfin, concernant la nécessité de maîtriser de nouveaux formats pour les maisons d'édition, il faut prendre en compte la différence entre des formats propriétaires qui n'apportent aucune plus-value en terme de connaissances, et des formats ouverts qui obligent à comprendre comment tout cela fonctionne, sans pour autant devenir développeur ou développeuse.
Les formats propriétaires sont rigides, ils enferment leurs utilisateurs et utilisatrices dans des pratiques normalisés — au sens péjoratif du terme — à défaut de proposer une interconnexion entre des pratiques diverses.
Ce qui nous amène à la question de l'interopérabilité, un concept central dans l'édition numérique.

### 2.4. L'interopérabilité comme condition
Comme je vient de le dire, il ne s'agit pas simplement d'intégrer un nouveau format parmi une panoplie.
Le format EPUB a eu l'avantage d'introduire un concept que les éditeurs et les éditrices n'avaient pas forcément pris en compte jusque-là : **l'interopérabilité**.
Rappelez-vous, l'interopérabilité est "la capacité à communiquer de deux logiciels différents" — pour reprendre [les mots](https://framablog.org/2019/06/12/cest-quoi-linteroperabilite-et-pourquoi-est-ce-beau-et-bien/) de Stéphane Bortzmeyer.
C'est un concept encore mal compris dans le monde de l'édition, qui a, comme je l'ai dit tout à l'heure, segmenté toute sa chaîne.
Cette segmentation est telle qu'une fois qu'une maison d'édition confie le travail de mise en page du texte à un graphiste qui va utiliser InDesign, personne ne sera en capacité de modifier le fichier et donc le texte, à part ce graphiste.
L'interopérabilité permet de connecter différents outils, ou plutôt différentes phases d'édition, et ainsi différents humains.
Si l'interopérabilité est mise en place, alors une certaine horizontalité est permise au sein de l'instance éditoriale, et les différentes phases d'édition peuvent s'articuler sans être des étapes hermétiques les unes par rapport aux autres.
L'édition numérique doit être poreuse.

Car bien souvent ce qui peut sembler être de l'édition numérique est en fait de l'édition numérique homothétique.
Mais qu'est-ce que cette expression d'édition numérique homothétique veut-elle bien dire ?
Il s'agit simplement de calquer les pratiques de l'édition classique en ajoutant quelques gadgets numériques comme de nouveaux logiciels ou des applications.
L'arrivée de l'informatique dans le monde de l'édition n'a pas engendrée automatiquement la constitution d'une nouvelle activité qui serait l'édition numérique.
Il a fallu un changement de paradigme, une modification des pratiques, une nouvelle relation au texte et aux différentes façons de produire des artefacts.

### 2.5. Prendre en compte les usages
Avant de conclure ce premier épisode de Qu'est-ce que l'édition numérique ?, parlons des usages de lecture.
J'ai parlé plusieurs fois du format EPUB comme d'un déclencheur dans l'édition, mais d'autres formats numériques existent.
L'attention se porte aujourd'hui moins sur _un_ format spécifique que sur des modes d'accès au texte.
L'EPUB est un bon moyen de diffuser et de vendre des livres au format numérique, mais il ne s'agit ni d'un format source (le texte originel qui permet de produire des formats de sortie), ni du seul format de sortie.

Je vous propose l'hypothèse suivante : l'édition numérique serait par définition multimodale, et hybride.
Je m'explique : une instance éditoriale numérique produirait plusieurs artefacts numériques et non une seule forme.
Plutôt que de se limiter au format EPUB, la condition pour répondre à la dénomination _édition numérique_ serait au moins deux formats différents.
Et, en plus de cela, ces productions numériques s'articuleraient avec des productions imprimées, c'est-à-dire qu'il doit y avoir une hybridation des formats, qu'ils soient imprimés ou numériques — Alessandro Ludovico a largement détaillé cette idée dans _Post-Digital Print_.

Je m'arrête sur cette hypothèse qui mérite un plus long développement.
Je prolongerait cette réflexion dans le prochain épisode de Qu'est-ce que l'édition numérique ?

## Ce qu'il faut retenir
Ce qu'il faut retenir de ce premier épisode de Qu'est-ce que l'édition numérique ?
Nous pouvons réunir plusieurs critères qui nous permettent de définir ce que c'est que l'édition numérique :

- l'édition numérique est une évolution de l'édition dite classique, avec différents événements que nous pouvons identifier et analyser ;
- l'édition numérique est influencée par l'informatique : les outils, les formats et les protocoles ;
- l'édition numérique n'est pas une transposition de l'édition avec quelques outils numériques, mais un changement de paradigme ;
- l'édition numérique serait (j'utilise ici le conditionnel) multimodale et hybride.

Voilà pour ce premier épisode de Qu'est-ce que l'édition numérique ?

En complément je vous invite à aller écouter la baladodiffusion Place de la toile, « Spéciale édition numérique », [archivée sur Archive,org](https://archive.org/details/PlaceDeLaToilespcialeditionNumrique).
